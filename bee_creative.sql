/*
 Navicat Premium Data Transfer

 Source Server         : okymarsel
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : bee_creative

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 06/07/2021 21:01:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bio
-- ----------------------------
DROP TABLE IF EXISTS `bio`;
CREATE TABLE `bio`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kelas_bio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama_bio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `jk_bio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tlhr_bio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tgllhr_bio` date NULL DEFAULT NULL,
  `alamat_bio` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `sosmed_bio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `bio_bio` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `id_pesan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bio
-- ----------------------------
INSERT INTO `bio` VALUES (63, 'MIPA 1', 'Andara Sumarta', 'Perempuan', 'Bandung', '2001-04-30', 'Jl. Pegadaian Timur, No. 14, Pesantren, Kediri', 'ig @andasumiati', 'nothing forever', 'YB0012', NULL, NULL);

-- ----------------------------
-- Table structure for kategori_port
-- ----------------------------
DROP TABLE IF EXISTS `kategori_port`;
CREATE TABLE `kategori_port`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kategori_port
-- ----------------------------
INSERT INTO `kategori_port` VALUES (1, 'Wedding Cinematic', NULL, '2021-06-12 23:38:02');
INSERT INTO `kategori_port` VALUES (2, 'Cinematic Prewedding ', NULL, NULL);
INSERT INTO `kategori_port` VALUES (3, 'Documentation', '2021-06-12 23:13:10', '2021-06-12 23:13:10');
INSERT INTO `kategori_port` VALUES (4, 'Group Photo', '2021-07-01 16:30:07', '2021-07-01 16:30:07');
INSERT INTO `kategori_port` VALUES (5, 'Prewedding', '2021-07-06 20:55:17', '2021-07-06 20:55:17');
INSERT INTO `kategori_port` VALUES (6, 'Family Photo', '2021-07-06 20:58:28', '2021-07-06 20:58:28');
INSERT INTO `kategori_port` VALUES (7, 'Misbahus Surur', '2021-07-06 20:58:59', '2021-07-06 20:58:59');

-- ----------------------------
-- Table structure for komentar
-- ----------------------------
DROP TABLE IF EXISTS `komentar`;
CREATE TABLE `komentar`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pel` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_foto` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_vidio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `komentar` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status_kom` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of komentar
-- ----------------------------
INSERT INTO `komentar` VALUES (5, '4', NULL, '1', 'wew', '1', '2021-06-11 13:45:22', '2021-06-11 14:02:15');
INSERT INTO `komentar` VALUES (6, '4', NULL, '1', 'cik', '0', '2021-06-11 13:45:39', '2021-06-11 13:45:39');
INSERT INTO `komentar` VALUES (16, '2', '4', NULL, 'xixixixi', '1', '2021-07-02 15:13:36', '2021-07-02 15:15:46');
INSERT INTO `komentar` VALUES (17, '2', '4', NULL, 'xaxaxaaxa', '1', '2021-07-02 15:13:49', '2021-07-02 15:16:02');

-- ----------------------------
-- Table structure for layanan
-- ----------------------------
DROP TABLE IF EXISTS `layanan`;
CREATE TABLE `layanan`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_lay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `jenis_lay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `foto_lay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `harga_lay` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ket_lay` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of layanan
-- ----------------------------
INSERT INTO `layanan` VALUES (2, 'Foto Group Outdoor', 'Photography', '1623679375_Avalon Photography by Jaclyn Sollars.jfif', '500', 'foto group konsep outdoor untuk yearbook, perpisahan, gathering, dll. Maksimal orang 5 orang. Lebih dari 5 orang, tambah 30k/orang. *harga belum termasuk uang transport luar kota kediri', '2021-06-04 15:24:58', '2021-06-14 21:06:43');
INSERT INTO `layanan` VALUES (3, 'Yearbook Deluxe', 'Yearbook', '1623679524_coverrr.jpg', '220', 'Ukuran 25x36cm | bonus 15 eksemplar, video&foto dokumentasi, foto wisuda, packaging/popup.', '2021-06-12 21:49:40', '2021-06-14 21:05:24');
INSERT INTO `layanan` VALUES (4, 'Wedding Documentation', 'Photography', '1623680334_wedding photo.jpg', '1.000', 'Foto 4r sebanyak 2 roll + album magnetic | Bonus all softfile, Cetak 16r 1x', '2021-06-14 21:12:09', '2021-06-23 09:40:55');
INSERT INTO `layanan` VALUES (6, 'Photo Product', 'Photography', '1623680455_photo product.jpg', '300', 'Foto khusus untuk meningkatkan daya engage produk anda. Bonus all file | Durasi 1 jam photo', '2021-06-14 21:20:55', '2021-06-14 21:20:55');
INSERT INTO `layanan` VALUES (7, 'Foto Studio', 'Photography', '1623680587_photo studio.jpg', '50', 'foto studio group/individu  |  Harga 50K/orang  |  Durasi 2jam full', '2021-06-14 21:23:07', '2021-06-14 21:23:07');
INSERT INTO `layanan` VALUES (8, 'Cinematic Wedding', 'Videography', '1623941211_wedding2.jfif', '1.300', 'durasi 3 menit + 1 menit cinematic', '2021-06-17 21:46:51', '2021-06-23 09:41:19');
INSERT INTO `layanan` VALUES (9, 'Documentation Wedding', 'Videography', '1624362224_wedding photo.jpg', '350', 'full dokumentasi pernikahan || 2 keping dvd || durasi 2 jam', '2021-06-22 18:43:44', '2021-06-22 18:43:44');
INSERT INTO `layanan` VALUES (10, 'Yearbook Small', 'Yearbook', '1625112808_buku tumpuk.jpg', '130', 'ukuran A4 | bonus 15 eksemplar, foto wisuda cetak 4r', '2021-07-01 11:13:28', '2021-07-01 11:16:36');
INSERT INTO `layanan` VALUES (11, 'Yearbook Medium', 'Yearbook', '1625113113_cover kelihatan semua.jpg', '180', 'ukuran 30x30 | bonus foto wisuda 4r | video catatan akhir sekolah', '2021-07-01 11:18:33', '2021-07-01 11:18:33');

-- ----------------------------
-- Table structure for log_lihat
-- ----------------------------
DROP TABLE IF EXISTS `log_lihat`;
CREATE TABLE `log_lihat`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pel` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_foto` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_vidio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of log_lihat
-- ----------------------------
INSERT INTO `log_lihat` VALUES (1, '4', '1', '1', NULL, NULL);
INSERT INTO `log_lihat` VALUES (2, '4', '1', '1', NULL, NULL);
INSERT INTO `log_lihat` VALUES (3, '2', '1', NULL, '2021-06-26 12:31:09', '2021-06-26 12:31:09');
INSERT INTO `log_lihat` VALUES (4, '2', NULL, '3', '2021-06-26 12:34:40', '2021-06-26 12:34:40');
INSERT INTO `log_lihat` VALUES (5, '2', NULL, '3', '2021-06-26 12:35:03', '2021-06-26 12:35:03');
INSERT INTO `log_lihat` VALUES (6, '2', '1', NULL, '2021-06-26 12:37:49', '2021-06-26 12:37:49');
INSERT INTO `log_lihat` VALUES (7, '2', '1', NULL, '2021-06-26 12:38:15', '2021-06-26 12:38:15');
INSERT INTO `log_lihat` VALUES (8, '2', NULL, '3', '2021-06-26 12:40:04', '2021-06-26 12:40:04');
INSERT INTO `log_lihat` VALUES (9, '2', NULL, '3', '2021-06-26 12:41:43', '2021-06-26 12:41:43');
INSERT INTO `log_lihat` VALUES (10, '2', NULL, '3', '2021-06-26 12:42:01', '2021-06-26 12:42:01');
INSERT INTO `log_lihat` VALUES (11, '2', NULL, '3', '2021-06-26 12:42:33', '2021-06-26 12:42:33');
INSERT INTO `log_lihat` VALUES (12, '2', NULL, '3', '2021-06-26 12:43:22', '2021-06-26 12:43:22');
INSERT INTO `log_lihat` VALUES (13, '2', NULL, '3', '2021-06-26 12:44:05', '2021-06-26 12:44:05');
INSERT INTO `log_lihat` VALUES (14, '2', '3', NULL, '2021-06-26 12:44:09', '2021-06-26 12:44:09');
INSERT INTO `log_lihat` VALUES (15, '2', NULL, '3', '2021-06-26 12:44:20', '2021-06-26 12:44:20');
INSERT INTO `log_lihat` VALUES (16, '2', NULL, '3', '2021-06-26 12:44:30', '2021-06-26 12:44:30');
INSERT INTO `log_lihat` VALUES (17, '2', NULL, '3', '2021-06-26 12:45:27', '2021-06-26 12:45:27');
INSERT INTO `log_lihat` VALUES (18, '2', '3', NULL, '2021-06-26 12:46:53', '2021-06-26 12:46:53');
INSERT INTO `log_lihat` VALUES (19, '2', '3', NULL, '2021-06-26 12:50:54', '2021-06-26 12:50:54');
INSERT INTO `log_lihat` VALUES (20, '2', '3', NULL, '2021-06-26 12:51:10', '2021-06-26 12:51:10');
INSERT INTO `log_lihat` VALUES (21, '2', '3', NULL, '2021-06-26 12:52:58', '2021-06-26 12:52:58');
INSERT INTO `log_lihat` VALUES (22, '2', '3', NULL, '2021-06-26 12:55:07', '2021-06-26 12:55:07');
INSERT INTO `log_lihat` VALUES (23, '2', '3', NULL, '2021-06-26 12:56:41', '2021-06-26 12:56:41');
INSERT INTO `log_lihat` VALUES (24, '2', '3', NULL, '2021-06-26 13:00:01', '2021-06-26 13:00:01');
INSERT INTO `log_lihat` VALUES (25, '2', '3', NULL, '2021-06-26 13:01:37', '2021-06-26 13:01:37');
INSERT INTO `log_lihat` VALUES (26, '2', '3', NULL, '2021-06-26 13:01:47', '2021-06-26 13:01:47');
INSERT INTO `log_lihat` VALUES (27, '2', '3', NULL, '2021-06-26 13:02:34', '2021-06-26 13:02:34');
INSERT INTO `log_lihat` VALUES (28, '2', '3', NULL, '2021-06-26 13:02:48', '2021-06-26 13:02:48');
INSERT INTO `log_lihat` VALUES (29, '2', '3', NULL, '2021-06-26 13:03:28', '2021-06-26 13:03:28');
INSERT INTO `log_lihat` VALUES (30, '2', '3', NULL, '2021-06-26 13:04:20', '2021-06-26 13:04:20');
INSERT INTO `log_lihat` VALUES (31, '2', '3', NULL, '2021-06-26 13:05:45', '2021-06-26 13:05:45');
INSERT INTO `log_lihat` VALUES (32, '2', '3', NULL, '2021-06-26 13:06:12', '2021-06-26 13:06:12');
INSERT INTO `log_lihat` VALUES (33, '2', '3', NULL, '2021-06-26 13:06:44', '2021-06-26 13:06:44');
INSERT INTO `log_lihat` VALUES (34, '2', '3', NULL, '2021-06-26 13:08:34', '2021-06-26 13:08:34');
INSERT INTO `log_lihat` VALUES (35, '2', '3', NULL, '2021-06-26 13:08:48', '2021-06-26 13:08:48');
INSERT INTO `log_lihat` VALUES (36, '2', '3', NULL, '2021-06-26 13:09:14', '2021-06-26 13:09:14');
INSERT INTO `log_lihat` VALUES (37, '2', '3', NULL, '2021-06-26 13:09:37', '2021-06-26 13:09:37');
INSERT INTO `log_lihat` VALUES (38, '2', '3', NULL, '2021-06-26 13:09:53', '2021-06-26 13:09:53');
INSERT INTO `log_lihat` VALUES (39, '2', '3', NULL, '2021-06-26 13:10:25', '2021-06-26 13:10:25');
INSERT INTO `log_lihat` VALUES (40, '2', '3', NULL, '2021-06-26 13:11:53', '2021-06-26 13:11:53');
INSERT INTO `log_lihat` VALUES (41, '2', '3', NULL, '2021-06-26 13:12:17', '2021-06-26 13:12:17');
INSERT INTO `log_lihat` VALUES (42, '2', '3', NULL, '2021-06-26 13:13:46', '2021-06-26 13:13:46');
INSERT INTO `log_lihat` VALUES (43, '2', '3', NULL, '2021-06-26 13:14:36', '2021-06-26 13:14:36');
INSERT INTO `log_lihat` VALUES (44, '2', '3', NULL, '2021-06-26 13:16:48', '2021-06-26 13:16:48');
INSERT INTO `log_lihat` VALUES (45, '2', '3', NULL, '2021-06-26 13:18:03', '2021-06-26 13:18:03');
INSERT INTO `log_lihat` VALUES (46, '2', '3', NULL, '2021-06-26 13:18:14', '2021-06-26 13:18:14');
INSERT INTO `log_lihat` VALUES (47, '2', '3', NULL, '2021-06-26 13:18:47', '2021-06-26 13:18:47');
INSERT INTO `log_lihat` VALUES (48, '2', '3', NULL, '2021-06-26 13:19:03', '2021-06-26 13:19:03');
INSERT INTO `log_lihat` VALUES (49, '2', '3', NULL, '2021-06-26 13:20:06', '2021-06-26 13:20:06');
INSERT INTO `log_lihat` VALUES (50, '2', '3', NULL, '2021-06-26 13:20:17', '2021-06-26 13:20:17');
INSERT INTO `log_lihat` VALUES (51, '2', '3', NULL, '2021-06-26 13:22:35', '2021-06-26 13:22:35');
INSERT INTO `log_lihat` VALUES (52, '2', '3', NULL, '2021-06-26 13:23:15', '2021-06-26 13:23:15');
INSERT INTO `log_lihat` VALUES (53, '2', '3', NULL, '2021-06-26 13:23:44', '2021-06-26 13:23:44');
INSERT INTO `log_lihat` VALUES (54, '2', '3', NULL, '2021-06-26 13:24:10', '2021-06-26 13:24:10');
INSERT INTO `log_lihat` VALUES (55, '2', '3', NULL, '2021-06-26 13:24:28', '2021-06-26 13:24:28');
INSERT INTO `log_lihat` VALUES (56, '2', '3', NULL, '2021-06-26 13:24:38', '2021-06-26 13:24:38');
INSERT INTO `log_lihat` VALUES (57, '2', '3', NULL, '2021-06-26 13:25:20', '2021-06-26 13:25:20');
INSERT INTO `log_lihat` VALUES (58, '2', '3', NULL, '2021-06-26 13:27:11', '2021-06-26 13:27:11');
INSERT INTO `log_lihat` VALUES (59, '2', '3', NULL, '2021-06-26 13:27:26', '2021-06-26 13:27:26');
INSERT INTO `log_lihat` VALUES (60, '2', '3', NULL, '2021-06-26 13:28:05', '2021-06-26 13:28:05');
INSERT INTO `log_lihat` VALUES (61, '2', '3', NULL, '2021-06-26 13:28:26', '2021-06-26 13:28:26');
INSERT INTO `log_lihat` VALUES (62, '2', '3', NULL, '2021-06-26 13:28:35', '2021-06-26 13:28:35');
INSERT INTO `log_lihat` VALUES (63, '2', '3', NULL, '2021-06-26 13:29:04', '2021-06-26 13:29:04');
INSERT INTO `log_lihat` VALUES (64, '2', '3', NULL, '2021-06-26 13:34:03', '2021-06-26 13:34:03');
INSERT INTO `log_lihat` VALUES (65, '2', '3', NULL, '2021-06-26 13:34:41', '2021-06-26 13:34:41');
INSERT INTO `log_lihat` VALUES (66, '2', '3', NULL, '2021-06-26 13:34:50', '2021-06-26 13:34:50');
INSERT INTO `log_lihat` VALUES (67, '2', '3', NULL, '2021-06-26 13:35:09', '2021-06-26 13:35:09');
INSERT INTO `log_lihat` VALUES (68, '2', '3', NULL, '2021-06-26 13:37:47', '2021-06-26 13:37:47');
INSERT INTO `log_lihat` VALUES (69, '2', '3', NULL, '2021-06-26 13:38:03', '2021-06-26 13:38:03');
INSERT INTO `log_lihat` VALUES (70, '2', '3', NULL, '2021-06-26 13:38:30', '2021-06-26 13:38:30');
INSERT INTO `log_lihat` VALUES (71, '2', '3', NULL, '2021-06-26 13:42:48', '2021-06-26 13:42:48');
INSERT INTO `log_lihat` VALUES (72, '2', '3', NULL, '2021-06-26 13:43:28', '2021-06-26 13:43:28');
INSERT INTO `log_lihat` VALUES (73, '2', '3', NULL, '2021-06-26 13:43:41', '2021-06-26 13:43:41');
INSERT INTO `log_lihat` VALUES (74, '2', '3', NULL, '2021-06-26 13:45:15', '2021-06-26 13:45:15');
INSERT INTO `log_lihat` VALUES (75, '2', '3', NULL, '2021-06-26 13:46:00', '2021-06-26 13:46:00');
INSERT INTO `log_lihat` VALUES (76, '2', '3', NULL, '2021-06-26 13:46:26', '2021-06-26 13:46:26');
INSERT INTO `log_lihat` VALUES (77, '2', '3', NULL, '2021-06-26 13:47:11', '2021-06-26 13:47:11');
INSERT INTO `log_lihat` VALUES (78, '2', '3', NULL, '2021-06-26 13:48:32', '2021-06-26 13:48:32');
INSERT INTO `log_lihat` VALUES (79, '2', '3', NULL, '2021-06-26 13:48:47', '2021-06-26 13:48:47');
INSERT INTO `log_lihat` VALUES (80, '2', '3', NULL, '2021-06-26 13:49:05', '2021-06-26 13:49:05');
INSERT INTO `log_lihat` VALUES (81, '2', '3', NULL, '2021-06-26 13:49:19', '2021-06-26 13:49:19');
INSERT INTO `log_lihat` VALUES (82, '2', '3', NULL, '2021-06-26 13:50:09', '2021-06-26 13:50:09');
INSERT INTO `log_lihat` VALUES (83, '2', '3', NULL, '2021-06-26 13:50:33', '2021-06-26 13:50:33');
INSERT INTO `log_lihat` VALUES (84, '2', '3', NULL, '2021-06-26 13:50:55', '2021-06-26 13:50:55');
INSERT INTO `log_lihat` VALUES (85, '2', '3', NULL, '2021-06-26 13:51:11', '2021-06-26 13:51:11');
INSERT INTO `log_lihat` VALUES (86, '2', '3', NULL, '2021-06-26 13:51:37', '2021-06-26 13:51:37');
INSERT INTO `log_lihat` VALUES (87, '2', '3', NULL, '2021-06-26 13:52:01', '2021-06-26 13:52:01');
INSERT INTO `log_lihat` VALUES (88, '4', NULL, '3', '2021-06-27 17:48:11', '2021-06-27 17:48:11');
INSERT INTO `log_lihat` VALUES (89, '4', '3', NULL, '2021-06-27 17:48:29', '2021-06-27 17:48:29');
INSERT INTO `log_lihat` VALUES (90, '4', '1', NULL, '2021-06-27 20:44:20', '2021-06-27 20:44:20');
INSERT INTO `log_lihat` VALUES (91, '4', '3', NULL, '2021-06-27 20:44:39', '2021-06-27 20:44:39');
INSERT INTO `log_lihat` VALUES (92, '2', NULL, '3', '2021-06-29 14:37:16', '2021-06-29 14:37:16');
INSERT INTO `log_lihat` VALUES (93, '2', '1', NULL, '2021-06-29 14:37:41', '2021-06-29 14:37:41');
INSERT INTO `log_lihat` VALUES (94, '2', '1', NULL, '2021-06-29 14:38:00', '2021-06-29 14:38:00');
INSERT INTO `log_lihat` VALUES (95, '2', '1', NULL, '2021-06-29 14:38:15', '2021-06-29 14:38:15');
INSERT INTO `log_lihat` VALUES (96, '2', '1', NULL, '2021-06-29 14:38:51', '2021-06-29 14:38:51');
INSERT INTO `log_lihat` VALUES (97, '2', '4', NULL, '2021-07-02 15:12:51', '2021-07-02 15:12:51');
INSERT INTO `log_lihat` VALUES (98, '2', '4', NULL, '2021-07-02 15:13:37', '2021-07-02 15:13:37');
INSERT INTO `log_lihat` VALUES (99, '2', '4', NULL, '2021-07-02 15:13:50', '2021-07-02 15:13:50');
INSERT INTO `log_lihat` VALUES (100, '2', '4', NULL, '2021-07-02 15:16:10', '2021-07-02 15:16:10');
INSERT INTO `log_lihat` VALUES (101, '2', '4', NULL, '2021-07-02 15:16:14', '2021-07-02 15:16:14');
INSERT INTO `log_lihat` VALUES (102, '2', '4', NULL, '2021-07-02 15:50:20', '2021-07-02 15:50:20');
INSERT INTO `log_lihat` VALUES (103, '2', '4', NULL, '2021-07-02 15:50:37', '2021-07-02 15:50:37');
INSERT INTO `log_lihat` VALUES (104, '2', NULL, '3', '2021-07-02 15:51:21', '2021-07-02 15:51:21');
INSERT INTO `log_lihat` VALUES (105, '2', NULL, '3', '2021-07-02 15:51:32', '2021-07-02 15:51:32');
INSERT INTO `log_lihat` VALUES (106, '2', NULL, '3', '2021-07-02 15:51:47', '2021-07-02 15:51:47');
INSERT INTO `log_lihat` VALUES (107, '2', NULL, '4', '2021-07-02 15:52:19', '2021-07-02 15:52:19');
INSERT INTO `log_lihat` VALUES (108, '2', NULL, '4', '2021-07-02 15:52:34', '2021-07-02 15:52:34');
INSERT INTO `log_lihat` VALUES (109, '2', NULL, '4', '2021-07-02 15:52:46', '2021-07-02 15:52:46');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (3, '2021_03_18_063342_admin', 1);
INSERT INTO `migrations` VALUES (6, '2021_03_18_064747_pelanggan', 2);
INSERT INTO `migrations` VALUES (14, '2014_10_12_000000_create_users_table', 3);
INSERT INTO `migrations` VALUES (15, '2014_10_12_100000_create_password_resets_table', 3);
INSERT INTO `migrations` VALUES (16, '2021_03_18_064722_layanan', 3);
INSERT INTO `migrations` VALUES (17, '2021_03_18_064737_pemesanan', 3);
INSERT INTO `migrations` VALUES (18, '2021_03_18_064801_yearbook', 3);
INSERT INTO `migrations` VALUES (19, '2021_03_18_064817_port_foto', 3);
INSERT INTO `migrations` VALUES (20, '2021_03_18_064834_port_vidio', 3);
INSERT INTO `migrations` VALUES (21, '2021_03_18_064850_komentar', 3);
INSERT INTO `migrations` VALUES (22, '2021_03_18_064901_rating', 3);
INSERT INTO `migrations` VALUES (23, '2021_03_18_065039_review', 3);
INSERT INTO `migrations` VALUES (24, '2021_03_18_065055_log_lihat', 3);
INSERT INTO `migrations` VALUES (25, '2021_05_25_055205_kategori_port', 3);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for pemesanan
-- ----------------------------
DROP TABLE IF EXISTS `pemesanan`;
CREATE TABLE `pemesanan`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pesan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_lay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_pel` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tgl_pesan` date NULL DEFAULT NULL,
  `jmlh_yb` int(11) NULL DEFAULT NULL,
  `status_pesan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ket_pesan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `bukti_pesan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tipe_bayar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id_pesan`(`id_pesan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pemesanan
-- ----------------------------
INSERT INTO `pemesanan` VALUES (1, 'FT001', '2', '4', '2020-06-10', NULL, '4', '', '1624791650_dd.png', NULL, '2020-08-10 15:51:31', '2021-06-27 18:02:33');
INSERT INTO `pemesanan` VALUES (2, 'FT002', '2', '4', '2021-06-13', NULL, '2', '', NULL, NULL, '2021-05-12 20:58:19', '2021-06-17 23:11:01');
INSERT INTO `pemesanan` VALUES (3, 'FT003', '5', '6', '2021-06-17', NULL, '0', '', NULL, NULL, '2021-04-16 20:59:45', '2021-06-16 20:59:45');
INSERT INTO `pemesanan` VALUES (4, 'FT004', '6', '6', '2021-06-17', NULL, '7', '', NULL, NULL, '2021-03-16 21:20:10', '2021-06-23 12:23:59');
INSERT INTO `pemesanan` VALUES (5, 'FT005', '7', '2', '2021-06-18', NULL, '10', '', NULL, NULL, '2021-02-16 23:45:23', '2021-06-22 14:56:39');
INSERT INTO `pemesanan` VALUES (6, 'FT006', '7', '2', '2021-06-24', NULL, '4', NULL, '1624595888_piano chord.jpg', NULL, '2021-01-17 00:03:54', '2021-06-25 11:43:31');
INSERT INTO `pemesanan` VALUES (8, 'YB007', '3', '2', '2021-06-25', 250, '10', 'anying', '1624601062_av.jpg', '1', '2020-09-17 08:43:27', '2021-06-25 13:05:00');
INSERT INTO `pemesanan` VALUES (9, 'YB009', '3', '4', '2021-06-24', 100, '4', '', NULL, '3', '2020-09-17 10:54:03', '2021-06-17 23:47:16');
INSERT INTO `pemesanan` VALUES (10, 'YB0010', '3', '4', '2021-06-24', 500, '4', '', NULL, '2', '2021-09-17 10:58:04', '2021-07-01 10:56:06');
INSERT INTO `pemesanan` VALUES (11, 'YB0011', '3', '4', '2021-06-30', 200, '10', '', NULL, '2', '2020-10-17 11:02:14', '2021-06-17 11:02:14');
INSERT INTO `pemesanan` VALUES (12, 'YB0012', '3', '4', '2021-06-30', 200, '4', 'SMA N 3 KEDIRI', NULL, '1', '2021-06-01 11:02:25', '2021-06-17 11:02:25');
INSERT INTO `pemesanan` VALUES (13, 'YB0013', '3', '4', '2021-06-22', 100, '10', 'SMA N 3 KEDIRI', NULL, '1', '2021-06-17 12:26:51', '2021-06-23 10:00:24');
INSERT INTO `pemesanan` VALUES (15, 'YB0014', '3', '4', '2021-06-22', 100, '1', 'SMA N 3 KEDIRI', NULL, '2', '2021-06-17 12:28:22', '2021-06-17 12:28:22');
INSERT INTO `pemesanan` VALUES (16, 'YB0016', '3', '4', '2021-06-22', 100, '10', 'SMA N 3 KEDIRI', NULL, '2', '2021-06-17 12:29:05', '2021-06-17 12:29:05');
INSERT INTO `pemesanan` VALUES (17, 'VD0017', '8', '2', '2021-06-26', NULL, '4', '', '1624416713_WhatsApp Image 2021-06-15 at 09.03.43.jpeg', NULL, '2021-06-17 23:50:24', '2021-06-23 09:53:07');
INSERT INTO `pemesanan` VALUES (18, 'YB0018', '3', '2', NULL, 500, '7', 'njajal tipe pembayaran', NULL, '2', '2021-06-23 10:57:09', '2021-06-23 10:57:09');
INSERT INTO `pemesanan` VALUES (19, 'FT0019', '9', '2', '2021-06-30', NULL, '0', NULL, NULL, NULL, '2021-06-23 12:43:25', '2021-06-23 12:43:25');
INSERT INTO `pemesanan` VALUES (20, 'VD0020', '9', '2', '2021-06-29', NULL, '4', 'nyoba ket pesan', NULL, NULL, '2020-08-23 12:44:25', '2021-06-23 12:44:25');
INSERT INTO `pemesanan` VALUES (21, 'YB0021', '3', '2', NULL, 56, '10', 'penteng apik', NULL, '3', '2020-08-25 13:16:42', '2021-06-25 13:16:42');
INSERT INTO `pemesanan` VALUES (22, 'YB0022', '3', '2', NULL, 402, '10', 'SMA N 3 KEDIRI', '1624793512_WhatsApp Image 2021-06-15 at 09.03.43 (1).jpeg', '1', '2020-07-27 18:04:48', '2021-06-27 18:33:19');
INSERT INTO `pemesanan` VALUES (23, 'VD0023', '3', '16', NULL, 557, '10', 'SMK N 1 NGASEM', '1625112553_WhatsApp Image 2021-06-30 at 16.29.26.jpeg', '2', '2021-07-01 10:35:21', '2021-07-01 11:24:36');

-- ----------------------------
-- Table structure for port_foto
-- ----------------------------
DROP TABLE IF EXISTS `port_foto`;
CREATE TABLE `port_foto`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_foto` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `foto` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ket_foto` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `kategori_foto` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of port_foto
-- ----------------------------
INSERT INTO `port_foto` VALUES (4, 'SMAGA Yearbook Committe', '1625132194_DSCF1821.JPG', 'foto panitia yearbook SMAN 3 KEDIRI 2021', '4', '2021-07-01 16:36:34', '2021-07-01 16:36:34');

-- ----------------------------
-- Table structure for port_vidio
-- ----------------------------
DROP TABLE IF EXISTS `port_vidio`;
CREATE TABLE `port_vidio`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_vid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `vidio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ket_vid` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `kategori_vid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of port_vidio
-- ----------------------------
INSERT INTO `port_vidio` VALUES (1, 'The Wedding Alex & Anna', 'https://www.youtube.com/watch?v=Gtv64ehkgFs', 'cekl', '1', '2021-05-31 23:39:56', '2021-06-04 09:42:43');
INSERT INTO `port_vidio` VALUES (3, '1st Birthday of Zoe', 'https://www.youtube.com/watch?v=3PTgJvx-8mA', 'ultah pertama angeline dengan tema love disney', '3', '2021-07-01 16:20:52', '2021-07-01 16:25:00');
INSERT INTO `port_vidio` VALUES (4, 'Milah Go Unimugo', 'https://www.youtube.com/watch?v=5v-RYOJcBIU', 'Happy birthday Unimugo, Wish u all the best', '3', '2021-07-01 16:26:23', '2021-07-01 16:26:23');

-- ----------------------------
-- Table structure for rating
-- ----------------------------
DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pel` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_foto` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_vidio` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `rating` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rating
-- ----------------------------
INSERT INTO `rating` VALUES (1, '2', '3', NULL, '4', NULL, NULL);
INSERT INTO `rating` VALUES (2, '3', '3', NULL, '3', NULL, NULL);
INSERT INTO `rating` VALUES (3, '2', '4', NULL, '4.8', '2021-07-02 15:50:37', '2021-07-02 15:50:37');
INSERT INTO `rating` VALUES (4, '2', NULL, '3', '4.7', '2021-07-02 15:51:46', '2021-07-02 15:51:46');
INSERT INTO `rating` VALUES (5, '2', NULL, '4', '2.5', '2021-07-02 15:52:34', '2021-07-02 15:52:34');
INSERT INTO `rating` VALUES (6, '2', NULL, '4', '1.4', '2021-07-02 15:52:46', '2021-07-02 15:52:46');

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pesan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_pel` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `isi_rev` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of review
-- ----------------------------
INSERT INTO `review` VALUES (5, '5', '2', 'njajal njir', '2021-06-22 14:56:39', '2021-06-22 14:56:39');
INSERT INTO `review` VALUES (6, '6', '2', 'check', '2021-06-22 17:32:58', '2021-06-22 17:32:58');
INSERT INTO `review` VALUES (8, '8', '2', 'coba review yb lagi', '2021-06-23 13:40:55', '2021-06-23 13:40:55');
INSERT INTO `review` VALUES (9, '8', '2', 'tes review yearbook', '2021-06-25 13:05:00', '2021-06-25 13:05:00');
INSERT INTO `review` VALUES (10, '22', '4', 'mantap ih. sebulan yg jadi awokwok', '2021-06-27 18:33:19', '2021-06-27 18:33:19');
INSERT INTO `review` VALUES (11, '23', '16', 'waaw warna tajem, bonusnya juga sip. packaging juga mantep', '2021-07-01 11:24:36', '2021-07-01 11:24:36');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `alamat` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `jk` enum('Perempuan','Laki - Laki') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tglhr` date NULL DEFAULT NULL,
  `role` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Eriawan Santis', '066544322123', 'Ngagel - Surabaya', NULL, NULL, 'owner', 'eriawane@gmail.com', NULL, '$2y$10$NDx8icNOdrRjGis6VsRRMe57feaE0WKT86Dzj3l2VMKEiHUPZtrzS', 'YcSGZ3q3HbzJ5H4LpAzcq2JHPa5WxcgmIppG2QscZVt5LVJsF7thAdXPzfjQ', '2021-05-31 01:01:46', '2021-06-10 08:49:03');
INSERT INTO `users` VALUES (2, 'silvanda oktavia', '086798766567', 'kediri indonesia', 'Perempuan', '2004-12-12', 'pengunjung', 'member1@gmail.com', NULL, '$2y$10$rDU1mKtvNLNgjYoPl6FYvOQsdIRF931SKHzzwblEXJB/iAPnTxHzO', 'sZeTrZgZCLpUqiWrdMCE4CBznOYLFDuOJ8s54heJvtzmagn9pxHUS3xIIury', '2021-05-31 16:59:28', '2021-05-31 16:59:28');
INSERT INTO `users` VALUES (3, 'oky marsel', '085708264091', 'Dsn. Grompol - Ds. Ngebrak - Kec. Gampengrejo - KEDIRI', 'Laki - Laki', '1999-12-31', 'pengunjung', 'member2@gmail.com', NULL, '$2y$10$bEL.M2DsMNwEJN/bUNkRC.cVCD1koGBruGi5YsXg6fzOz1.RGLLeS', 'c6bZLkdKDwfIXkXPx6YoXkHDrTUDwK7eaOS5cKz08Em94lMbLKVI4Icv0VQ6', '2021-05-31 22:27:23', '2021-05-31 22:27:23');
INSERT INTO `users` VALUES (4, 'RIsfandi Bayu', '089789677555', 'pare - Kediri', 'Laki - Laki', '1995-01-05', 'pengunjung', 'member3@gmail.com', NULL, '$2y$10$RAKKibTn6OWGYW0tmkFFrOMCG9nliVq791Fw2JO.aKVpg11/wkUMy', 'amXQKUDG8zpeZ0Mv7dQ23PsCHd9Gf0SZUQPudKXFIDAyTlOSAuLLbalU9u3b', '2021-05-31 22:37:21', '2021-05-31 22:37:21');
INSERT INTO `users` VALUES (5, 'Mantis de Glamoure', '098788987677', 'jombang - jawa timur', 'Perempuan', '2000-09-11', 'pengunjung', 'mantis@gmail.com', NULL, '$2y$10$POXB50qK.26N6Z9fbbh2uOSED/T/TEX3t0WF3U2j7D5Q/IWDHjyL6', 'ZqE4LnKszBPCgmZBjEIYHDzdwIzbANEwPST2nyzftQZqGVqin73RdwQzgK0a', '2021-05-31 23:07:25', '2021-05-31 23:07:25');
INSERT INTO `users` VALUES (6, 'juneadi', '098788986533', 'ngibrik grimpil', 'Laki - Laki', '2000-06-11', 'pengunjung', 'member4@gmail.com', NULL, '$2y$10$oYN2LJoMwkIMossKKvF15e3W1yxBEVtPmXRyToMuz7UXSHaOZFIAm', 'sPbris7LUp4oNBZ3y7e29UerkhuFfKLlnEOt00Gp1SaqKkEka5yj7Wgi9vyK', '2021-06-02 17:38:51', '2021-06-02 17:38:51');
INSERT INTO `users` VALUES (11, 'Rosdiana Safitri', '086798766567', 'srr gfg aaa ewerwrf fsf', NULL, NULL, 'admin layanan', 'layanan1@gmail.com', NULL, '$2y$10$NDx8icNOdrRjGis6VsRRMe57feaE0WKT86Dzj3l2VMKEiHUPZtrzS', 'znixCH3SMdOkBqkNdRuDGRK0PIzFx49pJ9sUTOGpF596sl4f7SU5itmDizvJ', '2021-06-02 21:05:26', '2021-06-02 21:05:26');
INSERT INTO `users` VALUES (12, 'silvanda oktavia', '098788987677', 'yaowoh', NULL, NULL, 'admin portofolio', 'port1@gmail.com', NULL, '$2y$10$kR791vJ4B99XknhzywSa2O1UhOWJD4R.XFAML58IAMVyz3IF3GiPS', NULL, '2021-06-03 11:51:58', '2021-06-04 08:12:37');
INSERT INTO `users` VALUES (13, 'Jono Joni', '086798766567', 'kediri - kertosono', NULL, NULL, 'admin layanan', 'layanan2@gmail.com', NULL, '$2y$10$rMpBTYcSHXrEdkbjX0pMsOvNTdJpxw6GhGnhZNZjPvNJI2OAL2sh6', 'gmayurhyuWoXgNByMOIMTws6fin8e0wqsvxPIBi5fJt3HcsTPyEvHc4sREIP', '2021-06-10 08:47:47', '2021-06-10 08:47:47');
INSERT INTO `users` VALUES (14, 'Jinny oh Jinny', '089786555432', 'Malang - Jatim', NULL, NULL, 'admin portofolio', 'port2@gmail.com', NULL, '$2y$10$YWUFGKZR0g4X0NwZtTK8LO3DX38a.mM1vbqX5LAbpYC2Bb.tGBh6S', 'blXMMBOevvyK9eZp7gPYk6S0SdN3y6vVIbuRMyVISaMKXJHsvAMbzbFtopfZ', '2021-06-10 08:48:22', '2021-06-10 08:48:22');
INSERT INTO `users` VALUES (15, 'silvanda oktavia', '089789677555', 'kdr - jbg', 'Perempuan', '2001-10-23', 'pengunjung', 'member5@gmail.com', NULL, '$2y$10$ZrlDOaC5HgV3yHm6c.6J4ecTb2Alou2G/pELA.MO2xlsRTM84D9mu', 'Ow4gFicqFfEgSDmCbyNZGqXHcL8ugfqjLB5XABR61ogPvuiKsbqgXi0ND5GA', '2021-06-13 00:15:56', '2021-06-13 00:15:56');
INSERT INTO `users` VALUES (16, 'Adam Levin', '089765566455', 'Jl Kediri - Kertosono, No 6, Kabupaten Kediiri', 'Laki - Laki', '1986-06-17', 'pengunjung', 'member6@gmail.com', NULL, '$2y$10$JjGugkrLHebrae6TwDLOA.A6VzZfXO5nz./7o97fK1jlUIJ8z/QjK', 'ubkyO4OtV13SgfNitB634osRcVe5f9imvJvlis6uTLWSiShDAcJRXckt4TVu', '2021-07-01 10:20:02', '2021-07-01 10:20:02');
INSERT INTO `users` VALUES (17, 'cok', '081291291212', 'Cok jancok', NULL, NULL, 'admin layanan', 'asu@gmail.com', NULL, '$2y$10$1QuR/vdKNt5bi8L5Ff8C5uRHVR3d4JCFx6A8BLPS7jq0Pxx.5piMm', NULL, '2021-07-02 15:00:10', '2021-07-02 15:00:10');

-- ----------------------------
-- Table structure for yearbook
-- ----------------------------
DROP TABLE IF EXISTS `yearbook`;
CREATE TABLE `yearbook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kelas_yb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama_yb` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `jk_yb` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tlhr_yb` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tgllhr_yb` date NULL DEFAULT NULL,
  `alamat_yb` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `sosmed_yb` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `bio_yb` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `id_pesan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yearbook
-- ----------------------------
INSERT INTO `yearbook` VALUES (28, 'IPS 5', 'junaedi santoso', 'Laki - Laki', 'kediri', '1999-06-09', 'Jl. Kediri kertoso, No 12, Kabupaten kediri', '@junsans', 'ayo menyerah ngapain semangat. beban keluarga harus tetap ber malas - malas an, dan tetap menjadi beban', 'YB0012', '2021-06-25 00:49:28', '2021-06-25 00:49:28');
INSERT INTO `yearbook` VALUES (29, 'IPS 5', 'Adnan Rizaldi', 'Laki - Laki', 'Nganjuk', '2000-09-09', 'Rejoso - nganjuk', '@bukananda', 'bojomu semangatku', 'YB0012', '2021-06-25 00:49:28', '2021-06-25 00:49:28');
INSERT INTO `yearbook` VALUES (30, 'IPS 5', 'Frederica Jr.', 'Perempuan', 'kediri', '2000-12-12', 'ngantang - malang', 'ig @chachajr', 'ikan hiu makan bekicot', 'YB0012', '2021-06-25 00:49:28', '2021-06-25 00:49:28');
INSERT INTO `yearbook` VALUES (31, 'IPS 5', 'Oky Juneva', 'Laki - Laki', 'kediri', '2000-06-04', 'grompol - gampengrejo', 'ig @oke.junn', 'nothing', 'YB0012', '2021-06-25 01:14:29', '2021-06-25 01:14:29');
INSERT INTO `yearbook` VALUES (32, 'MIPA 1', 'Angelica Montiwa', 'Perempuan', 'kediri', '2000-12-12', 'kampungdalem gang 5 - kota kediri', 'ig @angelicamm', 'aku tanpamu ngisingku angel', 'YB009', '2021-06-25 01:18:09', '2021-06-25 01:18:09');
INSERT INTO `yearbook` VALUES (33, 'MIPA 1', 'Afifuddin', 'Laki - Laki', 'Kediri', '2001-09-13', 'pohrubuh - semen - kediri', 'ig @apeff', 'anjay marganjar margunjao', 'YB0012', '2021-06-25 11:34:10', '2021-06-25 11:34:10');
INSERT INTO `yearbook` VALUES (34, 'MIPA 1', 'Eriawan Santoso', 'Laki - Laki', 'Kediri', '2001-12-12', 'gampengrejo - kediri', 'fb eri sann', 'anjay bisa ga', 'YB0012', '2021-06-26 10:47:18', '2021-06-26 10:47:18');
INSERT INTO `yearbook` VALUES (40, 'MIPA 2', 'Bertha Amanda', 'Perempuan', 'Kediri', '2021-06-02', 'kediri', '@bertnarbear33', 'coba validasi jumlah pemesanan', 'YB009', '2021-06-26 11:40:29', '2021-06-26 11:40:29');
INSERT INTO `yearbook` VALUES (41, 'MIPA 1', 'junaedi santoso', 'Laki - Laki', 'kediri', '2021-06-15', 'aa', '@junsans', 'aa', 'YB009', '2021-06-30 13:06:49', '2021-06-30 13:06:49');

SET FOREIGN_KEY_CHECKS = 1;
