<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::resource('/','WelcomeController');

Route::get('/login', function () {return view('/pengunjung/login');});
Route::get('/registeruser', function () {return view('/pengunjung/registeruser');});

Route::resource('/layvideo', 'LayvideoController');
Route::resource('/layfoto', 'LayfotoController');
Route::resource('/laybook', 'LaybookController');
Route::get('/detilfoto/{id}', 'Port_fotoController@detail');
Route::get('/detilvidio/{id}', 'Port_vidioController@detail');
Route::resource('/layfoto', 'LayfotoController');
Route::resource('/laybook', 'LaybookController');
Route::resource('/layvideo', 'LayvideoController');
Route::get('/multienty', 'LaybookController@cekyb');
Route::post('/multientry', 'LaybookController@bio');
Route::post('/multientryb', 'LaybookController@submityb');
Route::get('/desbio:{id}', 'LaybookController@destroy');
Route::get('edibio','LaybookController@edibio')->name('edibio');
Route::POST('updabio','LaybookController@updabio')->name('updabio');

Auth::routes();

Route::group(['middleware' => ['auth','CheckRole:pengunjung']],
    function(){
        Route::get('/welcomepel','WelcomeController@indexpel');
        Route::get('/welcomepel/detilfotpel/{id}', 'Port_fotoController@detailpel');
        Route::POST('/komentar_foto', 'Port_fotoController@postkomentar');
        Route::POST('/rating_foto', 'Port_fotoController@addratefoto');
        // Route::post('/welcomepel', 'WelcomeController@postlog');
        Route::get('/welcomepel/detilvidpel/{id}', 'Port_vidioController@detailpel');
        Route::post('/komentar_vidio', 'Port_vidioController@postkomentar');
        Route::POST('/rating_vidio', 'Port_vidioController@addratevidio');
        Route::get('/layfotopel', 'LayfotoController@indexpel');
        Route::get('/laybookpel', 'LaybookController@indexpel');
        Route::get('/layvideopel', 'LayvideoController@indexpel');
        Route::get('/laypesanfoto/{id}', 'LayfotoController@indexpesan');
        Route::post('/laypesanfoto/{id}', 'LayfotoController@postpesan');
        Route::get('/laypesanvid/{id}', 'LayvideoController@indexpesan');
        Route::post('/laypesanvid/{id}', 'LayvideoController@postpesan');
        Route::get('/laypesanbook/{id}', 'LaybookController@indexpesan');
        Route::post('/laypesanbook/{id}', 'LaybookController@postpesan');
        Route::get('/order','PemesananController@order');
        Route::get('/detilorder/{id}', 'PemesananController@detilorder');
        Route::get('/detilorderyb/{id}', 'PemesananController@detilorderyb');
        Route::POST('addreview','ReviewController@store')->name('addreview');
        // Route::resource('/rivorder/{id}','ReviewController');
        Route::get('editgl','PemesananController@editgl')->name('editgl');
        Route::POST('updatgl','PemesananController@updatgl')->name('updatgl');
        Route::POST('addbukti','PemesananController@addbukti')->name('addbukti');
        Route::POST('updabukti','PemesananController@updabukti')->name('updabukti');
        Route::get('editipe','PemesananybController@editipe')->name('editipe');
        Route::POST('updatipe','PemesananybController@updatipe')->name('updatipe');
        Route::get('/pelmultienty', 'LaybookController@pelcekyb');
        Route::post('/pelmultientry', 'LaybookController@pelbio');
        Route::post('/pelmultientryb', 'LaybookController@pelsubmityb');
        Route::get('/peldesbio:{id}', 'LaybookController@peldestroy');
        Route::get('peledibio','LaybookController@peledibio')->name('peledibio');
        Route::POST('pelupdabio','LaybookController@pelupdabio')->name('pelupdabio');
        
        

});


Route::group(['middleware' => ['auth','CheckRole:owner']],
    function(){
    Route::get('/dashboardowner', 'HomeController@index');
    Route::get('/laporan', 'HomeController@laporan')->name('laporan');
    // Route::resource('laporan', 'HomeController');
    Route::resource('/user','UserController');
    Route::resource('/profil','ProfilController');
    Route::POST('/updaprofilowner','ProfilController@updaprofilowner')->name('updaprofilowner');
});


Route::group(['middleware' => ['auth','CheckRole:admin layanan']],
function(){
    Route::get('/dashboardlayanan', 'HomeController@indexlay');
    Route::resource('/layanan','LayananController');
    Route::get('/deslayanan:{id}', 'LayananController@destroy');
    Route::resource('/pemesanan','PemesananController');
    Route::resource('/pemesananyb','PemesananybController');
    Route::resource('/bioyb','YearbookController');
    Route::resource('/reviewe','ReviewController');
    Route::resource('/profiladmlay','ProfilController');
    // Route::get('/profiladmlay','UserController@profil');
    Route::get('/export_excel/excel', 'YearbookController@excel')->name('export_excel.excel');
    Route::POST('updaprofillay','ProfilController@updaprofillay')->name('updaprofillay');
});

Route::group(['middleware' => ['auth','CheckRole:admin portofolio']],
    function(){
        Route::resource('/profiladmport','ProfilController');
        Route::get('/dashboardport', 'HomeController@indexport');
        Route::resource('/port_foto','Port_fotoController');
        Route::resource('/port_vidio','Port_vidioController');
        Route::resource('/kategori','Kategori_portController');
        Route::get('/komentar_foto','KomentarController@indexfoto');
        Route::get('/komentar_vidio','KomentarController@indexvidio');
        Route::get('/rating_foto','RatingController@indexfoto');
        Route::get('/rating_vidio','RatingController@indexvidio');
        Route::get('/komentar_foto:{id}', 'KomentarController@verif_fot');
        Route::get('/komentar_vidio:{id}', 'KomentarController@verif_vid');
        Route::get('/desfoto:{id}', 'Port_fotoController@destroy');
        Route::get('/desvidio:{id}', 'Port_vidioController@destroy');
        Route::get('/deskomvid:{id}', 'KomentarController@destroyvid');
        Route::get('/deskomfot:{id}', 'KomentarController@destroyfot');
        Route::POST('updaprofilport','ProfilController@updaprofilport')->name('updaprofilport');
    });