<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pemesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_pesan')->nullable();
            $table->string('id_lay')->nullable();
            $table->string('id_pel')->nullable();
            $table->date('tgl_pesan')->nullable();
            $table->string('status_pesan')->nullable();
            $table->string('tipe_bayar')->nullable();
            $table->integer('jmlh_yb')->nullable();
            $table->longText('ket_pesan')->nullable();
            $table->string('bukti_pesan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan');
    }
}
