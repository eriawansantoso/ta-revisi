<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortFoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('port_foto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_foto')->nullable();
            $table->string('foto')->nullable();
            $table->longText('ket_foto')->nullable();
            $table->string('kategori_foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('port_foto');
    }
}
