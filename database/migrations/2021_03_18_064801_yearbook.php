<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Yearbook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yearbook', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kelas_yb')->nullable();
            $table->string('nama_yb')->nullable();
            $table->string('jk_yb')->nullable();
            $table->string('tlhr_yb')->nullable();
            $table->date('tgllhr_yb')->nullable();
            $table->longText('alamat_bio')->nullable();
            $table->string('sosmed_yb')->nullable();
            $table->string('id_pesan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yearbook');
    }
}
