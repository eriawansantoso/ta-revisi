<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kelas_bio')->nullable();
            $table->string('nama_bio')->nullable();
            $table->string('jk_bio')->nullable();
            $table->string('tlhr_bio')->nullable();
            $table->date('tgllhr_bio')->nullable();
            $table->longText('alamat_bio')->nullable();
            $table->string('sosmed_bio')->nullable();
            $table->string('id_pesan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio');
    }
}
