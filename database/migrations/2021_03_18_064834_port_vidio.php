<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortVidio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('port_vidio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_vid')->nullable();
            $table->string('vidio')->nullable();
            $table->longText('ket_vid')->nullable();
            $table->string('kategori_vid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('port_vidio');
    }
}
