<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori_port extends Model
{
    protected $table = 'kategori_port';
    protected $fillable = [
        'nama',
    ];
}
