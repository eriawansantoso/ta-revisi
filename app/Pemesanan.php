<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = 'pemesanan';
    protected $guarded = ['id'];
    protected $fillable = [
        'id_pesan',
        'id_lay',
        'id_pel',
        'tgl_pesan',
        'jmlh_yb',
        'status_pesan',
        'ket_pesan',
        'bukti_pesan',
        'tipe_bayar'

    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function layanan(){
        return $this->belongsTo(Layanan::class);
    }
}
