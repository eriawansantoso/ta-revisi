<?php

namespace App\Exports;

use App\Yearbook;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;

class YearbookExport implements  WithEvents, WithHeadings, ShouldAutoSize,  FromQuery
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $id_pesan;

    public function __construct(string $id_pesan)
    {
        $this->id_pesan = $id_pesan;
    }

    public function query()
    {
        // $yearbook = DB::table('yearbook')
        // ->select('kelas_yb','nama_yb','jk_yb',(DB::raw("CONCAT(tlhr_yb,', ',tgllhr_yb) AS TTL")),'alamat_yb','sosmed_yb','bio_yb')
        // ->where('id_pesan',$this->id_pesan)
        // ->get();
        
        return Yearbook::query()
        ->select('kelas_yb','nama_yb','jk_yb',(DB::raw("CONCAT(tlhr_yb,', ',tgllhr_yb) AS TTL")),'alamat_yb','sosmed_yb','bio_yb')
        ->where('id_pesan',$this->id_pesan);
        
    }

    public function headings(): array
    {
        return [
            'KELAS',
            'NAMA SISWA',
            'JENIS KELAMIN',
            'TTL',
            'ALAMAT',
            'SOSMED',
            'BIO',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W100'; // All headers
                $event->sheet->getDelegate()->getStyle('A1:N1')->getAlignment()->setWrapText(true);
                $event->sheet->getDelegate()->getStyle('A1:N1')->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $styleHeader = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];
                $event->sheet->getStyle('A3:N3')->applyFromArray($styleHeader);
                $event->sheet->getStyle('A4:N4')->applyFromArray($styleHeader);
                $event->sheet->getStyle('A5:N3000')->applyFromArray($styleHeader);
            }
        ];
    }
}
