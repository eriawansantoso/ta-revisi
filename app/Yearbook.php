<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yearbook extends Model
{
    protected $table = 'yearbook';
    protected $fillable = [
        'kelas_yb',
        'nama_yb',
        'jk_yb',
        'tlhr_yb',
        'tgllhr_yb',
        'alamat_yb',
        'bio_yb',
        'sosmed_yb',
        'id_pesan'

    ];
}
