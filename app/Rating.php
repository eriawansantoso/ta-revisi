<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'rating';
    protected $guarded = ['id'];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function port_foto(){
        return $this->belongsTo(Port_foto::class);
    }
    public function port_vidio(){
        return $this->belongsTo(Port_vidio::class);
    }
}
