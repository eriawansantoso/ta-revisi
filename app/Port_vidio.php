<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

class Port_vidio extends Model
{
    protected $table = 'port_vidio';
    protected $fillable = [
        'id_vid',
        'nama_vid',
        'vidio',
        'ket_vid',
        'kategori_vid'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
