<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Port_foto extends Model
{
    protected $table = 'port_foto';
    protected $fillable = [
        'id_foto',
        'nama_foto',
        'foto',
        'ket_foto',
        'kategori_foto'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function komentar(){
        return $this->hasMany(Komentar::class);
    }
}
