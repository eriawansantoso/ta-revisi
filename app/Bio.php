<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bio extends Model
{
    protected $table = 'bio';
    protected $guarded = ['id'];
    protected $fillable = [
        'kelas_bio',
        'nama_bio',
        'jk_bio',
        'tlhr_bio',
        'tgllhr_bio',
        'alamat_bio',
        'bio_bio',
        'sosmed_bio',
        'id_pesan'

    ];
}
