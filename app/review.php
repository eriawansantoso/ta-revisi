<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class review extends Model
{
    protected $table = 'review';
    protected $fillable = [
        'isi_rev',
        'id_pesan',
        'id_pel'
    ];
}
