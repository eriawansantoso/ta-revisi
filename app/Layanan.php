<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Layanan extends Model
{
    protected $table = 'layanan';
    protected $fillable = [
        'id_lay',
        'nama_lay',
        'jenis_lay',
        'foto_lay',
        'harga_lay',
        'ket_lay'
    ];
}
