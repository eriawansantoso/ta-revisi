<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log_lihat extends Model
{
    protected $table = 'log_lihat';
    protected $guarded = ['id'];
    protected $fillable = [
        'id_pel',
        'id_foto',
        'id_vidio'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function port_foto(){
        return $this->belongsTo(Port_foto::class);
    }
    public function port_vidio(){
        return $this->belongsTo(Port_vidio::class);
    }
}
