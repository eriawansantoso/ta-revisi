<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Layanan;
use App\Pemesanan;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PemesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pemesanan = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*', 'a.name as nama_pemesan','b.harga_lay','b.nama_lay as nama_layanan')
            ->where('pemesanan.id_pesan', 'not like','YB%')
            ->orderBy('pemesanan.updated_at','desc')
            ->get();
        $data = array(
            'pemesanan' => $pemesanan
        );
        return view('main/pemesanan', $data);
    }

    public function yearbook()
    {
        $pemesanan = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*', 'a.name as nama_pemesan','b.nama_lay as nama_layanan')
            ->where('pemesanan.id_pesan', 'like','YB%')
            ->orderBy('pemesanan.id','desc')
            ->get();
        $data = array(
            'pemesanan' => $pemesanan
        );
        return view('main/pemesananyb', $data);
    }

    public function order()
    {
        $id_user = Auth::user()->id;
        $pemesanan = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*', 'a.name as nama_pemesan','b.nama_lay as nama_layanan', 'b.jenis_lay as jenis')
            ->where('id_pel',$id_user)
            ->orderBy('pemesanan.updated_at','desc')
            ->get();
        $data = array(
            'pemesanan' => $pemesanan
        );
        return view('pelanggan/order', $data);
    }

    public function detilorder($id)
    {
        $id_user = Auth::user()->id;
        $pemesanan = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*','a.*','b.*','a.name as nama_pemesan','pemesanan.created_at as tgl_nota','pemesanan.updated_at as riwayat', 'pemesanan.id as id_pemesanan')
            ->where('pemesanan.id', $id)
            ->where('id_pel',$id_user)
            ->get();
            
        $data = array(
            'pemesanan' => $pemesanan
        );
        return view('pelanggan/detilorder', $data);
    }

    public function detilorderyb($id)
    {
        $id_user = Auth::user()->id;
        $pemesanan = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*','a.*','b.*','a.name as nama_pemesan','pemesanan.created_at as tgl_nota','pemesanan.updated_at as riwayat', 'pemesanan.id as id_pemesanan')
            ->where('pemesanan.id', $id)
            ->where('id_pel',$id_user)
            ->get();
        // dd($pemesanan);
        $data = array(
            'pemesanan' => $pemesanan
        );
        return view('pelanggan/detilorderyb', $data);
    }



     // ngedit tgl pemesanan di pelangganu
    public function editgl(Request $request)
    {
        $data = Pemesanan::findOrFail($request->get('id'));
        echo json_encode($data);
    }

    public function updatgl(Request $request)
    {
            $data = array(
            'tgl_pesan'=>$request->post('tgl_pesan'),
            'status_pesan'=>2
            );
            $simpan = DB::table('pemesanan')
            ->where('id','=',$request->post('id'))
            ->update($data);

            if($simpan){
                return redirect()->back()->with('sukses', 'Status Berhasil Diubah');
            }else{
                return redirect()->back()->with('gagal', 'Status Gagal Diubah');
            } 
    }


    // ngedit bukti pesan di pelanggan

    public function updabukti(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg'
        ];
        $this->validate($request, [
            'bukti_pesan' => 'required | mimes:jpeg,jpg,png,JPEG,JPG | max:2000'
        ], $message);

        $order = pemesanan::findOrFail($request->id);
        if ($request->file('bukti_pesan')) {
            $file = $request->file('bukti_pesan');
            $nama_file = time() . "_" .$file->getClientOriginalName();
            $tujuan_upload = 'nota';
            $file->move($tujuan_upload, $nama_file);

            $order->bukti_pesan = $nama_file;
            $order-> status_pesan= 5;

            $order->save();
        }

        if($order){
            return redirect()->back()->with('sukses', 'Status Berhasil Diubah');
        }else{
            return redirect()->back()->with('gagalbukti', 'Status Gagal Diubah');
        }
            
    }
                                                                

    public function konfirmasi($id)
    {
        //function ini untuk mengkonfirmasi bahwa pelanngan sudah melakukan pembayaran
        Pemesanan::findOrFail($id)->update([
            'ket_pesan' => 1,
        ]);

        return redirect('/pemesanan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pemesanan = pemesanan::find($id);
        $pemesanan->status_pesan = $request->input('status_pesan'); 
        // dd($pemesanan);
        $pemesanan->save();
        return redirect('/pemesanan')->with('sukses', 'Status Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
