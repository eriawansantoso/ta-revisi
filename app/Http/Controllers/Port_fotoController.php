<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Port_foto;
use App\Rating;
use App\Komentar;
use Illuminate\Support\Facades\Auth;
use App\kategori_port;
use App\Log_lihat;

class Port_fotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori_port = kategori_port::all();
        $port_foto = DB::table('port_foto')
        ->join('kategori_port', 'kategori_port.id','=','port_foto.kategori_foto')
        ->select('port_foto.*','kategori_port.nama')
        ->orderBy('port_foto.id','desc')
        ->get();
        return view('main/port_foto',compact('kategori_port'))->with('port_foto',$port_foto);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama_foto' => 'required',
            'foto' => 'required',
            'ket_foto'=> 'required',
            'kategori_foto'=> 'required'
        ], $message);

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');
        $extension = $file->getClientOriginalExtension();
        $nama_file = time() . "_" .$file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'portofolio_file';
        $file->move($tujuan_upload, $nama_file);


        $port_foto = new Port_foto;
        $port_foto->nama_foto = $request->input('nama_foto'); 
        $port_foto->foto = $nama_file; 
        $port_foto->ket_foto = $request->input('ket_foto'); 
        $port_foto->kategori_foto = $request->input('kategori_foto'); 
        $port_foto->save();

        return redirect('/port_foto')->with('sukses', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function detail($id, request $request)
    {
        $kategori_port = kategori_port::all();
        $komentar = Komentar::all();
        $jmlkomen = DB::table('komentar')->where('id_foto', $request ->id)->where('status_kom', '1')->count();
        $jmlrating = DB::table('rating')->where('id_foto', $request ->id)->avg('rating');
        $jmllihat = DB::table('log_lihat')->where('id_foto', $request ->id)->count('id_foto');
        $port_foto = DB::table('port_foto')
        ->join('kategori_port', 'kategori_port.id','=','port_foto.kategori_foto')
        ->select('port_foto.*','kategori_port.nama')
        ->where('port_foto.id',$id)
        ->get();
        $komentar_foto = DB::table('komentar')
            ->Join('users as a', 'a.id', '=', 'komentar.id_pel')
            ->Join('port_foto as b', 'b.id', '=', 'komentar.id_foto')
            ->select('komentar.*', 'a.*','b.*')
            ->where('b.id',$id)
            ->get();

        
        return view('pengunjung/detilfoto',compact('jmllihat','jmlkomen','jmlrating','kategori_port','komentar','port_foto','komentar_foto','id'));
    }

    public function detailpel($id, request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id,'id_foto'=> $request ->id ]);
        $log_lihat = Log_lihat::create($request->all());
        // dd($log_lihat);

        $jmlkomen = DB::table('komentar')->where('id_foto', $request ->id)->where('status_kom', '1')->count();
        // dd($jmlkomen);
        $kategori_port = kategori_port::all();
        $komentar = Komentar::all();
        $port_foto = DB::table('port_foto')
        ->join('kategori_port', 'kategori_port.id','=','port_foto.kategori_foto')
        ->select('port_foto.*','kategori_port.nama')
        ->where('port_foto.id',$id)
        ->get();

        $jmlrating = DB::table('rating')->where('id_foto', $request ->id)->avg('rating');
        $jmllihat = DB::table('log_lihat')->where('id_foto', $request ->id)->count('id_foto');
    // dd($jmlrating);
        $komentar_foto = DB::table('komentar')
            ->Join('users as a', 'a.id', '=', 'komentar.id_pel')
            ->Join('port_foto as b', 'b.id', '=', 'komentar.id_foto')
            ->select('komentar.*', 'a.*','b.*')
            ->where('b.id',$id)
            ->get();

            $rating = DB::table('rating')
            ->select('*')
            ->get();

            $rat = DB::table('rating')
            ->select('*')
            // ->whereNotNull('id_foto')
            ->where('id_pel',Auth::user()->id)
            ->where('id_foto',$request->id)
            ->first();

        return view('pelanggan/detilfotpel',compact('jmllihat','jmlrating','rat','rating','jmlkomen','kategori_port','komentar','port_foto','komentar_foto','id'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function postkomentar(Request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id]);
        $komentar = Komentar::create($request->all());
        return redirect()->back()->with('sukses','Komentar berhasil ditambahkan, tunggu validasi dari admin untuk melihat komentar anda tampil');
    }
    public function addratefoto(Request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id]);
        $rating = Rating::create($request->all());
        return redirect()->back()->with('berhasil','Terima Kasih Telah Memberi Nilai');
    }


    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama_foto' => 'required',
            'ket_foto'=> 'required',
            'kategori_foto'=> 'required'
        ], $message);

        // menyimpan data file yang diupload ke variabel $file
        
        
        
                $port_foto = Port_foto::find($id);
                $port_foto->nama_foto = $request->input('nama_foto'); 
                if($request->hasfile('foto')){
                    $file = $request->file('foto');
                    $extension = $file->getClientOriginalExtension();
                    $nama_file = time() . "_" .$file->getClientOriginalName();
            
                    // isi dengan nama folder tempat kemana file diupload
                    $tujuan_upload = 'portofolio_file';
                    $file->move($tujuan_upload, $nama_file);
                    $port_foto->foto = $nama_file;
                    }
                $port_foto->ket_foto = $request->input('ket_foto'); 
                $port_foto->kategori_foto = $request->input('kategori_foto');
                $port_foto->save();
        
                return redirect('/port_foto')->with('sukses', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $port_foto = Port_foto::find($id);
        DB::table('komentar')->where('id_foto', $id)->delete();
        DB::table('rating')->where('id_foto', $id)->delete();
        DB::table('log_lihat')->where('id_foto', $id)->delete();
        $port_foto->delete();

        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
    }
}
