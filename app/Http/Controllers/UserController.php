<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $user = $users->where('role', '!=','pengunjung');
        return view('main/user')->with('user',$user);
    }

    public function profil()
    {
        
    }

    public function editusr()
    {
        $id_user = Auth::user()->id;
        $user = User::all()->where('id',$id_user);
        // $user = $users->where('role', '!=','pengunjung');
        return view('main/profiladm')->with('user',$user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'name' => 'required',
            'hp' => 'required','numeric','min:11',
            'email'=> 'required','email','unique:users',
            'password'=> 'required',
            'alamat'=> 'required',
            'role' => 'required'
        ], $message);

        $user = new User;
        $user->name = $request->input('name'); 
        $user->hp = $request->input('hp'); 
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password')); 
        $user->alamat = $request->input('alamat'); 
        $user->role = $request->input('role');
        $user->save();

        return redirect('/user')->with('sukses', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'name' => 'required',
            'hp' => 'required','numeric','min:11',
            'email'=> 'required','email','unique:users',
            // 'password'=> 'min:6',
            'alamat'=> 'required',
            'role' => 'required'
        ], $message);
        
                $user = User::find($id);
                $user->name = $request->input('name'); 
                $user->hp = $request->input('hp'); 
                $user->email = $request->input('email'); 
                // Hash::make($user['password']);
                $user->password = Hash::make('password'); 
                $user->alamat = $request->input('alamat'); 
                $user->role = $request->input('role'); 
                $user->save();
        
                return redirect('/user')->with('sukses', 'Data Berhasil Diubah');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
