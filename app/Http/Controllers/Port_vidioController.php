<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Port_vidio;
use App\Rating;
use App\Komentar;
use App\Log_lihat;
use App\kategori_port;
use App\Http\Controllers\Model;
use App\User;
use Cohensive\Embed\Facades\Embed;
use Illuminate\Support\Facades\Auth;

class Port_vidioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori_port = kategori_port::all();
        $port_vidio = DB::table('port_vidio')->join('kategori_port', 'kategori_port.id','=','port_vidio.kategori_vid')
        ->select('port_vidio.*','kategori_port.nama')
        ->orderBy('port_vidio.id','desc')
        ->get();
        return view('main/port_vidio',compact('kategori_port'))->with('port_vidio',$port_vidio);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama_vid' => 'required',
            'vidio' => 'required',
            'ket_vid'=> 'required',
            'kategori_vid'=> 'required'
        ], $message);

        $port_vidio = new Port_vidio;
        $port_vidio->nama_vid = $request->input('nama_vid'); 
        $port_vidio->vidio = $request->input('vidio');  
        $port_vidio->ket_vid = $request->input('ket_vid'); 
        $port_vidio->kategori_vid = $request->input('kategori_vid'); 
        $port_vidio->save();

        return redirect('/port_vidio')->with('sukses', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id, request $request)
    {
        $kategori_port = kategori_port::all();
        $komentar = Komentar::all();
        $jmlkomen = DB::table('komentar')->where('id_vidio', $request ->id)->where('status_kom', '1')->count();
        $jmlrating = DB::table('rating')->where('id_vidio', $request ->id)->avg('rating');
        $jmllihat = DB::table('log_lihat')->where('id_vidio', $request ->id)->count('id_vidio');
        $port_vidio = DB::table('port_vidio')
        ->join('kategori_port', 'kategori_port.id','=','port_vidio.kategori_vid')
        ->select('port_vidio.*','kategori_port.nama')
        ->where('port_vidio.id',$id)
        ->get();
        $komentar_vidio = DB::table('komentar')
            ->Join('users as a', 'a.id', '=', 'komentar.id_pel')
            ->Join('port_vidio as b', 'b.id', '=', 'komentar.id_vidio')
            ->select('komentar.*', 'a.*','b.*')
            ->where('b.id',$id)
            ->get();
        return view('pengunjung/detilvidio',compact('jmllihat','jmlkomen','jmlrating','kategori_port','komentar','port_vidio','komentar_vidio','id'));
    }


    public function detailpel($id, request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id,'id_vidio'=> $request ->id ]);
        $log_lihat = Log_lihat::create($request->all());

        $kategori_port = kategori_port::all();
        $komentar = Komentar::all();
        $port_vidio = DB::table('port_vidio')
        ->join('kategori_port', 'kategori_port.id','=','port_vidio.kategori_vid')
        ->select('port_vidio.*','kategori_port.nama')
        ->where('port_vidio.id',$id)
        ->get();
        $jmlkomen = DB::table('komentar')->where('id_vidio', $request ->id)->where('status_kom', '1')->count();
        $jmlrating = DB::table('rating')->where('id_vidio', $request ->id)->avg('rating');
        $jmllihat = DB::table('log_lihat')->where('id_vidio', $request ->id)->count('id_vidio');

        $komentar_vidio = DB::table('komentar')
            ->Join('users as a', 'a.id', '=', 'komentar.id_pel')
            ->Join('port_vidio as b', 'b.id', '=', 'komentar.id_vidio')
            ->select('komentar.*', 'a.*','b.*')
            ->where('b.id',$id)
            ->get();
            $rat = DB::table('rating')
            ->select('*') 
            // ->whereNotNull('id_vidio')
            ->where('id_pel',Auth::user()->id)
            ->where('id_vidio',$request->id)
            ->first();

        $rating = DB::table('rating')
            ->select('rating.*')
            ->get();
        return view('pelanggan/detilvidpel',compact('jmllihat','jmlrating','rat','rating','jmlkomen','kategori_port','komentar','port_vidio','komentar_vidio','id'));
    }

    
    public function postkomentar(Request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id]);
        $komentar = Komentar::create($request->all());
        return redirect()->back()->with('sukses','Komentar berhasil ditambahkan, tunggu validasi dari admin untuk melihat komentar anda tampil');
    }

    public function addratevidio(Request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id]);
        $rating = Rating::create($request->all());
        return redirect()->back()->with('berhasil','Terima Kasih Telah Memberi Nilai');
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama_vid' => 'required',
            'vidio' => 'required',
            'ket_vid'=> 'required',
            'kategori_vid'=> 'required'
        ], $message);

                $port_vidio = Port_vidio::find($id);
                $port_vidio->nama_vid = $request->input('nama_vid');
                $port_vidio->vidio = $request->input('vidio'); 
                $port_vidio->ket_vid = $request->input('ket_vid'); 
                $port_vidio->kategori_vid = $request->input('kategori_vid');
                $port_vidio->save();
        
                return redirect('/port_vidio')->with('sukses', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $port_vidio = Port_vidio::find($id);
        DB::table('komentar')->where('id_vidio', $id)->delete();
        DB::table('rating')->where('id_vidio', $id)->delete();
        DB::table('log_lihat')->where('id_vidio', $id)->delete();
        $port_vidio->delete();

        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
    }
}
