<?php

namespace App\Http\Controllers;

use App\Kategori_port;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Kategori_portController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori_port = Kategori_port::all();
        return view('main/kategori')->with('kategori_port',$kategori_port);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama' => 'required'
        ], $message);

        $kategori_port = new Kategori_port;
        $kategori_port->nama = $request->input('nama'); 
        $kategori_port->save();

        return redirect('/kategori')->with('sukses', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama' => 'required'
        ], $message);
        
                $kategori_port = Kategori_port::find($id);
                $kategori_port->nama = $request->input('nama'); 
                $kategori_port->save();
        
                return redirect('/kategori')->with('sukses', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
