<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Layanan;
use App\Pemesanan;
use App\User;

class PemesananybController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pemesanan = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*', 'a.name as nama_pemesan','b.harga_lay','b.nama_lay as nama_layanan')
            ->where('pemesanan.id_pesan', 'like','YB%')
            ->orderBy('pemesanan.updated_at','desc')
            ->get();
        $data = array(
            'pemesanan' => $pemesanan
        );
        return view('main/pemesananyb', $data);
    }

    // ngedit tipe bayar pemesanan di pelangganu
    public function editipe(Request $request)
    {
        $data = Pemesanan::findOrFail($request->get('id'));
        echo json_encode($data);
    }

    public function updatipe(Request $request)
    {
            $data = array(
            'tipe_bayar'=>$request->post('tipe_bayar'),
            'status_pesan'=>2
            );
            $simpan = DB::table('pemesanan')
            ->where('id','=',$request->post('id'))
            ->update($data);

            if($simpan){
                return redirect()->back()->with('sukses', 'Status Berhasil Diubah');
            }else{
                return redirect()->back()->with('gagal', 'Status Gagal Diubah');
            }
            
    }


    public function konfirmasi($id)
    {
        //function ini untuk mengkonfirmasi bahwa pelanngan sudah melakukan pembayaran
        Pemesanan::findOrFail($id)->update([
            'ket_pesan' => 1,
        ]);

        return redirect('/pemesanan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pemesanan = pemesanan::find($id);
        $pemesanan->status_pesan = $request->input('status_pesan'); 
        // dd($pemesanan);
        $pemesanan->save();
        
        if($pemesanan->status_pesan == $request->get('status_pesan')){
            return redirect('/pemesananyb')->with('sukses', 'Status Berhasil Diubah');
        }
        else{
            return redirect('/pemesananyb')->with('gagal', 'Status Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
