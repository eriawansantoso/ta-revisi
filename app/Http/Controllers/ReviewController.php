<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\review;
use App\Pemesanan;
use Illuminate\Support\Facades\DB;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $review = review::all();
        $review = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan')
        ->orderBy('review.id','desc')
        ->get();

        

        return view('main/review')->with('review',$review);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg'
        ];
        $this->validate($request, [
            'foto_rev' => 'required | mimes:jpeg,jpg,png,JPEG,JPG | max:2000'
        ], $message);

        if ($request->file('foto_rev')) {
            $file = $request->file('foto_rev');
            $nama_file = time() . "_" .$file->getClientOriginalName();
            $tujuan_upload = 'review';
            $file->move($tujuan_upload, $nama_file);
            
            $review = new review;
            $review->foto_rev = $nama_file;
            $review->id_pesan = $request->input('id_pesan'); 
            $review->id_pel = $request->input('id_pel'); 
            $review->isi_rev = $request->input('isi_rev'); 
            $review->save();
            Pemesanan::findOrFail($review->id_pesan)->update([
                'status_pesan' => 10,
            ]);
            return redirect()->back()->with('suksesrev', 'Berhasil mereview pesanan');
        }
        else{
            return redirect()->back()->with('gagalrev', 'Gagal mereview pesanan');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
