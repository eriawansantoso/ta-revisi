<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Log_lihat;
use App\Port_foto;
use App\Port_vidio;
use App\Komentar;
use App\Kategori_port;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function laporan(Request $request)
    {

        // $data = DB::table('pemesanan')
        //     ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
        //     ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
        //     ->select('pemesanan.*','pemesanan.id', 'a.name as nama_pemesan','b.jenis_lay','b.nama_lay as nama_layanan',db::raw('substr(pemesanan.created_at, 1, 10)  as tanggal'),db::raw('substr(pemesanan.updated_at, 1, 10)  as tgl'))
        //     ->where('status_pesan',"=",4)
        //     ->orwhere('status_pesan',"=",10)
        //     ->get();
        //     dd($data);

        if(request()->ajax())
        {
         if(!empty($request->from_date))
         {
            $data = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*','pemesanan.id', 'a.name as nama_pemesan','b.jenis_lay','b.nama_lay as nama_layanan',db::raw('substr(pemesanan.created_at, 1, 10)  as tanggal'),db::raw('substr(pemesanan.updated_at, 1, 10)  as tgl'))
            ->where('status_pesan',"=",10)
            ->whereBetween("pemesanan.created_at",[$request->from_date,$request->to_date])
            ->orwhere('status_pesan',"=",4)
            ->whereBetween("pemesanan.created_at",[$request->from_date,$request->to_date])
            ->get();
        }
        else
        {
            $data = DB::table('pemesanan')
            ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
            ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
            ->select('pemesanan.*','pemesanan.id', 'a.name as nama_pemesan','b.jenis_lay','b.nama_lay as nama_layanan',db::raw('substr(pemesanan.created_at, 1, 10)  as tanggal'),db::raw('substr(pemesanan.updated_at, 1, 10)  as tgl'))
            ->where('status_pesan',"=",4)
            ->orwhere('status_pesan',"=",10)
            ->get();
        }
        return datatables()->of($data)->make(true);
       }

       $laporanbelum = DB::table('pemesanan')
       ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
       ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
       ->select('pemesanan.*', 'a.name as nama_pemesan','b.jenis_lay','b.nama_lay as nama_layanan')
       ->where('pemesanan.status_pesan',"!=",10)
       ->where('pemesanan.status_pesan',"!=",4)
       ->where('pemesanan.status_pesan',"!=",7)
       ->orderBy('pemesanan.created_at')
       ->get();

       $laporanselesai = DB::table('pemesanan')
       ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
       ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
       ->select('pemesanan.*', 'a.name as nama_pemesan','b.jenis_lay','b.nama_lay as nama_layanan')
       ->where('pemesanan.status_pesan',"=",10)
       ->orwhere('pemesanan.status_pesan',"=",4)
       ->orderBy('pemesanan.created_at','desc')   
       ->get();

       $laporangagal = DB::table('pemesanan')
       ->Join('users as a', 'a.id', '=', 'pemesanan.id_pel')
       ->Join('layanan as b', 'b.id', '=', 'pemesanan.id_lay')
       ->select('pemesanan.*', 'a.name as nama_pemesan','b.jenis_lay','b.nama_lay as nama_layanan')
       ->where('pemesanan.status_pesan',"=",7)
       ->orderBy('pemesanan.created_at')
       ->get();
       // dd($laporanselesai);

       
       $data = array(
        // 'perbulan'=>$perbulan,
        'laporanbelum'=>$laporanbelum,
        'laporanselesai'=>$laporanselesai,
        'laporangagal'=>$laporangagal
       );
       return view('/main/laporan',$data);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_kom = DB::table('komentar')
        ->select(db::raw('count(*) as jml'))
        ->get();

        $total_kom_val = DB::table('komentar')
        ->select(db::raw('count(*) as jml'))
        ->where('status_kom','=','1')
        ->get();

        $total_kom_unval = DB::table('komentar')
        ->select(db::raw('count(*) as jml'))
        ->where('status_kom','=','0')
        ->get();

        $kom_fot = DB::table('komentar')
        ->Join('port_foto as b', 'b.id', '=', 'komentar.id_foto')
        ->select("komentar.*",
        "b.nama_foto"
        ,db::raw('count(komentar.id_foto) as jumlah'))
        ->groupby('komentar.id_foto')
        ->orderby('jumlah','desc')
        ->limit(3)
        ->get();

        $kom_vid = DB::table('komentar')
        ->Join('port_vidio as b', 'b.id', '=', 'komentar.id_vidio')
        ->select("komentar.*",
        "b.nama_vid"
        ,db::raw('count(komentar.id_vidio) as jumlah'))
        ->groupby('komentar.id_vidio')
        ->orderby('jumlah','desc')
        ->limit(3)
        ->get();

        $rat_fot = DB::table('rating')
        ->Join('port_foto as b', 'b.id', '=', 'rating.id_foto')
        ->select("rating.*",
        "b.nama_foto"
        ,db::raw('avg(rating.rating) as jumlah'))
        ->groupby('rating.rating')
        ->orderby('jumlah','desc')
        ->limit(3)
        ->get();

        $rat_vid = DB::table('rating')
        ->Join('port_vidio as b', 'b.id', '=', 'rating.id_vidio')
        ->select("rating.*",
        "b.nama_vid"
        ,db::raw('avg(rating.rating) as jumlah'))
        ->groupby('rating.rating')
        ->orderby('jumlah','desc')
        ->limit(3)
        ->get();

        

// dd($perbulan);
        $bl13vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 14 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 14 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 14 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 13 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 14 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 13 month)")
        ->first();

        $bl12vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 12 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 12 month)")
        ->first();

        $bl11vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 11 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 11 month)")
        ->first();

        $bl10vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 10 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 10 month)")
        ->first();

        $bl9vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 9 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 9 month)")
        ->first();

        $bl8vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 8 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 8 month)")
        ->first();

        $bl7vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 7 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 7 month)")
        ->first();

        $bl6vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 6 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 6 month)")
        ->first();

        $bl5vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 5 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 5 month)")
        ->first();

        $bl4vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 4 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 4 month)")
        ->first();

        $bl3vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 3 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 3 month)")
        ->first();

        $bl2vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 2 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 2 month)")
        ->first();

        $bl1vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 1 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 1 month)")
        ->first();

        $bl0vd = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate())")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate())")
        ->first();

        $bl12FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 12 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 12 month)")
        ->first();

        $bl11FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 11 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 11 month)")
        ->first();

        $bl10FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 10 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 10 month)")
        ->first();

        $bl9FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 9 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 9 month)")
        ->first();

        $bl8FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 8 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 8 month)")
        ->first();

        $bl7FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 7 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 7 month)")
        ->first();

        $bl6FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 6 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 6 month)")
        ->first();

        $bl5FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 5 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 5 month)")
        ->first();

        $bl4FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 4 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 4 month)")
        ->first();

        $bl3FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 3 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 3 month)")
        ->first();

        $bl2FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 2 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 2 month)")
        ->first();

        $bl1FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 1 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 1 month)")
        ->first();

        $bl0FT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate())")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate())")
        ->first();

        $bl12YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 12 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 13 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 12 month)")
        ->first();

        $bl11YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 11 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 12 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 11 month)")
        ->first();

        $bl10YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 10 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 11 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 10 month)")
        ->first();

        $bl9YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 9 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 10 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 9 month)")
        ->first();

        $bl8YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 8 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 9 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 8 month)")
        ->first();

        $bl7YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 7 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 8 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 7 month)")
        ->first();

        $bl6YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 6 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 7 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 6 month)")
        ->first();

        $bl5YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 5 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 6 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 5 month)")
        ->first();

        $bl4YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 4 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 5 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 4 month)")
        ->first();

        $bl3YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 3 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 3 month)")
        ->first();

        $bl2YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 2 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 2 month)")
        ->first();

        $bl1YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 1 month)")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate() - interval 1 month)")
        ->first();

        $bl0YB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"),
        db::raw("MONTH(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY)) as bln"),
        db::raw("Year(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY)) as thn "))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate())")
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->whereRaw("created_at BETWEEN DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY) and
        LAST_DAY(curdate())")
        ->first();

        $selesaiFT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','FT%')
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','FT%')
        ->first();

        $belumFT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"!=",10)
        ->where('status_pesan',"!=",4)
        ->where('status_pesan',"!=",7)
        ->where('id_pesan','like','FT%')
        ->first();

        $gagalFT = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",7)
        ->where('id_pesan','like','FT%')
        ->first();

        $selesaiVD = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','VD%')
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','VD%')
        ->first();

        $belumVD = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"!=",10)
        ->where('status_pesan',"!=",4)
        ->where('status_pesan',"!=",7)
        ->where('id_pesan','like','VD%')
        ->first();

        $gagalVD = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",7)
        ->where('id_pesan','like','VD%')
        ->first();

        $selesaiYB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",10)
        ->where('id_pesan','like','YB%')
        ->orwhere('status_pesan',"=",4)
        ->where('id_pesan','like','YB%')
        ->first();

        $belumYB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"!=",10)
        ->where('status_pesan',"!=",4)
        ->where('status_pesan',"!=",7)
        ->where('id_pesan','like','YB%')
        ->first();

        $gagalYB = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",7)
        ->where('id_pesan','like','YB%')
        ->first();

        $jmlvid= DB::table('port_vidio')
        ->select( db::raw("COUNT(id) as tot"))
        ->first();
        // dd($jmlvid);
        $jmlfot= DB::table('port_foto')
        ->select( db::raw("COUNT(id) as tot"))
        ->first();


        $data = array(
            'jmlvid'=>$jmlvid,
            'jmlfot'=>$jmlfot,
            'total_kom' => $total_kom,
            'total_kom_val' => $total_kom_val,
            'total_kom_unval' => $total_kom_unval,
            'kom_fot' => $kom_fot,
            'selesaiVD' => $selesaiVD,
            'belumVD' => $belumVD,
            'gagalVD' => $gagalVD,
            'selesaiFT' => $selesaiFT,
            'belumFT' => $belumFT,
            'gagalFT' => $gagalFT,
            'selesaiYB' => $selesaiYB,
            'belumYB' => $belumYB,
            'gagalYB' => $gagalYB,
            'kom_vid' => $kom_vid,
            'rat_vid' => $rat_vid,
            'rat_fot' => $rat_fot,
            'bl12vd' => $bl12vd,
            'bl11vd' => $bl11vd,
            'bl10vd' => $bl10vd,
            'bl9vd' => $bl9vd,
            'bl8vd' => $bl8vd,
            'bl7vd' => $bl7vd,
            'bl6vd' => $bl6vd,
            'bl5vd' => $bl5vd,
            'bl4vd' => $bl4vd,
            'bl3vd' => $bl3vd,
            'bl2vd' => $bl2vd,
            'bl1vd' => $bl1vd,
            'bl0vd' => $bl0vd,
            'bl12FT' => $bl12FT,
            'bl11FT' => $bl11FT,
            'bl10FT' => $bl10FT,
            'bl9FT' => $bl9FT,
            'bl8FT' => $bl8FT,
            'bl7FT' => $bl7FT,
            'bl6FT' => $bl6FT,
            'bl5FT' => $bl5FT,
            'bl4FT' => $bl4FT,
            'bl3FT' => $bl3FT,
            'bl2FT' => $bl2FT,
            'bl1FT' => $bl1FT,
            'bl0FT' => $bl0FT,
            'bl12YB' => $bl12YB,
            'bl11YB' => $bl11YB,
            'bl10YB' => $bl10YB,
            'bl9YB' => $bl9YB,
            'bl8YB' => $bl8YB,
            'bl7YB' => $bl7YB,
            'bl6YB' => $bl6YB,
            'bl5YB' => $bl5YB,
            'bl4YB' => $bl4YB,
            'bl3YB' => $bl3YB,
            'bl2YB' => $bl2YB,
            'bl1YB' => $bl1YB,
            'bl0YB' => $bl0YB,
        );
        return view('/main/dashboardowner',$data);
    }



    public function indexport()
    {
        $total_kom = DB::table('komentar')
        ->select(db::raw('count(*) as jml'))
        ->get();

        $total_kom_val = DB::table('komentar')
        ->select(db::raw('count(*) as jml'))
        ->where('status_kom','=','1')
        ->get();

        $total_kom_unval = DB::table('komentar')
        ->select(db::raw('count(*) as jml'))
        ->where('status_kom','=','0')
        ->get();

        $rekom_foto = DB::table('log_lihat')
        ->Join('users as a', 'a.id', '=', 'log_lihat.id_pel')
        ->Join('port_foto as b', 'b.id', '=', 'log_lihat.id_foto')
        ->Join('kategori_port as k', 'k.id', '=', 'b.kategori_foto')
        ->select("log_lihat.*",
        "b.foto", "b.nama_foto", "k.nama"
        ,db::raw('count(log_lihat.id_foto) as jml'))
        ->groupby('log_lihat.id_foto')
        ->orderby('jml','desc')
        ->limit(5)
        ->get();

        $rekom_vid = DB::table('log_lihat')
        ->Join('users as a', 'a.id', '=', 'log_lihat.id_pel')
        ->Join('port_vidio as b', 'b.id', '=', 'log_lihat.id_vidio')
        ->Join('kategori_port as k', 'k.id', '=', 'b.kategori_vid')
        ->select("log_lihat.*",
        "b.vidio", "b.nama_vid", "k.nama"
        ,db::raw('count(log_lihat.id_vidio) as jml'))
        ->groupby('log_lihat.id_vidio')
        ->orderby('jml','desc')
        ->limit(5)
        ->get();

        $user = User::all();
        $kategori_port = kategori_port::all();
        $port_foto = Port_foto::all();

        $jmlvid= DB::table('port_vidio')
        ->select( db::raw("COUNT(id) as tot"))
        ->first();
        // dd($jmlvid);
        $jmlfot= DB::table('port_foto')
        ->select( db::raw("COUNT(id) as tot"))
        ->first();

        $data = array(
            'total_kom' => $total_kom,
            'total_kom_val' => $total_kom_val,
            'total_kom_unval' => $total_kom_unval,
            'jmlvid'=>$jmlvid,
            'jmlfot'=>$jmlfot,
            'rekom_foto' => $rekom_foto,
            'rekom_vid' => $rekom_vid,
            'port_foto' => $port_foto,
            'kategori_port' => $kategori_port
            // 'age' => $age
        );
        return view('/main/dashboardport',$data);
    }

    public function indexlay()
    {
        $user = User::all();
        $kategori_port = kategori_port::all();
        $layanan = Port_foto::all()->count();

        $selesai = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",10)
        ->orwhere('status_pesan',"=",4)
        ->first();

        $belum = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"!=",10)
        ->where('status_pesan',"!=",4)
        ->where('status_pesan',"!=",7)
        ->first();

        $gagal = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->where('status_pesan',"=",7)
        ->first();

        $pesanan = DB::table('pemesanan')
        ->select( db::raw("COUNT(id) as tot"))
        ->first();

        $mostlay=DB::select(
           " SELECT a.nama_lay as layanan, 
            round(year(CURRENT_DATE) - avg(year(d.tglhr)),0) as usia,
            if(avg(d.jk)>1.5,'Laki - Laki', 'Perempuan') as gender
                FROM layanan a, pemesanan b, users d
                WHERE d.id = b.id_pel
                and b.id_lay = a.id
                and d.role = 'pengunjung'
                GROUP BY a.nama_lay
                ORDER BY count(a.nama_lay) desc"
        );
        
        // dd($layanan);

        $data = array(
            'mostlay' => $mostlay,
            'layanan' => $layanan,
            'gagal' => $gagal,
            'selesai' =>$selesai,
            'pesanan' =>$pesanan,
            'belum' => $belum
        );
        return view('/main/dashboardlay',$data);
    }
}
