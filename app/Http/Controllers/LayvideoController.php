<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Layanan;
use App\Pemesanan;
use Illuminate\Support\Facades\DB;



class LayvideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $layanans = Layanan::all();
        // $layanans->ket_lay = Str::limit($layanans->ket_lay, 20);
        $layanan = $layanans->where('jenis_lay','Videography');

        $reviewvid = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan, c.jenis_lay as jenis')
        ->where('c.jenis_lay','Videography')
        ->get();

        $data = array(
            'layanan' => $layanan,
            'reviewvid' => $reviewvid
        );

        return view('pengunjung/layvideo',$data);
    }
    
    
    public function indexpel()
    {
        $layanans = Layanan::all();
        // $layanans->ket_lay = Str::limit($layanans->ket_lay, 20);
        $layanan = $layanans->where('jenis_lay','Videography');

        $reviewvid = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan, c.jenis_lay as jenis')
        ->where('c.jenis_lay','Videography')
        ->get();

        $data = array(
            'layanan' => $layanan,
            'reviewvid' => $reviewvid
        );

        return view('pelanggan/layvideopel',$data);
    }


    public function indexpesan($id){

        $layanans = db::table('layanan')
        ->select('*')
        ->where('id',$id)
        ->get();
        $inv = db::table('pemesanan')
        ->select(db::raw('max(id) as id'))
        ->first();

        $nota = $inv->id+1;
        $psn = 'VD00'.$nota;
        $data = [
            'psn' => $psn,
            'layanans' => $layanans
        ];

        return view('pelanggan/pemesanan',$data);
    }

    public function postpesan(Request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id]);
        $pemesanan = Pemesanan::create($request->all());
        
        return  view('pelanggan/pesanberhasil');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
