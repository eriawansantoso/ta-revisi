<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Port_vidio;
use App\Port_foto;
use App\Log_lihat;
use App\kategori_port;
use App\Http\Controllers\Model;
use App\review;
use App\User;
use Illuminate\Support\Carbon;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $review = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan, c.jenis_lay as jenis')
        ->orderBy('review.id','desc')
        ->get();

        $kategori_port = kategori_port::all();

        $port_foto = DB::table('port_foto')
        ->join('kategori_port', 'kategori_port.id','=','port_foto.kategori_foto')
        ->select('port_foto.*','kategori_port.nama')
        // ->orderBy('port_foto.id','DESC')
        ->inRandomOrder()
        ->limit(15)
        ->get();

        // $kategori_port = kategori_port::all();
        $port_vidio = DB::table('port_vidio')
        ->join('kategori_port', 'kategori_port.id','=','port_vidio.kategori_vid')
        ->select('port_vidio.*','kategori_port.nama')
        // ->orderBy('port_vidio.id','DESC')
        ->inRandomOrder()
        ->limit(15)
        ->get();
        $rekom_port = DB::table('log_lihat')
        ->Join('users as a', 'a.id', '=', 'log_lihat.id_pel')
        ->Join('port_foto as b', 'b.id', '=', 'log_lihat.id_foto')
        ->select("log_lihat.*",
        "b.foto"
        ,db::raw('count(log_lihat.id_foto) as jml'))
        ->groupby('log_lihat.id_foto')
        ->orderby('jml','desc')
        ->limit(3)
        ->get();

        $rekom_vid = DB::table('log_lihat')
        ->Join('users as a', 'a.id', '=', 'log_lihat.id_pel')
        ->Join('port_vidio as b', 'b.id', '=', 'log_lihat.id_vidio')
        ->Join('kategori_port as k', 'k.id', '=', 'b.kategori_vid')
        ->select("log_lihat.*",
        "b.vidio", "b.nama_vid", "k.nama"
        ,db::raw('count(log_lihat.id_vidio) as jml'))
        ->groupby('log_lihat.id_vidio')
        ->orderby('jml','desc')
        ->limit(3)
        ->get();

        $rekom_ratfot = DB::table('rating')
        ->Join('users as a', 'a.id', '=', 'rating.id_pel')
        ->Join('port_foto as b', 'b.id', '=', 'rating.id_foto')
        ->select("rating.*",
        "b.foto"
        ,db::raw('avg(rating.rating) as average'))
        ->groupby('rating.id_foto')
        ->orderby('average','desc')
        ->limit(3)
        ->get();

        $rekom_ratvid = DB::table('rating')
        ->Join('users as a', 'a.id', '=', 'rating.id_pel')
        ->Join('port_vidio as b', 'b.id', '=', 'rating.id_vidio')
        ->select("rating.*",
        "b.vidio"
        ,db::raw('avg(rating.rating) as average'))
        ->groupby('rating.id_vidio')
        ->orderby('average','desc')
        ->limit(3)
        ->get();

        $jmlreview = DB::table('review')->count();
        $jmlmember = DB::table('users')->where('role','pengunjung')->count();
        $jmlstaff = DB::table('users')->where('role','admin layanan')->orwhere('role','admin portofolio')->count();        

        $review = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan')
        ->orderBy('review.id','desc')
        ->get();

        $foto_baru = DB::table('port_foto')
        ->join('kategori_port', 'kategori_port.id','=','port_foto.kategori_foto')
        ->select('port_foto.*','kategori_port.nama')
        ->orderBy('port_foto.created_at','DESC')
        ->limit(3)
        ->get();

        $vidio_baru = DB::table('port_vidio')
        ->join('kategori_port', 'kategori_port.id','=','port_vidio.kategori_vid')
        ->select('port_vidio.*','kategori_port.nama')
        ->orderBy('port_vidio.created_at','DESC')
        ->limit(3)
        ->get();

        $data = array(
            'kategori_port' => $kategori_port,
            'review' => $review,
            'rekom_port' => $rekom_port,
            'rekom_vid' => $rekom_vid,
            'rekom_ratfot' => $rekom_ratfot,
            'rekom_ratvid' => $rekom_ratvid,
            'port_foto' => $port_foto,
            'port_vidio' => $port_vidio,
            'foto_baru' => $foto_baru,
            'vidio_baru' => $vidio_baru,
            'jmlreview' => $jmlreview,
            'jmlmember' => $jmlmember,
            'jmlstaff' => $jmlstaff
        );

        return view('welcome',$data);
    }


    public function indexpel()
    {
        
        $review = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan')
        ->orderBy('review.id','desc')
        ->get();
        
        $rekom_port = DB::table('log_lihat')
        ->Join('users as a', 'a.id', '=', 'log_lihat.id_pel')
        ->Join('port_foto as b', 'b.id', '=', 'log_lihat.id_foto')
        ->select("log_lihat.*",
        "b.foto"
        ,db::raw('count(log_lihat.id_foto) as jml'))
        ->groupby('log_lihat.id_foto')
        ->orderby('jml','desc')
        ->limit(3)
        ->get();

        $rekom_vid = DB::table('log_lihat')
        ->Join('users as a', 'a.id', '=', 'log_lihat.id_pel')
        ->Join('port_vidio as b', 'b.id', '=', 'log_lihat.id_vidio')
        ->Join('kategori_port as k', 'k.id', '=', 'b.kategori_vid')
        ->select("log_lihat.*",
        "b.vidio", "b.nama_vid", "k.nama"
        ,db::raw('count(log_lihat.id_vidio) as jml'))
        ->groupby('log_lihat.id_vidio')
        ->orderby('jml','desc')
        ->limit(3)
        ->get();
        // 

        $rekom_lay = db::select("
        SELECT a.nama_lay as layanan, 
        (SELECT TIMESTAMPDIFF(YEAR, users.tglhr, CURDATE()) FROM users 
        where users.id = b.id_pel
        ORDER BY count(TIMESTAMPDIFF(YEAR, users.tglhr, CURDATE())) desc
        ) as usia,
        (
        SELECT jk from users
        WHERE users.id = b.id_pel
        ORDER BY count(jk) desc
        )as gender
        FROM layanan a, pemesanan b, users d
        WHERE d.id = b.id_pel
        and b.id_lay = a.id
        and d.role = 'pengunjung'
        GROUP BY a.nama_lay
        ORDER BY count(a.nama_lay) desc
        ");

        $rekom_ratfot = DB::table('rating')
        ->Join('users as a', 'a.id', '=', 'rating.id_pel')
        ->Join('port_foto as b', 'b.id', '=', 'rating.id_foto')
        ->select("rating.*",
        "b.foto"
        ,db::raw('avg(rating.rating) as average'))
        ->groupby('rating.id_foto')
        ->orderby('average','desc')
        ->limit(3)
        ->get();

        $rekom_ratvid = DB::table('rating')
        ->Join('users as a', 'a.id', '=', 'rating.id_pel')
        ->Join('port_vidio as b', 'b.id', '=', 'rating.id_vidio')
        ->select("rating.*",
        "b.vidio"
        ,db::raw('avg(rating.rating) as average'))
        ->groupby('rating.id_vidio')
        ->orderby('average','desc')
        ->limit(3)
        ->get();

        $user = User::all();      
        $kategori_port = kategori_port::all();
        
        $port_foto = DB::table('port_foto')
        ->join('kategori_port', 'kategori_port.id','=','port_foto.kategori_foto')
        ->select('port_foto.*','kategori_port.nama')
        // ->orderBy('port_foto.id','DESC')
        ->inRandomOrder()
        ->limit(15)
        ->get();
        
        $port_vidio = DB::table('port_vidio')
        ->join('kategori_port', 'kategori_port.id','=','port_vidio.kategori_vid')
        ->select('port_vidio.*','kategori_port.nama')
        // ->orderBy('port_vidio.id','DESC')
        ->inRandomOrder()
        ->limit(15)
        ->get();

        $foto_baru = DB::table('port_foto')
        ->join('kategori_port', 'kategori_port.id','=','port_foto.kategori_foto')
        ->select('port_foto.*','kategori_port.nama')
        ->orderBy('port_foto.created_at','DESC')
        ->limit(3)
        ->get();

        $vidio_baru = DB::table('port_vidio')
        ->join('kategori_port', 'kategori_port.id','=','port_vidio.kategori_vid')
        ->select('port_vidio.*','kategori_port.nama')
        ->orderBy('port_vidio.created_at','DESC')
        ->limit(3)
        ->get();
        
        $jmlreview = DB::table('review')->count();
        $jmlmember = DB::table('users')->where('role','pengunjung')->count();
        $jmlstaff = DB::table('users')->where('role','admin layanan')->orwhere('role','admin portofolio')->count();
            // dd($jmlstaff);
        $jmlrating = DB::table('rating')
        ->select(db::raw('avg(rating) as average'))
        ->first();
            // dd($jmlrating);
            
        $user = auth()->user()->id; 
        $foryou = DB::select(
            "SELECT a.nama_lay, a.*,b.*,d.*
            from layanan a, pemesanan b, users d
            where d.id = b.id_pel
            and b.id_lay = a.id
            and d.role = 'pengunjung'
            and d.jk = (select jk from users where id = $user)
            and year(d.tglhr) = (select year(tglhr) from users where id = $user)
            GROUP BY a.nama_lay
            ORDER BY count(a.nama_lay) DESC
            limit 3
            "
        );
        // dd($foryou);

        $data = array(
            'jmlrating' => $jmlrating,
            'kategori_port' => $kategori_port,
            'review' => $review,
            'foryou' => $foryou,
            'rekom_port' => $rekom_port,
            'rekom_vid' => $rekom_vid,
            'rekom_lay' => $rekom_lay,
            'rekom_ratfot' => $rekom_ratfot,
            'rekom_ratvid' => $rekom_ratvid,
            'port_foto' => $port_foto,
            'port_vidio' => $port_vidio,
            'foto_baru' => $foto_baru,
            'vidio_baru' => $vidio_baru,
            'jmlreview' => $jmlreview,
            'jmlmember' => $jmlmember,
            'jmlstaff' => $jmlstaff
        );
        
        return view('pelanggan/welcomepel',$data);
    }

    public function postlog(Request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id]);
        $log_lihat = Log_lihat::create($request->all());
        // $dd($log_lihat);
        return redirect()->back()->with('success','Komentar berhasil ditambahkan');
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
