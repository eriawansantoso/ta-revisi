<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Port_vidio;
use App\Port_foto;
use App\Komentar;

class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexfoto()
    {
        $komentar_foto = DB::table('komentar')
            ->Join('users as a', 'a.id', '=', 'komentar.id_pel')
            ->Join('port_foto as b', 'b.id', '=', 'komentar.id_foto')
            ->select('komentar.*', 'a.name as nama_pel','b.nama_foto as nama_fot')
            ->get();
        $data = array(
            'komentar_foto' => $komentar_foto
        );
        return view('main/komentar_foto', $data);
    }

    public function indexvidio()
    {
        $komentar_vidio = DB::table('komentar')
            ->Join('users as a', 'a.id', '=', 'komentar.id_pel')
            ->Join('port_vidio as b', 'b.id', '=', 'komentar.id_vidio')
            ->select('komentar.*','komentar.id', 'a.name as name','b.nama_vid as nama_vid')
            ->get();
        $data = array(
            'komentar_vidio' => $komentar_vidio
        );
        return view('main/komentar_vidio', $data);
    }

    public function verif_fot($id)
    {
        //function ini untuk mengkonfirmasi bahwa pelanngan sudah melakukan pembayaran
        Komentar::findOrFail($id)->update([
            'status_kom' => 1,
        ]);
        return redirect('/komentar_foto')->with('sukses', 'Data Berhasil Tervalidasi');
    }

    public function verif_vid($id)
    {
        //function ini untuk mengkonfirmasi bahwa pelanngan sudah melakukan pembayaran
        Komentar::findOrFail($id)->update([
            'status_kom' => 1,
        ]);

        return redirect('/komentar_vidio')->with('sukses', 'Data Berhasil Tervalidasi');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyfot($id)
    {
        $komentar = Komentar::findorfail($id);
        $komentar->delete();

        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
    }
    public function destroyvid($id)
    {
        $komentar = Komentar::findorfail($id);
        $komentar->delete();

        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
    }
}
