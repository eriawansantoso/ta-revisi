<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Port_foto;
use App\Port_vidio;
use App\Rating;
use App\User;

use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexfoto()
    {
        $rating_foto = DB::table('rating')
        ->Join('users as a', 'a.id', '=', 'rating.id_pel')
        ->Join('port_foto as b', 'b.id', '=', 'rating.id_foto')
        ->select("rating.*",
        "b.foto", "b.nama_foto"
        ,db::raw('avg(rating.rating) as rata'))
        ->groupby('rating.id_foto')
        ->get();
        $data = array(
            'rating_foto' => $rating_foto
        );
        return view('main/rating_foto', $data);
    }

    public function indexvidio()
    {
        $rating_vidio = DB::table('rating')
        ->Join('users as a', 'a.id', '=', 'rating.id_pel')
        ->Join('port_vidio as b', 'b.id', '=', 'rating.id_vidio')
        ->select("rating.*",
        "b.nama_vid"
        ,db::raw('avg(rating.rating) as rata'))
        ->groupby('rating.id_vidio')
        ->get();
        $data = array(
            'rating_vidio' => $rating_vidio
        );
        return view('main/rating_vidio', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
