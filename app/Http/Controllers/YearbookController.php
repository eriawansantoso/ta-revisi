<?php

namespace App\Http\Controllers;


use App\Yearbook;
use App\Pemesanan;
use Illuminate\Http\Request;
// use Excel;
use App\Export;
use SebastianBergmann\Exporter\Exporter;

use App\Exports\YearbookExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\facades\Excel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class YearbookController extends Controller
{

    public function index(){
        $yearbook = DB::table('Yearbook')
        ->select('yearbook.*')
        ->orderBy('id_pesan','desc')
        ->get();
        
        return view('main/yearbook')->with('yearbook',$yearbook);
    }

    public function excel(Request $request)
    {       
        // $walik = DB::table('yearbook')
        // ->select(DB::raw('DATE_FORMAT(tgllhr_yb, "%d-%b-%Y") as formatted_dob'))
        // ->where('id_pesan',$id_pesan)
        // ->get();
        // $yb=DB::select();
        
        // $yearbook = DB::table('yearbook')
        // ->select('kelas_yb','nama_yb','jk_yb',(DB::raw("CONCAT(tlhr_yb,', ',tgllhr_yb) AS TTL")),'alamat_yb','sosmed_yb','bio_yb')
        // ->where('id_pesan',$id_pesan)
        // ->get();

        $pes = db::table('pemesanan')
        ->select('*')
        ->where('id_pesan',$request->id_pesan )
        ->where('id_pesan', 'like','YB%')
        // ->Where('status_pesan', 4)
        // ->orWhere('id_pesan',$request->id_pesan )
        // ->where('id_pesan', 'like','YB%')
        // ->Where('status_pesan', 10)
        ->first();

        if ($pes) {
            // $term = $request->term;
            // $bios = Bio::all();
            // $bio = $bios->where('id_pesan',$term);
            // $sisayb = DB::table('yearbook')->where('id_pesan', $term)->count();
            // $totalyb = DB::table('pemesanan')->where('id_pesan', $term)->get();

            $id_pesan = $request->id_pesan;
            $pemesanan = DB::table('pemesanan')
            ->select('*')
            ->where('id_pesan',$id_pesan)
            ->first();
            
            // echo dd($pes);
            return Excel::download(new YearbookExport( $request->id_pesan), $pemesanan->id_pesan.'_Yearbook_'.$pemesanan->ket_pesan.'.xlsx');
        }else{
            return redirect()->back()->with('ggl', 'kode tidak ditemukan');
        }


        // dd($yb);
        // return Excel::download(new YearbookExport( $request->id_pesan), $pemesanan->id_pesan.'_Yearbook_'.$pemesanan->ket_pesan.'.xlsx');

    }
}
