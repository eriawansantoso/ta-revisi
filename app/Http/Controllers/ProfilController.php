<?php

namespace App\Http\Controllers;

use App\Kategori_port;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_user = Auth::user()->id;
        $user = User::all()->where('id',$id_user);
        // $user = $users->where('role', '!=','pengunjung');
        return view('main/profiladm')->with('user',$user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama' => 'required'
        ], $message);

        $kategori_port = new Kategori_port;
        $kategori_port->nama = $request->input('nama'); 
        $kategori_port->save();

        return redirect('/kategori')->with('sukses', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'name' => 'required',
            'hp' => 'required',
            'alamat' => 'required',
            'email' => 'required'
        ], $message);
        
                $profil = User::find($id);
                $profil->name = $request->input('name');
                $profil->hp = $request->input('hp');
                $profil->alamat = $request->input('alamat');
                $profil->email = $request->input('email'); 
                $profil->save();
        
                return back()->with('sukses', 'Profil Berhasil Diubah');
    }

    // public function editpsw(Request $request)
    // {
    //     $data = User::findOrFail($request->get('id_psw'));
    //     echo json_encode($data);
    // }
    public function updaprofilowner(Request $request)
    {

        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'password' => 'required|min:6'
        ], $message);
        
                $profil = User::findOrFail($request->id_psw);
                $profil->password = Hash::make($request->input('password')); 

        //         dd($profil);

                $profil->save();
        
                return redirect()->back()->with('sukses', 'Password Berhasil Diubah');
    }

    public function updaprofillay(Request $request)
    {

        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'password' => 'required|min:6'
        ], $message);
        
                $profil = User::findOrFail($request->id_psw);
                $profil->password = Hash::make($request->input('password')); 

        //         dd($profil);

                $profil->save();
        
                return redirect()->back()->with('sukses', 'Password Berhasil Diubah');
    }

    public function updaprofilport(Request $request)
    {

        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'password' => 'required|min:6'
        ], $message);
        
                $profil = User::findOrFail($request->id_psw);
                $profil->password = Hash::make($request->input('password')); 

        //         dd($profil);

                $profil->save();
        
                return redirect()->back()->with('sukses', 'Password Berhasil Diubah');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
