<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Layanan;

class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $layanan = Layanan::all();
        return view('main/layanan')->with('layanan',$layanan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama_lay' => 'required',
            'jenis_lay' => 'required',
            'foto_lay'=> 'required',
            'harga_lay'=> 'required',
            'ket_lay'=> 'required'
        ], $message);

        // menyimpan data file yang diupload ke variabel $filee
        $file = $request->file('foto_lay');
        $extension = $file->getClientOriginalExtension();
        $nama_file = time() . "_" .$file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload, $nama_file);


        $layanan = new Layanan;
        $layanan->nama_lay = $request->input('nama_lay'); 
        $layanan->jenis_lay = $request->input('jenis_lay'); 
        $layanan->foto_lay = $nama_file; 
        $layanan->harga_lay = $request->input('harga_lay'); 
        $layanan->ket_lay = $request->input('ket_lay'); 
        $layanan->save();

        return redirect('/layanan')->with('sukses', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];
        $this->validate($request, [
            'nama_lay' => 'required',
            'jenis_lay' => 'required',
            'harga_lay'=> 'required',
            'ket_lay'=> 'required'
        ], $message);
        
                $layanan = Layanan::find($id);
                $layanan->nama_lay = $request->input('nama_lay'); 
                $layanan->jenis_lay = $request->input('jenis_lay'); 
                if($request->hasfile('foto_lay')){
                    $file = $request->file('foto_lay');
                    $extension = $file->getClientOriginalExtension();
                    $nama_file = time() . "_" .$file->getClientOriginalName();
            
                    // isi dengan nama folder tempat kemana file diupload
                    $tujuan_upload = 'data_file';
                    $file->move($tujuan_upload, $nama_file);
                    $layanan->foto_lay = $nama_file;
                    } 
                $layanan->harga_lay = $request->input('harga_lay'); 
                $layanan->ket_lay = $request->input('ket_lay'); 
                $layanan->save();
        
                return redirect('/layanan')->with('sukses', 'Data Berhasil Diubah');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $layanan = Layanan::find($id);
        $layanan->delete();

        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
    }
}
