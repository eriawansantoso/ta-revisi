<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Layanan;
use App\Pemesanan;
use App\Bio;
use Illuminate\Support\Facades\DB;
use App\Yearbook;

class LaybookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $layanans = Layanan::all();
        // $layanans->ket_lay = Str::limit($layanans->ket_lay, 20);
        $layanan = $layanans->where('jenis_lay','Yearbook');

        $reviewbook = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan, c.jenis_lay as jenis')
        ->where('c.jenis_lay','Yearbook')
        ->get();

        $data = array(
            'layanan' => $layanan,
            'reviewbook' => $reviewbook
        );

        return view('pengunjung/laybook',$data);
    }


    public function indexpel()
    {
        $layanans = Layanan::all();
        // $layanans->ket_lay = Str::limit($layanans->ket_lay, 20);
        $layanan = $layanans->where('jenis_lay','Yearbook');

        $reviewbook = DB::table('review')
        ->Join('users as a', 'a.id', '=', 'review.id_pel')
        ->Join('pemesanan as b', 'b.id', '=', 'review.id_pesan')
        ->Join('layanan as c', 'c.id', '=', 'b.id_lay')
        ->select('review.*', 'a.*', 'b.*', 'c.*', 'a.name as nama_pemesan, c.jenis_lay as jenis')
        ->where('c.jenis_lay','Yearbook')
        ->get();

        $data = array(
            'layanan' => $layanan,
            'reviewbook' => $reviewbook
        );

        return view('pelanggan/laybookpel',$data);
    }


    public function indexpesan($id){

        $layanans = db::table('layanan')
        ->select('*')
        ->where('id',$id)
        ->get();
        $inv = db::table('pemesanan')
        ->select(db::raw('max(id) as id'))
        ->first();

        $nota = $inv->id+1;
        $psn = 'YB00'.$nota;
        $data = [
            'psn' => $psn,
            'layanans' => $layanans
        ];

        return view('pelanggan/pemesananyb',$data);
    }


    public function postpesan(Request $request)
    {
        $request->request->add(['id_pel'=> auth()->user()->id]);
        $pemesanan = Pemesanan::create($request->all());
        
        return  view('pelanggan/pesanberhasil');
    }


    // cek untuk masuk ke halaman multi entry
    public function cekyb(request $request)
    {
        $pes = db::table('pemesanan')
        ->select('*')
        ->where('id_pesan',$request->term )
        ->where('id_pesan', 'like','YB%')
        ->Where('status_pesan', 4)
        ->orWhere('id_pesan',$request->term )
        ->where('id_pesan', 'like','YB%')
        ->Where('status_pesan', 10)
        ->first();

        if ($pes) {
            $term = $request->term;
            $bios = Bio::all();
            $bio = $bios->where('id_pesan',$term);
            $sisayb = DB::table('yearbook')->where('id_pesan', $term)->count();
            $totalyb = DB::table('pemesanan')->where('id_pesan', $term)->get();

            $data = [
                'bio' => $bio,
                'sisayb' => $sisayb,
                'totalyb' => $totalyb
            ];
            // echo dd($pes);
            return view('pengunjung/entriyb',$data)->with('term',$term);
        }else{
            return redirect()->back()->with('sukses', 'kode tidak ditemukan');
        }
    }

    // add biodata temporari
    public function bio(Request $request)
    {
        
        $bio = Bio::insert([
            'id_pesan' => $request->term,
            'kelas_bio' => $request->get('kelas_bio'),
            'nama_bio' => $request->get('nama_bio'),
            'jk_bio' => $request->get('jk_bio'),
            'tlhr_bio' => $request->get('tlhr_bio'),
            'tgllhr_bio' => $request->get('tgllhr_bio'),
            'alamat_bio' => $request->get('alamat_bio'),
            'bio_bio' => $request->get('bio_bio'),
            'sosmed_bio' => $request->get('sosmed_bio')
        ]);

            return redirect()->back()->with('berhasil', 'Data berhasil ditambah');

    }

    public function pelcekyb(request $request)
    {
        $pes = db::table('pemesanan')
        ->select('*')
        ->where('id_pesan',$request->term )
        ->where('id_pesan', 'like','YB%')
        ->Where('status_pesan', 4)
        ->orWhere('id_pesan',$request->term )
        ->where('id_pesan', 'like','YB%')
        ->Where('status_pesan', 10)
        ->first();

        if ($pes) {
            $term = $request->term;
            $bios = Bio::all();
            $bio = $bios->where('id_pesan',$term);
            $sisayb = DB::table('yearbook')->where('id_pesan', $term)->count();
            $totalyb = DB::table('pemesanan')->where('id_pesan', $term)->get();

            $data = [
                'bio' => $bio,
                'sisayb' => $sisayb,
                'totalyb' => $totalyb
            ];
            // echo dd($pes);
            return view('pelanggan/entriyb',$data)->with('term',$term);
        }else{
            return redirect()->back()->with('sukses', 'kode tidak ditemukan');
        }

    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //  pindah data dari bio temporary ke yearbook dan hapus di tabel bio temporari

    public function submityb(Request $request)
    {
        $jmlh = DB::table('bio')->where('id_pesan', $request->term)->count();
        $sisayb = DB::table('yearbook')->where('id_pesan', $request->term)->count();
        $totalyb = DB::table('pemesanan')->where('id_pesan', $request->term)->first();
        $data=$jmlh+$sisayb;

        // dd($data);
        // dd($sisayb, "lebih besar", $totalyb->jmlh_yb);
        if ($jmlh >= 1 ) {
            // if ($data <= $totalyb->jmlh_yb) {
                $bio = DB::table('bio')->where('id_pesan', $request->term)->get();
                // masukan biodata yang di tabel bio ke tabel yb
                foreach ($bio as $yb) {
                    $yub=Yearbook::create([
                        'id_pesan' => $yb->id_pesan,
                        'kelas_yb' => $yb->kelas_bio,
                        'nama_yb' => $yb->nama_bio,
                        'jk_yb' => $yb->jk_bio,
                        'tlhr_yb' => $yb->tlhr_bio,
                        'tgllhr_yb' => $yb->tgllhr_bio,
                        'alamat_yb' => $yb->alamat_bio,
                        'bio_yb' => $yb->bio_bio,
                        'sosmed_yb' => $yb->sosmed_bio,
                    ]);
                }
                DB::table('bio')->where('id_pesan', $request->term)->delete();
                return redirect()->back()->with('yey', 'Data Berhasil Dikirim');
            // }
            // else{
            //     return redirect()->back()->with('full', 'Data melebihi batas dari total Eksemplar yang dipesan');
            // }
        }
        else{
            return redirect()->back()->with('wek', 'Data Kosong');
        }
    }


      // ngedit bio di temporary
      public function edibio(Request $request)
      {
          $data = Bio::findOrFail($request->get('id'));
          echo json_encode($data);
      }
  
      public function updabio(Request $request)
      {
            $data = array(
            'kelas_bio'=>$request->post('kelas_bio'),
            'nama_bio'=>$request->post('nama_bio'),
            'jk_bio'=>$request->post('jk_bio'),
            'tlhr_bio'=>$request->post('tlhr_bio'),
            'tgllhr_bio'=>$request->post('tgllhr_bio'),
            'alamat_bio'=>$request->post('alamat_bio'),
            'bio_bio'=>$request->post('bio_bio'),
            'sosmed_bio'=>$request->post('sosmed_bio'),
            'id_pesan'=>$request->post('id_pesan')
              );

              $simpan = DB::table('bio')
              ->where('id','=',$request->post('id'))
              ->update($data);
  
              if($simpan){
                  return redirect()->back()->with('suksesbio', 'Biodata Berhasil Diubah');
              }else{
                  return redirect()->back()->with('gagalbio', 'Biodata Gagal Diubah');
              } 
      }


      public function pelsubmityb(Request $request)
    {
        $jmlh = DB::table('bio')->where('id_pesan', $request->term)->count();
        $sisayb = DB::table('yearbook')->where('id_pesan', $request->term)->count();
        $totalyb = DB::table('pemesanan')->where('id_pesan', $request->term)->first();
        $data=$jmlh+$sisayb;

        // dd($data);
        // dd($sisayb, "lebih besar", $totalyb->jmlh_yb);
        if ($jmlh >= 1 ) {
            // if ($data <= $totalyb->jmlh_yb) {
                $bio = DB::table('bio')->where('id_pesan', $request->term)->get();
                // masukan biodata yang di tabel bio ke tabel yb
                foreach ($bio as $yb) {
                    $yub=Yearbook::create([
                        'id_pesan' => $yb->id_pesan,
                        'kelas_yb' => $yb->kelas_bio,
                        'nama_yb' => $yb->nama_bio,
                        'jk_yb' => $yb->jk_bio,
                        'tlhr_yb' => $yb->tlhr_bio,
                        'tgllhr_yb' => $yb->tgllhr_bio,
                        'alamat_yb' => $yb->alamat_bio,
                        'bio_yb' => $yb->bio_bio,
                        'sosmed_yb' => $yb->sosmed_bio,
                    ]);
                }
                DB::table('bio')->where('id_pesan', $request->term)->delete();
                return redirect()->back()->with('yey', 'Data Berhasil Dikirim');
            // }
            // else{
            //     return redirect()->back()->with('full', 'Data melebihi batas dari total Eksemplar yang dipesan');
            // }
        }
        else{
            return redirect()->back()->with('wek', 'Data Kosong');
        }
    }

       // ngedit bio di temporary
       public function peledibio(Request $request)
       {
           $data = Bio::findOrFail($request->get('id'));
           echo json_encode($data);
       }
   
       public function pelupdabio(Request $request)
       {
             $data = array(
             'kelas_bio'=>$request->post('kelas_bio'),
             'nama_bio'=>$request->post('nama_bio'),
             'jk_bio'=>$request->post('jk_bio'),
             'tlhr_bio'=>$request->post('tlhr_bio'),
             'tgllhr_bio'=>$request->post('tgllhr_bio'),
             'alamat_bio'=>$request->post('alamat_bio'),
             'bio_bio'=>$request->post('bio_bio'),
             'sosmed_bio'=>$request->post('sosmed_bio'),
             'id_pesan'=>$request->post('id_pesan')
               );
 
               $simpan = DB::table('bio')
               ->where('id','=',$request->post('id'))
               ->update($data);
   
               if($simpan){
                   return redirect()->back()->with('suksesbio', 'Biodata Berhasil Diubah');
               }else{
                   return redirect()->back()->with('gagalbio', 'Biodata Gagal Diubah');
               } 
       }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bio = Bio::find($id);
        $bio->delete();

        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
    }

    public function peldestroy($id)
    {
        $bio = Bio::find($id);
        $bio->delete();

        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
    }
}
