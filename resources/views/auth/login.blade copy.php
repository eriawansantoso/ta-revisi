@extends('pengunjung.masteruser')
@section('content')


<div class="hero-slider-wrap">
        
        <div class="owl-carousel single-slider mb-0">
          
          <div class="cover-v1 jarallax overlay" style="background-image: url('{{asset('pengunjung/images/bg1.jpg')}}');" id="home-section">
            <div class="container">
              <div class="row align-items-center">

                <form method="POST" action="{{ route('login') }}" class="col-md-7 mx-auto">
                  <!-- <div class="col-md-7 mx-auto"> -->
                    <h3 class=" text-center text-white "> <strong> Log in /<a href="/signup"> Daftar Member</a></strong></h3>
                    <div class="col-lg-12 form-group gsap-reveal">
                        <label for="email">Email</label>
                        <input name="email" type="email" class="form-control" id="email">
                    </div>
                    <div class="col-lg-12 form-group gsap-reveal">
                        <label for="password">Password</label>
                        <input name="password" type="password" class="form-control" id="password">
                    </div>
                    <p class="gsap-reveal">
                      <button class="btn btn-outline-pill btn-bg-white--hover btn-custom-light" type="submit">Login</button>
                      <!-- <a href="#" >Login</a> -->
                    </p>
                  <!-- </div> -->
                </form>

              </div>
            </div>

          </div>
          <!-- END .cover-v1 -->

        </div>
        <!-- END owl-carousel -->
        
      </div>

@endsection