@extends('pengunjung.masteruser')
@section('content')

  <div class="cover-v1 jarallax overlay" style="background-image: url('{{asset('pengunjung/images/bg1.jpg')}}');" id="login-section">
    <div class="container">
      <div class="row align-items-center">

      <form method="POST" action="{{ route('login') }}" class="col-md-7 mx-auto">
        @csrf
        
          <h3 class=" text-center text-red "> <strong>Login</strong></h3>
          <div class="col-lg-12 form-group gsap-reveal">
              <label for="email">Email</label>
              <input name="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"  id="email">
              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
          <div class="col-lg-12 form-group gsap-reveal">
              <label for="password">Password</label>
              <input name="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password">
              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
          <p class="gsap-reveal">
            <button class="btn btn-outline-pill btn-bg-white--hover btn-custom-light" type="submit">Login</button>
          </p>
          <p class="gsap-reveal " style="float:right;">Belum jadi member ? <a href="/registeruser">daftar member</a></p>
        
      </form>

      </div>
    </div>
  </div>
      <!-- END .cover-v1 -->

@endsection