@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Daftar Portofolio CV Bee Creative</h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Portofolio</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Portofolio Vidio CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif

                <div class="btn-group mr-1" style="margin-bottom:20px;">
                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#addportvid">
                    <i class="icon-edit icon-white"></i>Tambah Data
                </button>
                <!-- <a href="/tambahmasuk">
                    <button class="btn btn-xs btn-success"><i class="icon-edit icon-white"></i>Tambah Data</button>
                </a> -->
                </div>
                    <div  class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th hidden></th>
                                    <th width="5px">No.</th>
                                    <th width="10px">Nama</th>
                                    <th width="10px">vidio</th>
                                    <th width="15px" >Keterangan</th>
                                    <th width="15px">Kategori</th>
                                    <th width="15px" class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($port_vidio as $l)
                                <tr>
                                    <td hidden>{{$l->id}}</td>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td>{{$l->nama_vid}}</td>
                                    <td> <a href=" {{$l->vidio}}"> {{$l->vidio}}</a></td>
                                    <td> {{$l->ket_vid}} </td>
                                    <td>{{$l->nama}}</td>
                                    <td  class="text-center">
                                        <a href="#" class="btn btn-xs btn-warning editportvid" data-target="#editportvid" data-id="{{$l->id}}" data-nama_vid="{{$l->nama_vid}}" data-vidio="{{$l->vidio}}" data-ket_vid="{{$l->ket_vid}}" data-kategori_vid="{{$l->kategori_vid}}">Ubah</a>
                                        <div class="btn-group mr-2">
                                            <a href="/desvidio:{{$l->id}}" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>

<!-- start modal add Port vid -->
<div class="modal fade" id="addportvid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Portofolio vidio</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{ action('Port_vidioController@store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-body">
            <div class="form-group">
                <label for="kategori_vid" class="col-form-label">Kategori</label>
                    <select class="form-control"  name="kategori_vid" value="{{old('kategori_vid')}}">
                                <option selected="selected">-- Pilih Kategori --</option>
                                @foreach($kategori_port as $f)
                                <option value="{{$f->id}}">{{$f->nama}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('kategori_vid'))
                            <div class="text-danger">
                                {{ $errors->first('kategori_vid')}}
                            </div>
                            @endif
                </div>
                <div class="form-group">
                    <label for="nama_vid" class="col-form-label">Nama Portofolio</label>
                    <input name="nama_vid" type="text" class="form-control" value="{{old('nama_vid')}}" placeholder="masukkan nama portofolio">
                    @if($errors->has('nama_vid'))
                        <div class="text-danger">
                            {{ $errors->first('nama_vid')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label class="col-form-label">Upload Link Vidio</label>
                    <div class="input-group mb-3">
                        <input name="vidio" type="text" class="form-control" value="{{old('vidio')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ket_vid" class="col-form-label">Keterangan Portofolio</label>
                    <input name="ket_vid" type="text" class="form-control" value="{{old('ket_vid')}}" placeholder="masukkan Keterangan Portofolio">
                    @if($errors->has('ket_vid'))
                        <div class="text-danger">
                            {{ $errors->first('ket_vid')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal add portofolio -->

<!-- start modal edit portofolio -->
<div class="modal fade" id="editportvid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data Port Vidio</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/port_vidio" method="POST" id="editvidform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-group">
                    <label for="kategori_vid" class="col-form-label">Kategori</label>
                    <select class="form-control edit_kategori_vid"  name="kategori_vid" value="{{old('kategori_vid')}}">
                                <option selected="selected">-- Pilih Kategori --</option>
                                @foreach($kategori_port as $f)
                                <option value="{{$f->id}}">{{$f->nama}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('kategori_vid'))
                            <div class="text-danger">
                                {{ $errors->first('kategori_vid')}}
                            </div>
                            @endif
                </div>
                <div class="form-group">
                    <label for="nama_vid" class="col-form-label">Nama Portofolio</label>
                    <input name="nama_vid" id="nama_vid" type="text" class="form-control edit_nama_vid" placeholder="masukkan nama vidio">
                    @if($errors->has('nama_vid'))
                        <div class="text-danger">
                            {{ $errors->first('nama_vid')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="vidio" class="col-form-label">Link Vidio</label>
                    <input name="vidio" id="vidio" type="text" class="form-control edit_vidio" placeholder="masukkan link vidio">
                    @if($errors->has('vidio'))
                        <div class="text-danger">
                            {{ $errors->first('vidio')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="ket_vid" class="col-form-label">Keterangan Portofolio</label>
                    <input name="ket_vid" id="ket_vid" type="text" class="form-control edit_ket_vid" placeholder="masukkan keterangan">
                    @if($errors->has('ket_vid'))
                        <div class="text-danger">
                            {{ $errors->first('ket_vid')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit layanan -->
 


@endsection