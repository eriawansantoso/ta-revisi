@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Review Pelanggan</h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Review Pelanggan</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Daftar Review Pemesan Layanan CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif

                    <!-- <div class="btn-group mr-1" style="margin-bottom:20px;">
                        <a href="/cetakyb">
                            <button class="btn btn-xs btn-info"><i class="icon-edit icon-white"></i>Cetak Biodata</button>
                        </a>
                    </div> -->
                    <div  class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th hidden></th>
                                    <th width="8%">Kode Pemesanan</th>
                                    <th width="10%">Nama</th>
                                    <th width="10%">Foto</th>
                                    <th width="30%">Review</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($review as $l)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td hidden>{{$l->id}}</td>
                                    <td>{{$l->id_pesan}}</td>
                                    <td>{{$l->nama_pemesan}}</td>
                                    <td>
                                        <p >
                                            <a href="{{asset('review/'.$l->foto_rev)}}" class="grid-item blog-item w-100 h-100"  data-fancybox="gal">
                                            <img style="width: 50%;  object-fit:cover;" src="{{asset('review/'.$l->foto_rev)}}" alt="Image" class="img-fluid"></a>
                                        </p>
                                    </td> 
                                    <td>{{$l->isi_rev}}</td>
                                    <!-- <td  class="text-center">
                                        <a href="javascript:;" class="btn btn-xs btn-warning editlay" data-target="#editlay" data-id="{{$l->id}}" data-nama_lay="{{$l->nama_lay}}" data-jenis_lay="{{$l->jenis_lay}}" data-foto_lay="{{$l->foto_lay}}" data-harga_lay="{{$l->harga_lay}}" data-ket_lay="{{$l->ket_lay}}" >Ubah</a>
                                        <div class="btn-group mr-2">
                                            <a href="/deslayanan:{{$l->id}}" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div>
                                    </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>

<!-- start modal add layanan -->
<div class="modal fade" id="addlay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Layanan</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{ action('LayananController@store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label for="nama_lay" class="col-form-label">Nama Layanan</label>
                    <input name="nama_lay" type="text" class="form-control" value="{{old('nama_lay')}}" placeholder="masukkan nama layanan">
                    @if($errors->has('nama_lay'))
                        <div class="text-danger">
                            {{ $errors->first('nama_lay')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="jenis_lay">Pilih Jenis Layanan</label>
                    <select class="form-control" name="jenis_lay" >
                        <option value="{{old('jenis_lay')}}" selected="selected">-- Pilih jenis layanan --</option>
                        <option>Photography</option>
                        <option>Videography</option>
                        <option>Yearbook</option>
                        <!-- <option>Events</option> -->
                    </select>
                    @if($errors->has('jenis_lay'))
                        <div class="text-danger">
                            {{ $errors->first('jenis_lay')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label class="col-form-label">Upload Foto Layanan</label>
                    <div class="input-group mb-3">
                        <input name="foto_lay" type="file" class="form-control" value="{{old('foto_lay')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="harga_lay" class="col-form-label">Harga Layanan</label>
                    <input name="harga_lay" type="text" class="form-control" value="{{old('harga_lay')}}" placeholder="masukkan harga layanan">
                    @if($errors->has('harga_lay'))
                        <div class="text-danger">
                            {{ $errors->first('harga_lay')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="ket_lay" class="col-form-label">Keterangan</label>
                    <textarea name="ket_lay" class="form-control" value="{{old('ket_lay')}}" placeholder="masukkan deskripsi layanan" rows="3"></textarea>
                    @if($errors->has('ket_lay'))
                        <div class="text-danger">
                            {{ $errors->first('ket_lay')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal add layanan -->

<!-- start modal edit layanan -->
<div class="modal fade" id="editlay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data Layanan</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/layanan" method="POST" id="editlayform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-group">
                    <label for="nama_lay" class="col-form-label">Nama Layanan</label>
                    <input name="nama_lay" id="nama_lay" type="text" class="form-control edit_nama_lay" placeholder="masukkan nama layanan">
                    @if($errors->has('nama_lay'))
                        <div class="text-danger">
                            {{ $errors->first('nama_lay')}}
                        </div>
                    @endif
                </div>
                <div class="form-group ">
                    <label for="jenis_lay">Pilih Jenis Layanan</label>
                    <select class="form-control edit_jenis_lay" name="jenis_lay" id="jenis_lay" >
                        <option  selected="selected">-- Pilih jenis layanan --</option>
                        <option>Photography</option>
                        <option>Videography</option>
                        <option>Yearbook</option>
                        <!-- <option>Events</option> -->
                    </select>
                    @if($errors->has('jenis_lay'))
                        <div class="text-danger">
                            {{ $errors->first('jenis_lay')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label class="col-form-label">Upload Foto Layanan</label>
                    <div class="input-group mb-3">
                        <input name="foto_lay" id="foto_lay" type="file" class="form-control edit_foto_lay" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="harga_lay" class="col-form-label">Harga Layanan</label>
                    <input name="harga_lay" id="harga_lay" type="text" class="form-control edit_harga_lay" placeholder="masukkan harga layanan">
                    @if($errors->has('harga_lay'))
                        <div class="text-danger">
                            {{ $errors->first('harga_lay')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="ket_lay" class="col-form-label">Keterangan</label>
                    <textarea name="ket_lay" id="ket_lay" class="form-control edit_ket_lay"  placeholder="masukkan deskripsi layanan" rows="3"></textarea>
                    @if($errors->has('ket_lay'))
                        <div class="text-danger">
                            {{ $errors->first('ket_lay')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit layanan -->
 


@endsection

