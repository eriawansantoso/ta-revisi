@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Komentar Portofolio CV Bee Creative</h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">komentar fotografi</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Komentar Fotografi CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first text-center">
                            <thead>
                                <tr>
                                    <th hidden></th>
                                    <!-- <th hidden></th> -->
                                    <th class="text-center" width="5px">No.</th>
                                    <th class="text-center" width="">Nama Pelanggan</th>
                                    <th class="text-center" width="">Nama Fotografi</th>
                                    <th class="text-center" width="">Komentar</th>
                                    <th class="text-center" width="">Status</th>
                                    <th class="text-center" >Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($komentar_foto as $u)
                                <tr>
                                    <td hidden>{{$u->id}}</td>
                                    <th class="text-center" ><?php  echo $no;$no++;?></th>
                                    <td>{{$u->nama_pel}}</td>
                                    <td>{{$u->nama_fot}}</td>
                                    <td>{{$u->komentar}}</td>
                                    <td>
                                    @if($u->status_kom == 0)
                                        <div class="alert alert-warning" >
                                            <span class="text-right"><i class="icon-check"></i> </span>
                                            <span class="font-weight-bold">Belum Tervalidasi</span>
                                        </div>
                                    @else
                                        <div class="alert alert-success" >
                                            <span class="text-right"><i class="icon-check"></i> </span>
                                            <span class="font-weight-bold">Selesai Tervalidasi</span>
                                        </div>
                                    @endif
                                    </td>  
                                    <td class="text-center">
                                    @if($u->status_kom == 0)
                                    <a href="/komentar_foto:{{$u->id}}" onclick="return confirm('APAKAH ANDA YAKIN VALIDASI KOMENTAR INI?')">
                                            <button class="btn btn-xs btn-warning tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Terima</button>
                                        </a>
                                    @else
                                    @endif
                                        <div class="btn-group mr-2">
                                            <a href="/deskomfot:{{$u->id}}" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>


@endsection

