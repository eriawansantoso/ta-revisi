@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Pemesanan Foto/Video </h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Pemesanan Foto/Video</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Daftar Pemesanan Layanan Foto/Video CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif

                    <div  class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">No.</th>
                                    <th hidden></th>
                                    <th>ID</th>
                                    <th style="width: 5%;">Nama Pemesan</th>
                                    <th style="width: 10%;">Nama Layanan</th>
                                    <th style="width: 5%;">Tanggal Pelaksanaan</th>
                                    <th style="width: 15%;">Ket. Tambahan</th>
                                    <th class="text-center">Status</th>
                                    <th style="width: 10%;">Bukti Bayar</th>
                                    <th style="width: 20%;">Total</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($pemesanan as $p)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td hidden>{{$p->id}}</td>
                                    <th>{{$p->id_pesan}}</th>
                                    <td>{{$p->nama_pemesan}}</td>
                                    <td>{{$p->nama_layanan}}</td>
                                    <td>{{$p->tgl_pesan}}</td>
                                    <td>{{$p->ket_pesan}}</td>
                                    <td>
                                        @if($p->status_pesan == 0)
                                        <div class="alert alert-warning" role="alert">
                                            <span class="font-weight-bold">Belum Terverifikasi</span>
                                        </div>
                                        @elseif($p->status_pesan == 1)
                                            <div class="alert alert-success" >
                                                <span class="text-right"><i class="icon-check"></i> </span>
                                                <span class="font-weight-bold">Tanggal Terverifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 2)
                                            <div class="alert alert-warning" role="alert">
                                                <span class="font-weight-bold">Menunggu Verifikasi Tanggal</span>
                                            </div>
                                        @elseif($p->status_pesan == 3)
                                            <div class="alert alert-danger" >
                                                <span class="text-right"><i class="icon-close"></i> </span>
                                                <span class="font-weight-bold">Tanggal Gagal Terverifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 4)
                                            <div class="alert alert-success" >
                                                <span class="text-right"><i class="icon-check"></i> </span>
                                                <span class="font-weight-bold">Selesai Verifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 5)
                                            <div class="alert alert-warning" role="alert">
                                                <span class="font-weight-bold">Menunggu Verifikasi Bukti Pembayaran</span>
                                            </div>
                                        @elseif($p->status_pesan == 6)
                                            <div class="alert alert-danger" >
                                                <span class="text-right"><i class="icon-close"></i> </span>
                                                <span class="font-weight-bold">Bukti Pembayaran Gagal Terverifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 10)
                                            <div class="alert alert-info" >
                                                <span class="text-right"><i class="icon-check"></i> </span>
                                                <span class="font-weight-bold">Pemesanan Selesai</span>
                                            </div>
                                        @else
                                            <div class="alert alert-dark" >
                                                <span class="text-right"><i class="icon-close"></i> </span>
                                                <span class="font-weight-bold">PEMESANAN GAGAL</span>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        @if($p->bukti_pesan > 0)
                                        <p class="text-center">
                                            <a href="{{asset('nota/'.$p->bukti_pesan)}}" class="grid-item blog-item w-100 h-100"  data-fancybox="gal">
                                            <img style="width: 50%;" src="{{asset('nota/'.$p->bukti_pesan)}}" alt="Image" class="img-fluid"></a>
                                        </p>
                                        @else
                                            <p class="text-center">-</p>
                                        @endif
                                    </td>
                                    <td>Rp.{{number_format($p->harga_lay)}},000,-</td>
                                    <td  class="text-center">
                                        @if($p->status_pesan == 4)
                                            <p class="text-center">-</p>
                                        @elseif($p->status_pesan == 10)
                                            <p class="text-center">-</p>
                                        @elseif($p->status_pesan == 7)
                                            <p class="text-center">-</p>
                                        @else
                                            <a href="javascript:;" class="btn btn-xs btn-warning editpesan" data-target="#editpesan" data-id="{{$p->id}}" data-id_pesan="{{$p->id_pesan}}" data-harga_lay="{{$p->harga_lay}}" data-nama_pemesan="{{$p->nama_pemesan}}" data-nama_layanan="{{$p->nama_layanan}}" data-tgl_pesan="{{$p->tgl_pesan}}" data-status_pesan="{{$p->status_pesan}}" data-ket_pesan="{{$p->ket_pesan}}">Ubah</a>
                                        @endif
                                            <!-- <div class="btn-group mr-2">
                                            <a href="/despesan:{{$p->id}}" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div> -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>

<!-- start modal edit pemesanan-->
<div class="modal fade" id="editpesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Verifikasi Pemesanan</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/pemesanan" method="POST" id="editpesanform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <label for="id_pesan" class="col-form-label">ID</label>
                        <input name="id_pesan" id="id_pesan" type="text" class="form-control edit_id_pesan" readonly>
                    </div>
                    <div class="form-group  col-lg-8">
                        <label for="nama_pemesan" class="col-form-label">Nama Pemesan</label>
                        <input name="nama_pemesan" id="nama_pemesan" type="text" class="form-control edit_nama_pemesan" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <label for="tgl_pesan" class="col-form-label">Tgl Pelaksanaan</label>
                        <input name="tgl_pesan" id="tgl_pesan" type="date" class="form-control edit_tgl_pesan" readonly>
                    </div>
                    <div class="form-group col-lg-8">
                        <label for="nama_layanan" class="col-form-label">Nama Layanan</label>
                        <input name="nama_layanan" id="nama_layanan" type="text" class="form-control edit_nama_layanan" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ket_pesan" class="col-form-label">Keterangan</label>
                    <textarea name="ket_pesan" id="ket_pesan" class="form-control edit_ket_pesan" rows="3" readonly></textarea>
                </div>
                <div class="form-group ">
                    <label for="status_pesan">Status Verifikasi</label>
                    <select class="form-control edit_status_pesan" name="status_pesan" id="status_pesan" >
                        <option  selected="selected">-- Pilih status verifikasi pemesanan --</option>
                        <option value="0">Pemesanan Belum Terverifikasi</option>
                        <option value="1">Tanggal Terverifikasi</option>
                        <option value="3">Ganti Tanggal</option>
                        <option value="4">Bukti Bayar terverifikasi</option>
                        <option value="6">Upload Bukti Baru</option>
                        <option value="7">Ulangi pemesanan anda</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit pemesanan -->
 


@endsection