@extends('layouts.master')
@section('content')
@foreach($user as $u)
<div class="dashboard-influence">
	            <div class="container-fluid dashboard-content">
	                <!-- ============================================================== -->
	                <!-- pageheader  -->
	                <!-- ============================================================== -->
	                <div class="row">
	                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                        <div class="page-header">
	                            <h3 class="mb-2">Profile </h3>
	                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
	                            <div class="page-breadcrumb">
	                                <nav aria-label="breadcrumb">
	                                    <ol class="breadcrumb">
	                                        <!-- <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li> -->
	                                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
	                                    </ol>
	                                </nav>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <!-- ============================================================== -->
	                <!-- end pageheader  -->
	                <!-- ============================================================== -->
	                <!-- ============================================================== -->
	                <!-- content  -->
	                <!-- ============================================================== -->
	                <!-- ============================================================== -->
	                <!-- influencer profile  -->
	                <!-- ============================================================== -->
	                <div class="row">
					
	                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						@if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif
	                        <div class="card influencer-profile-data">
	                            <div class="card-body">
	                                <div class="row">
	                                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12">
	                                        <div class="text-center">
												<img src="{{asset('pengunjung/images/logo.png')}}" alt="User Avatar" class="rounded-circle user-avatar-xxl">
	                                            <!-- <img src="assets/images/avatar-1.jpg" alt="User Avatar" class="rounded-circle user-avatar-xxl"> -->
	                                            </div>
	                                        </div>
	                                        <div class="col-xl-10 col-lg-8 col-md-8 col-sm-8 col-12">
	                                            <div class="user-avatar-info">
	                                                <div class="m-b-20">
	                                                    <div class="user-avatar-name">
	                                                        <h2 class="mb-1">{{$u->name}}</h2>
	                                                    </div>
	                                                        <p class="d-inline-block font-weight-bold">({{$u->role}}) </p>
	                                                </div>
	                                                <!--  <div class="float-right"><a href="#" class="user-avatar-email text-secondary">www.henrybarbara.com</a></div> -->
	                                                <div class="user-avatar-address">
	                                                    <p class="border-bottom pb-3">
	                                                        <span class="mb-2 d-xl-inline-block d-block "><i class="fa fa-map-marker-alt mr-2 text-primary "></i>{{$u->alamat}}</span>
	                                                        <span class="mb-2 ml-xl-4 d-xl-inline-block d-block"><i class="fa fa-phone mr-2 text-primary "></i> {{$u->hp}}  </span>
	                                                        <span class="mb-2 ml-xl-4 d-xl-inline-block d-block"><i class="fa fa-envelope mr-2 text-primary "></i> {{$u->email}}  </span>
	                                                    </p>
	                                                    <div class="mt-3">
	                                                        <a href="javascript:;" class="badge badge-light mr-1 text-secondary editusr" data-target="#editusr" data-id="{{$u->id}}" data-role="{{$u->role}}" data-name="{{$u->name}}" data-hp="{{$u->hp}}" data-alamat="{{$u->alamat}}" data-email="{{$u->email}}">ubah profil</a> 
															
                                                            @if(auth()->user()->role == 'owner')
                                                            <a href="javascript:;" class="badge badge-light mr-1 text-secondary editpsw"  data-toggle="modal" data-target="#editpsw" data-id_psw="{{$u->id}}" data-role="{{$u->role}}">ubah password</a>
                                                            @elseif(auth()->user()->role == 'admin layanan')
                                                            <a href="javascript:;" class="badge badge-light mr-1 text-secondary editpswlay"  data-toggle="modal" data-target="#editpswlay" data-id_psw="{{$u->id}}" data-role="{{$u->role}}">ubah password</a>
                                                            @elseif(auth()->user()->role == 'admin portofolio')
                                                            <a href="javascript:;" class="badge badge-light mr-1 text-secondary editpswport"  data-toggle="modal" data-target="#editpswport" data-id_psw="{{$u->id}}" data-role="{{$u->role}}">ubah password</a>
                                                            @endif
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- <div class="border-top user-social-box">
	                                    <div class="user-social-media d-xl-inline-block"><span class="mr-2 twitter-color"> <i class="fab fa-twitter-square"></i></span><span>13,291</span></div>
	                                    <div class="user-social-media d-xl-inline-block"><span class="mr-2  pinterest-color"> <i class="fab fa-pinterest-square"></i></span><span>84,019</span></div>
	                                    <div class="user-social-media d-xl-inline-block"><span class="mr-2 instagram-color"> <i class="fab fa-instagram"></i></span><span>12,300</span></div>
	                                    <div class="user-social-media d-xl-inline-block"><span class="mr-2  facebook-color"> <i class="fab fa-facebook-square "></i></span><span>92,920</span></div>
	                                    <div class="user-social-media d-xl-inline-block "><span class="mr-2 medium-color"> <i class="fab fa-medium"></i></span><span>291</span></div>
	                                    <div class="user-social-media d-xl-inline-block"><span class="mr-2 youtube-color"> <i class="fab fa-youtube"></i></span><span>1291</span></div>
	                                </div> -->
	                            </div>
	                        </div>
	                    </div>
	                    <!-- ============================================================== -->
	                    <!-- end influencer profile  -->
	                    <!-- ============================================================== -->
	                    

        <!-- ============================================================== -->
        <!-- end content  -->
        <!-- ============================================================== -->
    </div>
</div>
@endforeach

<!-- start modal edit profil -->
<div class="modal fade" data-toggle="modal" id="editusr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profil</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/editusr" method="POST" id="editusrform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-group">
                    <label for="name" class="col-form-label">Nama</label>
                    <input name="name" type="text" id="name" class="form-control edit_name" value="{{old('name')}}" required>
                    @if($errors->has('name'))
                        <div class="text-danger">
                            {{ $errors->first('name')}}
                        </div>
                    @endif
                </div>
				<div class="form-group">
                    <label for="email" class="col-form-label">Email</label>
                    <input name="email" type="text" id="email" class="form-control edit_email" value="{{old('email')}}" required>
                    @if($errors->has('email'))
                        <div class="text-danger">
                            {{ $errors->first('email')}}
                        </div>
                    @endif
                </div>
				<div class="form-group">
                    <label for="hp" class="col-form-label">No. HP</label>
                    <input name="hp" type="text" id="hp" class="form-control edit_hp" value="{{old('hp')}}" required>
                    @if($errors->has('hp'))
                        <div class="text-danger">
                            {{ $errors->first('hp')}}
                        </div>
                    @endif
                </div>
				<div class="form-group">
                    <label for="alamat" class="col-form-label">Alamat</label>
                    <input name="alamat" type="text" id="alamat" class="form-control edit_alamat" value="{{old('alamat')}}" required>
                    @if($errors->has('alamat'))
                        <div class="text-danger">
                            {{ $errors->first('alamat')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit user -->


<!-- start modal edit pwrd owner -->
<div class="modal fade"  data-toggle="modal" id="editpsw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Password</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{route('updaprofilowner')}}" id="editpswform" name="form4" id="form4" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    @foreach($user as $u)
					<input type="text" name="id_psw" id="id_psw" class="form-control" value="{{$u->id}}" readonly>
                    @endforeach
                    <div class="form-group ">
                        <label for="password" class="detail-label">Masukkan Password Baru</label>
                        <div class="form-group">
                            <input name="password" id="password" type="password" class="form-control" required>
                        </div>
                    </div>
                    <!-- <div class="form-group ">
                        <label for="password" class="detail-label">Ulangi Password Baru</label>
                        <div class="form-group">
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                	<button type="submit" class="btn btn-warning">Simpan Perubahan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal ubah bukti -->

<!-- start modal edit pwrd lay -->
<div class="modal fade"  data-toggle="modal" id="editpswlay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Password</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{route('updaprofillay')}}" id="editpswform" name="form4" id="form4" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    @foreach($user as $u)
					<input type="text" name="id_psw" id="id_psw" class="form-control" value="{{$u->id}}" readonly>
                    @endforeach
                    <div class="form-group ">
                        <label for="password" class="detail-label">Masukkan Password Baru</label>
                        <div class="form-group">
                            <input name="password" id="password" type="password" class="form-control" required>
                        </div>
                    </div>
                    <!-- <div class="form-group ">
                        <label for="passwordlay" class="detail-label">Ulangi Password Baru</label>
						<input id="ConfirmPassword" type="password" class="form-control" name="confpassword" required>
                        <span id='CheckPasswordMatch'></span>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                	<button type="submit" class="btn btn-warning">Simpan Perubahan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal ubah bukti -->

<!-- start modal edit pwrd port -->
<div class="modal fade"  data-toggle="modal" id="editpswport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Password</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{route('updaprofilport')}}" id="editpswform" name="form4" id="form4" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    @foreach($user as $u)
					<input type="text" name="id_psw" id="id_psw" class="form-control" value="{{$u->id}}" readonly>
                    @endforeach
                    <div class="form-group ">
                        <label for="password" class="detail-label">Masukkan Password Baru</label>
                        <div class="form-group">
                            <input name="password" id="password" type="password" class="form-control" required>
                        </div>
                    </div>
                    <!-- <div class="form-group ">
                        <label for="password" class="detail-label">Ulangi Password Baru</label>
                        <div class="form-group">
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                	<button type="submit" class="btn btn-warning">Simpan Perubahan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal ubah bukti -->

@endsection