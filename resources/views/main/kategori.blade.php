@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Kategori Portofolio CV Bee Creative</h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">kategori</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Daftar Kategori Portofolio CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif

                <div class="btn-group mr-1" style="margin-bottom:20px;">
                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#addkat">
                    <i class="icon-edit icon-white"></i>Tambah Data
                </button>
                <!-- <a href="/tambahmasuk">
                    <button class="btn btn-xs btn-success"><i class="icon-edit icon-white"></i>Tambah Data</button>
                </a> -->
                </div>
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first text-center">
                            <thead>
                                <tr>
                                    <th hidden></th>
                                    <!-- <th hidden></th> -->
                                    <th class="text-center" width="5px">No.</th>
                                    <th class="text-center" width="">Nama</th>
                                    <th class="text-center" >Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($kategori_port as $u)
                                <tr>
                                    <td hidden>{{$u->id}}</td>
                                    <th class="text-center" ><?php  echo $no;$no++;?></th>
                                    <td>{{$u->nama}}</td>
                                    <td class="text-center">
                                        <a href="#"  class="btn btn-xs btn-warning editkat"  data-target="#editkat" data-id="{{$u->id}}" data-nama="{{$u->nama}}" >Ubah</a>
                                        <!-- <div class="btn-group mr-2">
                                            <a href="" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div> -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>

<!-- start modal add kategori -->
<div class="modal fade" id="addkat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{ action('Kategori_portController@store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label for="nama" class="col-form-label">Nama Kategori</label>
                    <input name="nama" type="text" class="form-control" value="{{old('nama')}}" placeholder="masukkan kategori">
                    @if($errors->has('nama'))
                        <div class="text-danger">
                            {{ $errors->first('nama')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal add user -->


<!-- start modal edit user -->
<div class="modal fade" data-toggle="modal" id="editkat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/kategori" method="POST" id="editkatform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-group">
                    <label for="nama" class="col-form-label">Nama Kategori</label>
                    <input name="nama" type="text" id="nama" class="form-control edit_nama" value="{{old('nama')}}" placeholder="masukkan nama kategori">
                    @if($errors->has('nama'))
                        <div class="text-danger">
                            {{ $errors->first('nama')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit user -->



@endsection

