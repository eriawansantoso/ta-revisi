@extends('layouts.master')
@section('content')
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Laporan Pemesanan</h2>
                    <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <!-- <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li> -->
                                <li class="breadcrumb-item active" aria-current="page">Laporan Pemesanan</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        
        <!-- <div class="page-header">
                <h2 class="text-center">Periode 2019-2024</h2>
            </div> -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                <h5 class="card-header">Laporan Pemesanan Selesai Tiap Bulan</h5>
                <div class="card-body">
                
                    <div  class="table-responsive">
                        <div class="row input-daterange">
                            <div class="col-md-5">
                                <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                            </div>
                            <div class="col-md-5">
                                <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                            </div>
                            <div class="col-md-2">
                                <button type="button" name="filter" id="filter" class="btn btn-primary btn-sm">Filter</button>
                                <button type="button" name="refresh" id="refresh" class="btn btn-default btn-sm">Refresh</button>
                            </div>
                        </div>
                        <br />
                        <table id="order_table" class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Pesan</th>
                                    <th>Nama</th>
                                    <th>Layanan</th>
                                    <th>Jenis</th>
                                    <th>Tgl Pesan</th>
                                    <th >Tgl</th>
                                    <!-- <th >Opsi</th>
                                    <th >tgl pesan</th> -->
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                <div class="card">
                    <h5 class="card-header">Laporan Pemesanan Selesai</h5>
                    <div class="card-body">
                        <div  class="table-responsive">
                            <table id="datatable"  class="table table-striped table-bordered first">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Layanan</th>
                                        <th>Jenis</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $no=1; ?>
                                        @foreach($laporanselesai as $ls)
                                    <tr>
                                        <th><?php  echo $no;$no++;?></th>
                                        <td>{{$ls->nama_pemesan}}</td>
                                        <td>{{$ls->nama_layanan}}</td>
                                        <td>{{$ls->jenis_lay}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                <div class="card">
                <h5 class="card-header">Laporan Pemesanan Belum Selesai</h5>
                <div class="card-body">
                
                    <div  class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Layanan</th>
                                    <th>Jenis</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($laporanbelum as $lp)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td>{{$ls->nama_pemesan}}</td>
                                    <td>{{$lp->nama_layanan}}</td>
                                    <td>{{$lp->jenis_lay}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>

    </div>
</div>

<!-- start modal detil bulanan -->
<div class="modal fade" id="detilbulanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Biodata Anda</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <div class="row modal-body">
                <div class="detail-v1 col-lg-4 mb-3">
                    <label class="detail-label font-weight-bold">ID Pemesanan</label>
                    <p id="id_pesan" class="detail-val"></p>
                </div>
                <div class="detail-v1 col-lg-4 mb-3">
                    <label class="detail-label font-weight-bold">Tgl Pesan</label>
                    <p id="tanggal" class="detail-val"></p>
                </div>
                <div class="detail-v1 col-lg-4 mb-3">
                    <label class="detail-label font-weight-bold">Nama</label>
                    <p id="nama_pemesan" class="detail-val"></p>
                </div>
                <div class="detail-v1 col-lg-4 mb-3">
                    <label class="detail-label font-weight-bold">Jenis</label>
                    <p id="jenis_lay" class="detail-val"></p>
                </div>
                <div class="detail-v1 col-lg-4 mb-3">
                    <label class="detail-label font-weight-bold">Layanan</label>
                    <p id="nama_layanan" class="detail-val"></p>
                </div>
                
                <div class="detail-v1 col-lg-4 mb-3">
                    <label class="detail-label font-weight-bold">Tgl Pesan</label>
                    <p id="tanggal" class="detail-val"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
                <!-- <button type="submit"  class="btn btn-warning">Ubah</button> -->
            </div>
        </div>
    </div>
</div>
<!-- end modal detil bulanan -->

@endsection
@section('js')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>


<script>
$(document).ready(function(){
    $('.input-daterange').datepicker({
        todayBtn:'linked',
        format:'yyyy-mm-dd',
        autoclose:true
    });

    load_data();

    function load_data(from_date = '', to_date = '')
    {
        var  table=$('#order_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("laporan") }}',
                data:{from_date:from_date, to_date:to_date}
            },
            "order": [[ 6, "desc" ]],
            columns: [
                {
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data:'id_pesan',
                    name:'id_pesan'
                },
                {
                    data:'nama_pemesan',
                    name:'nama_pemesan'
                },
                {
                    data:'nama_layanan',
                    name:'nama_layanan'
                },
                {
                    data:'jenis_lay',
                    name:'jenis_lay'
                },
                {
                    data:'tanggal',
                    name:'tanggal'
                },
                {
                    "targets": [ 6 ],
                    "visible": false,
                    "searchable": false,
                    data:'tgl',
                    name:'tgl'
                }
            ]
        });

        $('#order_table tbody').on( 'click', 'button', function () {
            var data = table.row( $(this).parents('tr') ).data();
            // table.rows( {page: 'current'} ).deselect();
            // table.row( $(this).parents('tr') ).select();
            c = table.rows( { selected: true } ).nodes().length;
            console.log("# selected rows after 'click': ", c);
                                    
            // craft modal body                                           
            $('#id_pesan').html(data.id_pesan);
            $('#nama_pemesan').html(data.nama_pemesan);
            $('#tanggal').html(data.tanggal);                           
            $('#detilbulanan').modal('show'); 
        });
    }

    $('#filter').click(function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date != '' &&  to_date != ''){
            $('#order_table').DataTable().destroy();
            load_data(from_date, to_date);
        }
        else {
            alert('Both Date is required');
        }
    });

    $('#refresh').click(function(){
        $('#from_date').val('');
        $('#to_date').val('');
        $('#order_table').DataTable().destroy();
        load_data();
    });

    });
</script>



@endsection
