@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Daftar Portofolio CV Bee Creative</h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Portofolio</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Portofolio Foto CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif

                <div class="btn-group mr-1" style="margin-bottom:20px;">
                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#addportfot">
                    <i class="icon-edit icon-white"></i>Tambah Data
                </button>
                <!-- <a href="/tambahmasuk">
                    <button class="btn btn-xs btn-success"><i class="icon-edit icon-white"></i>Tambah Data</button>
                </a> -->
                </div>
                    <div  class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    
                                    <th>No.</th>
                                    <th hidden></th>
                                    <th style="width: 30%;">Nama</th>
                                    <th>Foto</th>
                                    <th style="width: 15%;">Keterangan</th>
                                    <th>Kategori</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($port_foto as $l)
                                <tr>
                                    
                                    <th><?php  echo $no;$no++;?></th>
                                    <td hidden>{{$l->id}}</td>
                                    <td>{{$l->nama_foto}}</td>
                                    <td><img src="{{asset('portofolio_file/'.$l->foto)}}" width="60px;" height="60px;" style="object-fit:cover;" alt="Image"></td>
                                    <td>{{$l->ket_foto}}</td>
                                    <td>{{$l->nama}}</td>
                                    <td  class="text-center">
                                        <a href="#" class="btn btn-xs btn-warning editportfot" data-target="#editportfot" data-id="{{$l->id}}" data-nama_foto="{{$l->nama_foto}}" data-foto="{{$l->foto}}" data-ket_foto="{{$l->ket_foto}}" data-kategori_foto="{{$l->kategori_foto}}">Ubah</a>
                                        <div class="btn-group mr-2">
                                            <a href="/desfoto:{{$l->id}}" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>

<!-- start modal add Port Foto -->
<div class="modal fade" id="addportfot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Portofolio Foto</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{ action('Port_fotoController@store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-body">
            <div class="form-group">
                    <label for="kategori_foto" class="col-form-label">Kategori</label>
                    <select class="form-control"  name="kategori_foto" value="{{old('kategori_foto')}}">
                                <option selected="selected">-- Pilih Kategori --</option>
                                @foreach($kategori_port as $f)
                                <option value="{{$f->id}}">{{$f->nama}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('kategori_foto'))
                            <div class="text-danger">
                                {{ $errors->first('kategori_foto')}}
                            </div>
                            @endif
                </div>
                <div class="form-group">
                    <label class="col-form-label">Upload Foto Portofolio</label>
                    <div class="input-group mb-3">
                        <input name="foto" type="file" class="form-control" value="{{old('foto')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_foto" class="col-form-label">Nama Portofolio</label>
                    <input name="nama_foto" type="text" class="form-control" value="{{old('nama_foto')}}" placeholder="masukkan nama portofolio">
                    @if($errors->has('nama_foto'))
                        <div class="text-danger">
                            {{ $errors->first('nama_foto')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="ket_foto" class="col-form-label">Keterangan Portofolio</label>
                    <textarea name="ket_foto" id="ket_foto" class="form-control" value="{{old('ket_foto')}}" cols="4" rows="4"></textarea>
                    @if($errors->has('ket_foto'))
                        <div class="text-danger">
                            {{ $errors->first('ket_foto')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal add portofolio -->

<!-- start modal edit portofolio -->
<div class="modal fade" id="editportfot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data Port Foto</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/port_foto" method="POST" id="editfotoform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-group">
                    <label for="kategori_foto" class="col-form-label">Kategori</label>
                    <select class="form-control edit_kategori_foto"  name="kategori_foto" value="{{old('kategori_foto')}}">
                        @foreach($kategori_port as $f)
                        <option value="{{$f->id}}" >{{$f->nama}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('kategori_foto'))
                    <div class="text-danger">
                        {{ $errors->first('kategori_foto')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label class="col-form-label">Upload Portofolio</label>
                    <div class="input-group mb-3">
                        <input name="foto" id="foto" type="file" class="form-control edit_foto" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_foto" class="col-form-label">Nama Portofolio</label>
                    <input name="nama_foto" id="nama_foto" type="text" class="form-control edit_nama_foto" placeholder="masukkan nama port_foto">
                    @if($errors->has('nama_foto'))
                        <div class="text-danger">
                            {{ $errors->first('nama_foto')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="ket_foto" class="col-form-label">Keterangan Portofolio</label>
                    <textarea name="ket_foto" id="ket_foto" class="form-control edit_ket_foto" value="{{old('ket_foto')}}" cols="4" rows="4"></textarea>
                    @if($errors->has('ket_foto'))
                        <div class="text-danger">
                            {{ $errors->first('ket_foto')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit layanan -->
 


@endsection