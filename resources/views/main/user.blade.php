@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Daftar User CV Bee Creative</h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">user</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">User CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif

                <div class="btn-group mr-1" style="margin-bottom:20px;">
                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#addusr">
                    <i class="icon-edit icon-white"></i>Tambah Data
                </button>
                <!-- <a href="/tambahmasuk">
                    <button class="btn btn-xs btn-success"><i class="icon-edit icon-white"></i>Tambah Data</button>
                </a> -->
                </div>
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first text-center">
                            <thead>
                                <tr>
                                    <th hidden></th>
                                    <th class="text-center" width="5px">No.</th>
                                    <th class="text-center" width="">Nama</th>
                                    <th class="text-center" width="">Alamat</th>
                                    <th class="text-center" width="">No.HP</th>
                                    <th class="text-center" width="">Username</th>
                                    <th class="text-center" width="">Level</th>
                                    <th class="text-center" >Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($user as $u)
                                <tr>
                                    <td hidden>{{$u->id}}</td>
                                    <th class="text-center" ><?php  echo $no;$no++;?></th>
                                    <td>{{$u->name}}</td>
                                    <td>{{$u->alamat}}</td>
                                    <td class="text-center" >{{$u->hp}}</td>
                                    <td class="text-center" >{{$u->email}}</td>
                                    <td class="text-center" >{{$u->role}}</td>
                                    <td class="text-center">
                                        <a href="#"  class="btn btn-xs btn-warning editusr"  data-target="#editusr" data-id="{{$u->id}}" data-name="{{$u->name}}" data-alamat="{{$u->alamat}}" data-hp="{{$u->hp}}" data-email="{{$u->email}}" data-role="{{$u->role}}" >Ubah</a>
                                        <div class="btn-group mr-2">
                                            <a href="" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>

<!-- start modal add user -->
<div class="modal fade" id="addusr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data User</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{ action('UserController@store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="col-form-label">Nama User</label>
                    <input name="name" type="text" class="form-control" value="{{old('name')}}" placeholder="masukkan nama user">
                    @if($errors->has('name'))
                        <div class="text-danger">
                            {{ $errors->first('name')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="role">Level</label>
                    <select class="form-control" name="role" >
                        <option value="{{old('role')}}" selected="selected">-- Pilih Level Admin --</option>
                        <option>admin layanan</option>
                        <option>admin portofolio</option>
                        <option>owner</option>
                    </select>
                    @if($errors->has('role'))
                        <div class="text-danger">
                            {{ $errors->first('role')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email" class="col-form-label">Email User</label>
                    <input name="email" id="email" type="text" class="form-control" value="{{old('email')}}" placeholder="masukkan email user">
                    @if($errors->has('email'))
                        <div class="text-danger">
                            {{ $errors->first('email')}}
                        </div>
                    @endif
                </div>
                <input type="text" value="admin123" name="password" id="password" class="form-control" hidden>
                <div class="form-group">
                    <label for="hp" class="col-form-label">No. HP</label>
                    <input name="hp" id="hp" type="text" class="form-control" value="{{old('hp')}}" placeholder="masukkan no hp user">
                    @if($errors->has('hp'))
                        <div class="text-danger">
                            {{ $errors->first('hp')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="alamat" class="col-form-label">Alamat</label>
                    <textarea name="alamat" class="form-control" value="{{old('alamat')}}" placeholder="masukkan alamat user" rows="3"></textarea>
                    @if($errors->has('alamat'))
                        <div class="text-danger">
                            {{ $errors->first('alamat')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal add user -->


<!-- start modal edit user -->
<div class="modal fade" data-toggle="modal" id="editusr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data user</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/user" method="POST" id="editusrform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-group">
                    <label for="name" class="col-form-label">Nama User</label>
                    <input name="name" type="text" id="name" class="form-control edit_name" value="{{old('name')}}" placeholder="masukkan nama user">
                    @if($errors->has('name'))
                        <div class="text-danger">
                            {{ $errors->first('name')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="role">Level</label>
                    <select class="form-control edit_role" name="role" id="role">
                        <option value="{{old('role')}}" selected="selected">-- Pilih Level Admin --</option>
                        <option>admin layanan</option>
                        <option>admin portofolio</option>
                        <option>owner</option>
                    </select>
                    @if($errors->has('role'))
                        <div class="text-danger">
                            {{ $errors->first('role')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email" class="col-form-label">Email User</label>
                    <input name="email" type="text" id="email" class="form-control edit_email" value="{{old('email')}}" placeholder="masukkan email user">
                    @if($errors->has('email'))
                        <div class="text-danger">
                            {{ $errors->first('email')}}
                        </div>
                    @endif
                </div>
                <!-- <div class="form-group">
                    <label for="password" class="col-form-label">Password Baru</label>
                    <input name="password" id="password" type="password" class="form-control " value="" placeholder="masukkan password baru">
                    <input id="hapus1" name="hapus1" type="checkbox" /> Ganti Password
                    @if($errors->has('password'))
                        <div class="text-danger">
                            {{ $errors->first('password')}}
                        </div>
                    @endif
                </div> -->
                <div class="form-group">
                    <label for="hp" class="col-form-label">No. HP</label>
                    <input name="hp" id="hp" type="text" class="form-control edit_hp" value="{{old('hp')}}" placeholder="masukkan no hp user">
                    @if($errors->has('hp'))
                        <div class="text-danger">
                            {{ $errors->first('hp')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="alamat" class="col-form-label">Alamat</label>
                    <textarea name="alamat" id="alamat" class="form-control edit_alamat" value="{{old('alamat')}}" placeholder="masukkan alamat user" rows="3"></textarea>
                    @if($errors->has('alamat'))
                        <div class="text-danger">
                            {{ $errors->first('alamat')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit user -->



@endsection

