@extends('layouts.master')
@section('content')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Pemesanan Yearbook</h2>
                <!-- <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p> -->
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Pemesanan Yearbook </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-8" style="float:right;">
                    <p class="font-weight-bold" style="float:right;">*Panduan Tipe Pembayaran 
                        <a href="javascript:;"data-toggle="modal" data-target="#tipebayar"> Disini</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Daftar Pemesanan Layanan Yearbook CV Bee Creative</h5>
                <div class="card-body">
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(\Session::has('sukses'))
                <div class="alert alert-success" role="alert">
                    <strong>{{\Session::get('sukses')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @elseif(\Session::has('gagal'))
                <div class="alert alert-danger" role="alert">
                    <strong>{{\Session::get('gagal')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif

                    <div  class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">No.</th>
                                    <th hidden></th>
                                    <th>ID</th>
                                    <th style="width: 5%;">Nama Pemesan</th>
                                    <th style="width: 10%;">Nama Layanan</th>
                                    <th style="width: 5%;">Jumlah Eksemplar</th>
                                    <th style="width: 5%;">Tipe Bayar</th>
                                    <th style="width: 15%;">Sekolah</th>
                                    <th style="width: 10%;" class="text-center">Status</th>
                                    <th  style="width: 10%;">Bukti Bayar</th>
                                    <th style="width: 20%;">Total</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($pemesanan as $p)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td hidden>{{$p->id}}</td>
                                    <th>{{$p->id_pesan}}</th>
                                    <td>{{$p->nama_pemesan}}</td>
                                    <td>{{$p->nama_layanan}}</td>
                                    <td class="text-center">{{$p->jmlh_yb}}</td>
                                    <td class="text-center">
                                        @if($p->tipe_bayar == 1)
                                            A
                                        @elseif($p->tipe_bayar == 2)
                                            B
                                        @else
                                            C
                                        @endif
                                    </td>
                                    <td>
                                        @if($p->ket_pesan == '')
                                            -
                                        @else
                                            {{$p->ket_pesan}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($p->status_pesan == 0)
                                        <div class="alert alert-warning" role="alert">
                                            <span class="font-weight-bold">Belum Terverifikasi</span>
                                        </div>
                                        @elseif($p->status_pesan == 1)
                                            <div class="alert alert-success" >
                                                <span class="text-right"><i class="icon-check"></i> </span>
                                                <span class="font-weight-bold">Tipe Pembayaran Terverifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 2)
                                            <div class="alert alert-warning" role="alert">
                                                <span class="font-weight-bold">Menunggu Verifikasi Tipe Pembayaran</span>
                                            </div>
                                        @elseif($p->status_pesan == 3)
                                            <div class="alert alert-danger" >
                                                <span class="text-right"><i class="icon-close"></i> </span>
                                                <span class="font-weight-bold">Tipe Pembayaran Gagal Terverifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 4)
                                            <div class="alert alert-success" >
                                                <span class="text-right"><i class="icon-check"></i> </span>
                                                <span class="font-weight-bold">Selesai Verifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 5)
                                            <div class="alert alert-warning" role="alert">
                                                <span class="font-weight-bold">Menunggu Verifikasi Bukti Pembayaran</span>
                                            </div>
                                        @elseif($p->status_pesan == 6)
                                            <div class="alert alert-danger" >
                                                <span class="text-right"><i class="icon-close"></i> </span>
                                                <span class="font-weight-bold">Bukti Pembayaran Gagal Terverifikasi</span>
                                            </div>
                                        @elseif($p->status_pesan == 10)
                                            <div class="alert alert-info" >
                                                <span class="text-right"><i class="icon-check"></i> </span>
                                                <span class="font-weight-bold">Pemesanan Selesai</span>
                                            </div>
                                        @else
                                            <div class="alert alert-dark" >
                                                <span class="text-right"><i class="icon-close"></i> </span>
                                                <span class="font-weight-bold">PESANAN GAGAL</span>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        @if($p->bukti_pesan > 0)
                                        <p class="text-center">
                                            <a href="{{asset('nota/'.$p->bukti_pesan)}}" class="grid-item blog-item w-100 h-100"  data-fancybox="gal">
                                            <img style="width: 50%;" src="{{asset('nota/'.$p->bukti_pesan)}}" alt="Image" class="lazyload img-fluid"></a>
                                        </p>
                                        @else
                                            <p class="text-center">-</p>
                                        @endif
                                    </td>
                                    <td>
                                        @if($p->jmlh_yb >= 451)
                                        Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000.-
                                        @elseif($p->jmlh_yb <= 150)
                                        Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000.-
                                        @else
                                        Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000.-
                                        @endif
                                    </td>
                                    <td  class="text-center">
                                        @if($p->status_pesan == 4)
                                            <p class="text-center">-</p>
                                        @elseif($p->status_pesan == 10)
                                            <p class="text-center">-</p>
                                        @elseif($p->status_pesan == 7)
                                            <p class="text-center">-</p>
                                        @else
                                        <a href="javascript:;" class="btn btn-xs btn-warning editpesanyb" data-target="#editpesanyb" data-id="{{$p->id}}" data-id_pesan="{{$p->id_pesan}}" data-tipe_bayar="{{$p->tipe_bayar}}" data-nama_pemesan="{{$p->nama_pemesan}}" data-nama_layanan="{{$p->nama_layanan}}" data-jmlh_yb="{{$p->jmlh_yb}}" data-status_pesan="{{$p->status_pesan}}" data-ket_pesan="{{$p->ket_pesan}}">Ubah</a>
                                        @endif
                                        <!-- <div class="btn-group mr-2">
                                            <a href="/despesan:{{$p->id}}" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                                <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Hapus</button>
                                            </a>
                                        </div> -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>

<!-- start modal edit pemesanan yb-->
<div class="modal fade" id="editpesanyb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Verifikasi Pemesanan</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="/pemesananyb" method="POST" id="editpesanybform" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <div class="modal-body">
                <input type="text" name="id" id="id" class="form-control edit_id" hidden>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <label for="id_pesan" class="col-form-label">ID</label>
                        <input name="id_pesan" id="id_pesan" type="text" class="form-control edit_id_pesan" readonly>
                    </div>
                    <div class="form-group  col-lg-8">
                        <label for="nama_pemesan" class="col-form-label">Nama Pemesan</label>
                        <input name="nama_pemesan" id="nama_pemesan" type="text" class="form-control edit_nama_pemesan" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <label class="col-form-label">Tipe Pembayaran</label>
                        <select  class="form-control edit_tipe_bayar"  name="tipe_bayar" id="tipe_bayar" readonly>
                            <option value="1">Tipe A</option>
                            <option value="2">Tipe B</option>
                            <option value="3">Tipe C</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-8">
                        <label for="jmlh_yb" class="col-form-label">Jmlh Eksemplar</label>
                        <input name="jmlh_yb" id="jmlh_yb" type="number" class="form-control edit_jmlh_yb" readonly>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="nama_layanan" class="col-form-label">Nama Layanan</label>
                    <input name="nama_layanan" id="nama_layanan" type="text" class="form-control edit_nama_layanan" readonly>
                </div>
                <div class="form-group">
                    <label for="ket_pesan" class="col-form-label">Sekolah</label>
                    <input name="ket_pesan" id="ket_pesan" type="text" class="form-control edit_ket_pesan" readonly>
                </div>
                <div class="form-group ">
                    <label for="status_pesan">Status Verifikasi</label>
                    <select class="form-control edit_status_pesan" name="status_pesan" id="status_pesan" >
                        <option  selected="selected">-- Pilih status validasi pemesanan --</option>
                        <option value="0">Pemesanan Belum Terverifikasi</option>
                        <option value="1">Tipe Pembayaran Terverifikasi</option>
                        <option value="3">Ganti Tipe Pembayaran</option>
                        <option value="4">Bukti Bayar terverifikasi</option>
                        <option value="6">Upload Bukti Baru</option>
                        <option value="7">Ulangi pemesanan anda</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning">Simpan Perubahan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit pemesanan yb -->
 
<!-- start modal tipe pembayaran -->
<div class="modal fade" id="tipebayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tipe Pembayaran Yearbook</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <div class="modal-body">
            <div class="detail-v1  mb-4">
                <label class="detail-label font-weight-bold">Tipe A</label>
                <p class="detail-val">Pembayaran DP 50% , Pelunasan 50% Setelah Pengiriman.</p>
            </div>
            <div class="detail-v1  mb-4">
                <label class="detail-label font-weight-bold">Tipe B</label>
                <p class="detail-val">Pembayaran DP 10% , Setelah Sesi Foto 30%, Setelah Sesi Layout 30%, Pelunasan 30% Setelah Pengiriman.</p>
            </div>
            <div class="detail-v1   ">
                <label class="detail-label font-weight-bold">Tipe C</label>
                <p class="detail-val">Pembayaran DP 10%, Pelunasan 90% Setelah Pengiriman.</p>
            </div>
            </div>
            <!-- <div class="modal-footer" >
                <small class="text-center" style="color: red; margin-right: 7%;" > hubungi admin via Whatsapp untuk info lebih lanjut 089708264091</small>
            </div> -->
        </div>
    </div>
</div>
<!-- end modal tipe pembayaran -->

@endsection