@extends('layouts.master')
@section('content')
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Dashboard Owner</h2>
                    <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <!-- <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li> -->
                                <li class="breadcrumb-item active" aria-current="page">Dashboard Owner</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        
        <!-- <div class="page-header">
                <h2 class="text-center">Periode 2019-2024</h2>
            </div> -->
        <div class="row">
            <!-- ============================================================== -->
            <!-- dount chart  -->
            <!-- ============================================================== -->
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Layanan Photography </h5>
                    <div class="card-body">
                        <div id="c3chart_pie"></div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end dount chart  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- dount chart  -->
            <!-- ============================================================== -->
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Layanan videography </h5>
                    <div class="card-body">
                        <div id="c3chart_pievid"></div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end dount chart  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- dount chart  -->
            <!-- ============================================================== -->
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Layanan Yearbook </h5>
                    <div class="card-body">
                        <div id="c3chart_piebook"></div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end dount chart  -->
            <!-- ============================================================== -->


            <!-- ============================================================== -->
            <!-- simple line chart  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header text-center">Layanan Terjual Tiap Bulan</h5>
                    <div class="card-body">
                        <canvas id="chartjs_line"></canvas>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end simple line chart  -->
            <!-- ============================================================== -->

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <div class="card">
                    <h5 class="card-header text-center">Jumlah Portofolio</h5>
                    <div class="card-body">
                        <canvas id="chartjs_bar"></canvas>
                    </div>
                </div>
            </div>
            
        @foreach($total_kom as $tot)
        @foreach($total_kom_val as $t)
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                <h5 class="text-muted">Komentar Tervalidasi</h5>
                    <div class="metric-value d-inline-block">
                        <h1 class="mb-1">{{$t->jml}}/{{$tot->jml}}</h1>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @foreach($total_kom_unval as $t)
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                <h5 class="text-muted">Komentar Belum Tervalidasi</h5>
                    <div class="metric-value d-inline-block">
                        <h1 class="mb-1">{{$t->jml}}/{{$tot->jml}}</h1>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @endforeach
        
        </div>
        <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="card">
                <h5 class="card-header">Komentar Fotografi Terbanyak</h5>
                <div class="card-body">
                
                    <div  class="table-responsive">
                        <table id="" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Jumlah komentar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($kom_fot as $l)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td>{{$l->nama_foto}}</td>
                                    <td>{{$l->jumlah}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="card">
                <h5 class="card-header">Komentar Videografi Terbanyak</h5>
                <div class="card-body">
                
                    <div  class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Jumlah komentar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($kom_vid as $l)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td>{{$l->nama_vid}}</td>
                                    <td>{{$l->jumlah}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            </div>

            <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="card">
                <h5 class="card-header">Rating Fotografi Tertinggi</h5>
                <div class="card-body">
                
                    <div  class="table-responsive">
                        <table id="" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Rating</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($rat_fot as $l)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td>{{$l->nama_foto}}</td>
                                    <td>{{$l->jumlah}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="card">
                <h5 class="card-header">Rating Videografi Tertinggi</h5>
                <div class="card-body">
                
                    <div  class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Rating</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                                    @foreach($rat_vid as $l)
                                <tr>
                                    <th><?php  echo $no;$no++;?></th>
                                    <td>{{$l->nama_vid}}</td>
                                    <td>{{$l->jumlah}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            </div>


    </div>
</div>
@endsection
@section('js')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<!-- <script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left'
  }, function(from_date, to_date, label) {
    console.log("A new date selection was made: " + from_date.format('YYYY-MM-DD') + ' to ' + to_date.format('YYYY-MM-DD'));
  });
});
</script> -->



<script>
(function(window, document, $, undefined) {
        "use strict";
        $(function() {

                if ($('#chartjs_line').length) {
                    var ctx = document.getElementById('chartjs_line').getContext('2d');

                    var myChart = new Chart(ctx, {
                            type: 'line',

                            data: {
                                labels: [{{$bl12vd->bln}} +'/'+ {{$bl12vd->thn}}, {{$bl11vd->bln}} +'/'+ {{$bl11vd->thn}},  {{$bl10vd->bln}} +'/'+ {{$bl10vd->thn}},  {{$bl9vd->bln}} +'/'+ {{$bl9vd->thn}},  {{$bl8vd->bln}} +'/'+ {{$bl8vd->thn}},  {{$bl7vd->bln}} +'/'+ {{$bl7vd->thn}},  {{$bl6vd->bln}} +'/'+ {{$bl6vd->thn}},  {{$bl5vd->bln}} +'/'+ {{$bl5vd->thn}},  {{$bl4vd->bln}} +'/'+ {{$bl4vd->thn}},  {{$bl3vd->bln}} +'/'+ {{$bl3vd->thn}},  {{$bl2vd->bln}} +'/'+ {{$bl2vd->thn}},  {{$bl1vd->bln}} +'/'+ {{$bl1vd->thn}},  {{$bl0vd->bln}} +'/'+ {{$bl0vd->thn}}],
                                
                                datasets: [{
                                    label: 'Videography',
                                    data: [{{$bl12vd->tot}}, {{$bl11vd->tot}}, {{$bl10vd->tot}}, {{$bl9vd->tot}}, {{$bl8vd->tot}}, {{$bl7vd->tot}}, {{$bl6vd->tot}}, {{$bl5vd->tot}}, {{$bl4vd->tot}}, {{$bl3vd->tot}}, {{$bl2vd->tot}}, {{$bl1vd->tot}}, {{$bl0vd->tot}}],

                                    backgroundColor: "rgba(5, 187, 255,0.5)",
                                    borderColor: "rgba(5, 155, 211,0.7)",
                                    borderWidth: 2
                                }, {
                                    label: 'Photography',
                                    data: [{{$bl12FT->tot}}, {{$bl11FT->tot}}, {{$bl10FT->tot}}, {{$bl9FT->tot}}, {{$bl8FT->tot}}, {{$bl7FT->tot}}, {{$bl6FT->tot}}, {{$bl5FT->tot}}, {{$bl4FT->tot}}, {{$bl3FT->tot}}, {{$bl2FT->tot}}, {{$bl1FT->tot}}, {{$bl0FT->tot}}],
                                    backgroundColor: "rgba(255, 64, 123,0.5)",
                                    borderColor: "rgba(255, 64, 123,0.7)",
                                    borderWidth: 2
                                },
                                {
                                    label: 'Yearbook',
                                    data: [{{$bl12YB->tot}}, {{$bl11YB->tot}}, {{$bl10YB->tot}}, {{$bl9YB->tot}}, {{$bl8YB->tot}}, {{$bl7YB->tot}}, {{$bl6YB->tot}}, {{$bl5YB->tot}}, {{$bl4YB->tot}}, {{$bl3YB->tot}}, {{$bl2YB->tot}}, {{$bl1YB->tot}}, {{$bl0YB->tot}}],
                                    backgroundColor: "rgba(239, 176, 5,0.5)",
                                    borderColor: "rgba(211, 156, 7,0.7)",
                                    borderWidth: 2
                                },
                            ]

                            },
                            options: {
                                legend: {
                                    display: true,
                                    position: 'bottom',

                                    labels: {
                                        fontColor: '#71748d',
                                        fontFamily: 'Circular Std Book',
                                        fontSize: 14,
                                    }
                                },

                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            fontSize: 14,
                                            fontFamily: 'Circular Std Book',
                                            fontColor: '#71748d',
                                        }
                                    }],
                                    yAxes: [{
                                        ticks: {
                                            fontSize: 14,
                                            fontFamily: 'Circular Std Book',
                                            fontColor: '#71748d',
                                        }
                                    }]
                                }
                            }
                        


                    });
            }


            if ($('#chartjs_bar').length) {
                var ctx = document.getElementById("chartjs_bar").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Jumlah Portofolio"],
                        datasets: [{
                            label: 'Fotografi',
                            data: [{{$jmlfot->tot}}],
                           backgroundColor: "rgba(52, 244, 0,0.5)",
                                    borderColor: "rgba(36, 166, 1,0.7)",
                            borderWidth: 2
                        }, {
                            label: 'Videografi',
                            data: [{{$jmlvid->tot}}],
                           backgroundColor: "rgba(78, 0, 255,0.5)",
                                    borderColor: "rgba(59, 1, 190,0.7)",
                            borderWidth: 2
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{

                            }]
                        },
                             legend: {
                        display: true,
                        position: 'bottom',

                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,
                        }
                    },

                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 14,
                                fontFamily: 'Circular Std Book',
                                fontColor: '#71748d',
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontSize: 14,
                                fontFamily: 'Circular Std Book',
                                fontColor: '#71748d',
                            }
                        }]
                    }
                }

                    
                });
            }

            if ($('#chartjs_radar').length) {
                var ctx = document.getElementById("chartjs_radar");
                var myChart = new Chart(ctx, {
                    type: 'radar',
                    data: {
                        labels: ["M", "T", "W", "T", "F", "S", "S"],
                        datasets: [{
                            label: 'Almonds',
                           backgroundColor: "rgba(89, 105, 255,0.5)",
                                    borderColor: "rgba(89, 105, 255,0.7)",
                            data: [12, 19, 3, 17, 28, 24, 7],
                            borderWidth: 2
                        }, {
                            label: 'Cashew',
                             backgroundColor: "rgba(255, 64, 123,0.5)",
                                    borderColor: "rgba(255, 64, 123,0.7)",
                            data: [30, 29, 5, 5, 20, 3, 10],
                            borderWidth: 2
                        }]
                    },
                    options: {
                       
                             legend: {
                        display: true,
                        position: 'bottom',

                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,
                        }
                    },

                    
                }

                });
            }


            if ($('#chartjs_polar').length) {
                var ctx = document.getElementById("chartjs_polar").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'polarArea',
                    data: {
                        labels: ["M", "T", "W", "T", "F", "S", "S"],
                        datasets: [{
                            backgroundColor: [
                                "#5969ff",
                                "#ff407b",
                                "#25d5f2",
                                "#ffc750",
                                "#2ec551",
                                "#7040fa",
                                "#ff004e"
                            ],
                            data: [12, 19, 3, 17, 28, 24, 7]
                        }]
                    },
                    options: {
                        
                             legend: {
                        display: true,
                        position: 'bottom',

                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,
                        }
                    },

                    
                }
                });
            }


            if ($('#chartjs_pie').length) {
                var ctx = document.getElementById("chartjs_pie").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: ["M", "T", "W", "T", "F", "S", "S"],
                        datasets: [{
                            backgroundColor: [
                               "#5969ff",
                                "#ff407b",
                                "#25d5f2",
                                "#ffc750",
                                "#2ec551",
                                "#7040fa",
                                "#ff004e"
                            ],
                            data: [12, 19, 3, 17, 28, 24, 7]
                        }]
                    },
                    options: {
                           legend: {
                        display: true,
                        position: 'bottom',

                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,
                        }
                    },

                    
                }
                });
            }


            if ($('#chartjs_doughnut').length) {
                var ctx = document.getElementById("chartjs_doughnut").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["M", "T", "W", "T", "F", "S", "S"],
                        datasets: [{
                            backgroundColor: [
                                 "#5969ff",
                                "#ff407b",
                                "#25d5f2",
                                "#ffc750",
                                "#2ec551",
                                "#7040fa",
                                "#ff004e"
                            ],
                            data: [12, 19, 3, 17, 28, 24, 7]
                        }]
                    },
                    options: {

                             legend: {
                        display: true,
                        position: 'bottom',

                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,
                        }
                    },

                    
                }

                });
            }


        });

})(window, document, window.jQuery);
</script>

<script>
(function(window, document, $, undefined) {
    "use strict";
    $(function() {

        if ($('#c3chart_area').length) {
            var chart = c3.generate({
                bindto: "#c3chart_area",
                data: {
                    columns: [
                        ['data1', 300, 350, 300, 0, 0, 0],
                        ['data2', 130, 100, 140, 200, 150, 50]
                    ],
                    types: {
                        data1: 'area',
                        data2: 'area-spline'
                    },
                    colors: {
                        data1: '#5969ff',
                        data2: '#ff407b',

                    }

                },
                axis: {

                    y: {
                        show: true




                    },
                    x: {
                        show: true
                    }
                }

            });
        }


        if ($('#c3chart_spline').length) {
            var chart = c3.generate({
                bindto: "#c3chart_spline",
                data: {
                    columns: [
                        ['data1', 30, 200, 100, 400, 150, 250],
                        ['data2', 130, 100, 140, 200, 150, 50]
                    ],
                    type: 'spline',
                    colors: {
                         data1: '#5969ff',
                        data2: '#ff407b',

                    }
                },
                axis: {
                    y: {
                        show: true,


                    },
                    x: {
                        show: true,
                    }
                }
            });
        }

        if ($('#c3chart_zoom').length) {
            var chart = c3.generate({
                bindto: "#c3chart_zoom",
                data: {
                    columns: [
                        ['sample', 30, 200, 100, 400, 150, 250, 150, 200, 170, 240, 350, 150, 100, 400, 150, 250, 150, 200, 170, 240, 100, 150, 250, 150, 200, 170, 240, 30, 200, 100, 400, 150, 250, 150, 200, 170, 240, 350, 150, 100, 400, 350, 220, 250, 300, 270, 140, 150, 90, 150, 50, 120, 70, 40]
                    ],
                    colors: {
                        sample: '#5969ff'


                    }
                },
                zoom: {
                    enabled: true
                },
                axis: {
                    y: {
                        show: true,


                    },
                    x: {
                        show: true,
                    }
                }

            });
        }


        if ($('#c3chart_scatter').length) {
            var chart = c3.generate({
                bindto: "#c3chart_scatter",
                data: {
                    xs: {
                        setosa: 'setosa_x',
                        versicolor: 'versicolor_x',
                    },
                    // iris data from R
                    columns: [
                        ["setosa_x", 3.5, 3.0, 3.2, 3.1, 3.6, 3.9, 3.4, 3.4, 2.9, 3.1, 3.7, 3.4, 3.0, 3.0, 4.0, 4.4, 3.9, 3.5, 3.8, 3.8, 3.4, 3.7, 3.6, 3.3, 3.4, 3.0, 3.4, 3.5, 3.4, 3.2, 3.1, 3.4, 4.1, 4.2, 3.1, 3.2, 3.5, 3.6, 3.0, 3.4, 3.5, 2.3, 3.2, 3.5, 3.8, 3.0, 3.8, 3.2, 3.7, 3.3],
                        ["versicolor_x", 3.2, 3.2, 3.1, 2.3, 2.8, 2.8, 3.3, 2.4, 2.9, 2.7, 2.0, 3.0, 2.2, 2.9, 2.9, 3.1, 3.0, 2.7, 2.2, 2.5, 3.2, 2.8, 2.5, 2.8, 2.9, 3.0, 2.8, 3.0, 2.9, 2.6, 2.4, 2.4, 2.7, 2.7, 3.0, 3.4, 3.1, 2.3, 3.0, 2.5, 2.6, 3.0, 2.6, 2.3, 2.7, 3.0, 2.9, 2.9, 2.5, 2.8],
                        ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
                        ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
                    ],
                    type: 'scatter',
                    colors: {
                        setosa: '#5969ff',
                        versicolor: '#ff407b',

                    }
                },
                axis: {
                    y: {
                        show: true,


                    },
                    x: {
                        show: true,
                    }
                }
            });
            setTimeout(function() {
                chart.load({
                    xs: {
                        virginica: 'virginica_x'
                    },
                    columns: [
                        ["virginica_x", 3.3, 2.7, 3.0, 2.9, 3.0, 3.0, 2.5, 2.9, 2.5, 3.6, 3.2, 2.7, 3.0, 2.5, 2.8, 3.2, 3.0, 3.8, 2.6, 2.2, 3.2, 2.8, 2.8, 2.7, 3.3, 3.2, 2.8, 3.0, 2.8, 3.0, 2.8, 3.8, 2.8, 2.8, 2.6, 3.0, 3.4, 3.1, 3.0, 3.1, 3.1, 3.1, 2.7, 3.2, 3.3, 3.0, 2.5, 3.0, 3.4, 3.0],
                        ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
                    ]
                });
            }, 1000);
            setTimeout(function() {
                chart.unload({
                    ids: 'setosa'
                });
            }, 2000);
            setTimeout(function() {
                chart.load({
                    columns: [
                        ["virginica", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
                    ]
                });
            }, 3000);
        }


        if ($('#c3chart_stacked').length) {
            var chart = c3.generate({
                bindto: "#c3chart_stacked",

                data: {
                    columns: [
                        ['data1', 130, 200, 320, 400, 530, 750],
                        ['data2', -130, 10, 130, 200, 150, 250],
                        ['data3', -130, -50, -10, -200, -250, -150]
                    ],
                    type: 'bar',
                    groups: [
                        ['data1', 'data2', 'data3']
                    ],
                    order: 'desc', // stack order by sum of values descendantly. this is default.
                    //      order: 'asc'  // stack order by sum of values ascendantly.
                    //      order: null   // stack order by data definition.

                    colors: {
                        data1: '#5969ff',
                        data2: '#ff407b',
                        data3: '#64ced3'

                    }
                },
                axis: {
                    y: {
                        show: true,


                    },
                    x: {
                        show: true,
                    }
                },
                grid: {
                    y: {
                        lines: [{ value: 0 }]
                    }
                }
            });
            setTimeout(function() {
                chart.load({
                    columns: [
                        ['data4', 1200, 1300, 1450, 1600, 1520, 1820],
                    ]
                });
            }, 1000);
            setTimeout(function() {
                chart.load({
                    columns: [
                        ['data5', 200, 300, 450, 600, 520, 820],
                    ]
                });
            }, 2000);
            setTimeout(function() {
                chart.groups([
                    ['data1', 'data2', 'data3', 'data4', 'data5']
                ])
            }, 3000);
        }


        if ($('#c3chart_combine').length) {
            var chart = c3.generate({
                bindto: "#c3chart_combine",
                data: {
                    columns: [
                        ['data1', 30, 20, 50, 40, 60, 50],
                        ['data2', 200, 130, 90, 240, 130, 220],
                        ['data3', 300, 200, 160, 400, 250, 250],
                        ['data4', 200, 130, 90, 240, 130, 220],
                        ['data5', 130, 120, 150, 140, 160, 150],
                        ['data6', 90, 70, 20, 50, 60, 120],
                    ],
                    type: 'bar',
                    types: {
                        data3: 'spline',
                        data4: 'line',
                        data6: 'area',
                    },
                    groups: [
                        ['data1', 'data2']
                    ],

                    colors: {
                        data1: '#5969ff',
                        data2: '#ff407b',
                        data3: '#25d5f2',
                        data4: '#ffc750',
                        data5: '#2ec551',
                        data6: '#1ba3b9',


                    }

                },
                axis: {
                    y: {
                        show: true,


                    },
                    x: {
                        show: true,
                    }
                }
            });
        }

        if ($('#c3chart_pie').length) {
            var chart = c3.generate({
                bindto: "#c3chart_pie",
                data: {
                    columns: [
                        ['selesai', {{$selesaiFT->tot}}],
                        ['proses', {{$belumFT->tot}}],
                        ['gagal', {{$gagalFT->tot}}]
                    ],
                    type: 'pie',

                    colors: {
                        selesai: '#02da25',
                        proses: '#fed40f',
                        gagal: '#e73232'
                    }
                },
                pie: {
                    label: {
                        format: function(value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }
                }
            });
            
        }

        if ($('#c3chart_pievid').length) {
            var chart = c3.generate({
                bindto: "#c3chart_pievid",
                data: {
                    columns: [
                        ['selesai', {{$selesaiVD->tot}}],
                        ['proses', {{$belumVD->tot}}],
                        ['gagal', {{$gagalVD->tot}}]
                    ],
                    type: 'pie',

                    colors: {
                        selesai: '#02da25',
                        proses: '#fed40f',
                        gagal: '#e73232'
                    }
                },
                pie: {
                    label: {
                        format: function(value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }
                }
            });
            
        }
        if ($('#c3chart_piebook').length) {
            var chart = c3.generate({
                bindto: "#c3chart_piebook",
                data: {
                    columns: [
                        ['selesai', {{$selesaiYB->tot}}],
                        ['proses', {{$belumYB->tot}}],
                        ['gagal', {{$gagalYB->tot}}]
                    ],
                    type: 'pie',

                    colors: {
                        selesai: '#02da25',
                        proses: '#fed40f',
                        gagal: '#e73232'
                    }
                },
                pie: {
                    label: {
                        format: function(value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }
                }
            });
            
        }

        if ($('#c3chart_donut').length) {
            var chart = c3.generate({
                bindto: "#c3chart_donut",
                data: {
                    columns: [
                        ['Selesai', 30],
                        ['Belum_Selesai', 120],
                        ['Gagal', 120]
                    ],
                    type: 'donut',
                    onclick: function(d, i) { console.log("onclick", d, i); },
                    onmouseover: function(d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function(d, i) { console.log("onmouseout", d, i); },

                    colors: {
                         Selesai: '#02da25',
                        Belum_Selesai: '#fed40f',
                        Gagal: '#e73232'
                    }
                },
                donut: {
                    title: "Photography"
                }
            });
        }

        

     

        if ($('#c3chart_gauge').length) {
            var chart = c3.generate({
                bindto: "#c3chart_gauge",
                data: {
                    columns: [
                        ['data1', 91.4]

                    ],
                    type: 'gauge',
                    onclick: function(d, i) { console.log("onclick", d, i); },
                    onmouseover: function(d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function(d, i) { console.log("onmouseout", d, i); },
                    colors: {
                      data1: '#5969ff',
                        data2: '#ff407b',
                        data3: '#25d5f2',
                        data4: '#ffc750',
                        data5: '#2ec551',
                        data6: '#1ba3b9',

                    }
                },
                gauge: {
                    //        label: {
                    //            format: function(value, ratio) {
                    //                return value;
                    //            },
                    //            show: false // to turn off the min/max labels.
                    //        },
                    //    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                    //    max: 100, // 100 is default
                    //    units: ' %',
                    //    width: 39 // for adjusting arc thickness
                },

                size: {
                    height: 320
                }
            });



        }


    });

})(window, document, window.jQuery);
</script>
@endsection
