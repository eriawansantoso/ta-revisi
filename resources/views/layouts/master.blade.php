<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link href="{{asset('assets/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/libs/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/fonts/fontawesome/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/charts/chartist-bundle/chartist.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/charts/morris-bundle/morris.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/charts/c3charts/c3.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/fonts/flag-icon-css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/datepicker/tempusdominus-bootstrap-4.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/datatables/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/datatables/css/buttons.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/datatables/css/select.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/datatables/css/fixedHeader.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/inputmask/css/inputmask.css')}}" />
    <!-- <img src="{{asset('pengunjung/images/logo.png')}}" alt=""> -->
    @yield('css')
    <title>@if (Auth::guest())
                @else 
                {{ Auth::user()->role }}
            @endif
            | CV BEE CREATIVE</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        @include('layouts.navbar')
        @include('layouts.sidebar')
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            @yield('content')
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2021. CV Bee Creative. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
        </div>
    <!-- ============================================================== -->
    <!-- end wrapper  -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="{{asset('assets/vendor/jquery/jquery-3.3.1.min.js')}}"></script>
    <!-- bootstap bundle js -->
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <!-- slimscroll js -->
    <script src="{{asset('assets/vendor/slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets/vendor/multi-select/js/jquery.multi-select.js')}}"></script>
    <!-- main js -->
    <script src="{{asset('assets/libs/js/main-js.js')}}"></script>
    <!-- datatable -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets/vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('assets/vendor/datatables/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/js/data-table.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <!-- chart chartist js -->
    <script src="{{asset('assets/vendor/charts/chartist-bundle/chartist.min.js')}}"></script>
    <script src="{{asset('assets/vendor/charts/charts-bundle/Chart.Bundle.js')}}"></script>

    <!-- <script src="../assets/vendor/charts/charts-bundle/Chart.bundle.js"></script> -->
    <!-- <script src="{{asset('assets/vendor/charts/charts-bundle/chartjs.js')}}"></script> -->
    <!-- sparkline js -->
    <script src="{{asset('assets/vendor/charts/sparkline/jquery.sparkline.js')}}"></script>
    <!-- morris js -->
    <script src="{{asset('assets/vendor/charts/morris-bundle/raphael.min.js')}}"></script>
    <script src="{{asset('assets/vendor/charts/morris-bundle/morris.js')}}"></script>
    <!-- chart c3 js -->
    <script src="{{asset('assets/vendor/charts/c3charts/c3.min.js')}}"></script>
    <script src="{{asset('assets/vendor/charts/c3charts/d3-5.4.0.min.js')}}"></script>
    <script src="{{asset('assets/vendor/charts/c3charts/C3chartjs.js')}}"></script>
    
    <script src="{{asset('assets/libs/js/dashboard-ecommerce.js')}}"></script>
    <!-- datepicker -->
    <script src="{{asset('assets/vendor/datepicker/moment.js')}}"></script>
    <script src="{{asset('assets/vendor/datepicker/tempusdominus-bootstrap-4.js')}}"></script>
    <script src="{{asset('assets/vendor/datepicker/datepicker.js')}}"></script>
    <!-- input mask -->
    <script src="{{asset('assets/vendor/inputmask/js/jquery.inputmask.bundle.js')}}"></script>

    @yield('js')
    @yield('footer')



    <!-- edit layanan -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('click','.editlay',function(e){
               
               $('#editlay').modal('show');

               var id = $(this).data('id');
               var nama_lay = $(this).data('nama_lay');
               // var foto = $(this).data('foto');
               var jenis_lay = $(this).data('jenis_lay');
               var harga_lay = $(this).data('harga_lay');
               var ket_lay = $(this).data('ket_lay');

               $('.edit_id').val(id);
               $('.edit_nama_lay').val(nama_lay);
               // $('.edit_foto').val(foto);
               $('.edit_ket_lay').val(ket_lay);
               $('.edit_jenis_lay').val(jenis_lay);
               $('.edit_harga_lay').val(harga_lay);
               console.log("hai");
               $('#editlayform').attr('action', '/layanan/'+id);
           });
        });
    </script>

    <!-- edit port foto -->
    <script type="text/javascript">
         $(document).ready(function(){
            $(document).on('click','.editportfot',function(e){
               
                $('#editportfot').modal('show');

                var id = $(this).data('id');
                var nama_foto = $(this).data('nama_foto');
                // var foto = $(this).data('foto');
                var ket_foto = $(this).data('ket_foto');
                var kategori_foto = $(this).data('kategori_foto');

                $('.edit_id').val(id);
                $('.edit_nama_foto').val(nama_foto);
                // $('.edit_foto').val(foto);
                $('.edit_ket_foto').val(ket_foto);
                $('.edit_kategori_foto').val(kategori_foto);
                console.log("hai");
                $('#editfotoform').attr('action', '/port_foto/'+id);
            });
        });
    </script>

    <!-- edit port vid -->
    <script type="text/javascript">
         $(document).ready(function(){
            $(document).on('click','.editportvid',function(e){
               
                $('#editportvid').modal('show');

                var id = $(this).data('id');
                var nama_vid = $(this).data('nama_vid');
                var vidio = $(this).data('vidio');
                var ket_vid = $(this).data('ket_vid');
                var kategori_vid = $(this).data('kategori_vid');

                $('.edit_id').val(id);
                $('.edit_nama_vid').val(nama_vid);
                $('.edit_vidio').val(vidio);
                $('.edit_ket_vid').val(ket_vid);
                $('.edit_kategori_vid').val(kategori_vid);

                $('#editvidform').attr('action', '/port_vidio/'+id);
            });
        });
    </script>

    <!-- password confirm jek gagal -->
    <script>
    $(document).ready(function () {
    $("#ConfirmPassword").on('keyup', function(){
        var password = $("#Password").val();
        var confirmPassword = $("#ConfirmPassword").val();
        if (password != confirmPassword)
            $("#CheckPasswordMatch").html("Password does not match !").css("color","red");
        else
            $("#CheckPasswordMatch").html("Password match !").css("color","green");
    });
    });
</script>

    <!-- edit user -->
    <script  type="text/javascript">
         $(document).ready(function(){
            $(document).on('click','.editusr',function(e){
               
                $('#editusr').modal('show');

                var id = $(this).data('id');
                var name = $(this).data('name');
                var role = $(this).data('role');
                var email = $(this).data('email');
                var password = $(this).data('password');
                var hp = $(this).data('hp');
                var alamat = $(this).data('alamat');

                $('.edit_id').val(id);
                $('.edit_name').val(name);
                $('.edit_role').val(role);
                $('.edit_email').val(email);
                $('.edit_password').val(password);
                $('.edit_hp').val(hp);
                $('.edit_alamat').val(alamat);
                console.log("hai");

                $('#editusrform').attr('action', '/user/'+id);
            });
        });
    </script>

    <!-- edit kategori -->
    <script  type="text/javascript">
        $(document).ready(function(){
           $(document).on('click','.editkat',function(e){
               $('#editkat').modal('show');
               var id = $(this).data('id');
               var nama = $(this).data('nama');

               console.log("hai");
               $('.edit_id').val(id);
               $('.edit_nama').val(nama);
               $('#editkatform').attr('action', '/kategori/'+id);
           });
       });
   </script>

    <!-- edit profil -->
    <script  type="text/javascript">
        $(document).ready(function(){
           $(document).on('click','.editusr',function(e){
               $('#editusr').modal('show');
               var id = $(this).data('id');
               var name = $(this).data('name');
               var hp = $(this).data('hp');
               var alamat = $(this).data('alamat');
               var email = $(this).data('email');
               var role = $(this).data('role');

               console.log("hai");
               $('.edit_id').val(id);
               $('.edit_name').val(name);
               $('.edit_hp').val(hp);
               $('.edit_alamat').val(alamat);
               $('.edit_email').val(email);
               if (role == "owner") {
                $('#editusrform').attr('action', '/profil/'+id);
               }
               else if(role == "admin layanan"){
                $('#editusrform').attr('action', '/profiladmlay/'+id);
               }
               else if(role == "admin portofolio"){
                $('#editusrform').attr('action', '/profiladmport/'+id);
               }
           });
       });
   </script>

    <!-- edit pasword -->
  
    <!-- <script  type="text/javascript">
        $(document).ready(function(){
           $(document).on('click','.editpsw',function(e){
               $('#editpsw').modal('show');
               var id_psw = $(this).data('id_psw');
               var role = $(this).data('role');

               console.log("hai");
               $('.edit_id_psw').val(id_psw);
               if (role == "owner") {
                $('#editpswform').attr('action', '/profil');
               }
               else if(role == "admin layanan"){
                $('#editpswform').attr('action', '/profiladmlay');
               }
               else if(role == "admin portofolio"){
                $('#editpswform').attr('action', '/profiladmport');
               }
           });
       });
   </script> -->

   <!-- edit pemesanan -->
   <script  type="text/javascript">
        $(document).ready(function(){
           $(document).on('click','.editpesan',function(e){
               $('#editpesan').modal('show');
               var id = $(this).data('id');
               var id_pesan = $(this).data('id_pesan');
               var nama_pemesan = $(this).data('nama_pemesan');
               var nama_layanan = $(this).data('nama_layanan');
               var tgl_pesan = $(this).data('tgl_pesan');
               var status_pesan = $(this).data('status_pesan');
               var ket_pesan = $(this).data('ket_pesan');
               var harga_lay = $(this).data('harga_lay');

               console.log("hai");
               $('.edit_id').val(id);
               $('.edit_id_pesan').val(id_pesan);
               $('.edit_nama_pemesan').val(nama_pemesan);
               $('.edit_nama_layanan').val(nama_layanan);
               $('.edit_tgl_pesan').val(tgl_pesan);
               $('.edit_status_pesan').val(status_pesan);
               $('.edit_ket_pesan').val(ket_pesan);
               $('.edit_harga_lay').val(harga_lay);
               $('#editpesanform').attr('action', '/pemesanan/'+id);
           });
       });
   </script>

   <!-- edit pemesanan yb -->
   <script  type="text/javascript">
        $(document).ready(function(){
           $(document).on('click','.editpesanyb',function(e){
               $('#editpesanyb').modal('show');
               var id_yb = $(this).data('id');
               var id_pesan_yb = $(this).data('id_pesan');
               var nama_pemesan_yb = $(this).data('nama_pemesan');
               var nama_layanan_yb = $(this).data('nama_layanan');
               var tipe_bayar_yb = $(this).data('tipe_bayar');
               var jmlh_yb_yb = $(this).data('jmlh_yb');
               var status_pesan_yb = $(this).data('status_pesan');
               var ket_pesan_yb = $(this).data('ket_pesan');

               console.log("hai");
               $('.edit_id').val(id_yb);
               $('.edit_id_pesan').val(id_pesan_yb);
               $('.edit_nama_pemesan').val(nama_pemesan_yb);
               $('.edit_nama_layanan').val(nama_layanan_yb);
               $('.edit_tipe_bayar').val(tipe_bayar_yb);
               $('.edit_jmlh_yb').val(jmlh_yb_yb);
               $('.edit_status_pesan').val(status_pesan_yb);
               $('.edit_ket_pesan').val(ket_pesan_yb);
               $('#editpesanybform').attr('action', '/pemesananyb/'+id_yb);
           });
       });
   </script>
   
   <script type="text/javascript">
        $(".postbtn").click(function(e) {

            id = e.target.dataset.id;
            Swal.fire({
            title: "Apakah pembayaran sudah valid?",
            text: "Anda akan mengkonfirmasi pembayaran ini",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, Valid"
            }).then(function(result) {
            if (result.value) {

                Swal.fire(
                "Pembayaran Ter-Konfirmasi!",
                "Data telah Tersimpan.",
                "success"
                );
                $(`#post${id}`).submit();

            } else {

            }
            });
        });
    </script>
    
   
</body>

</html>