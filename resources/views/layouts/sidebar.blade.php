<!-- ============================================================== -->
<!-- left sidebar -->
<!-- ============================================================== -->
<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    @if(auth()->user()->role == 'owner')
                    <li class="nav-item ">
                        <a class="nav-link" href="/dashboardowner" data-toggle="" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1">
                            <i class="fa fa-fw fa-chart-pie"></i>Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/laporan"> <i class="fas fa-fw fa-chevron-circle-down"></i>Laporan Pemesanan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/user"> <i class="fas fa-fw fa-chevron-circle-down"></i>Users</a>
                    </li>
                    @endif
                    @if(auth()->user()->role == 'admin layanan')
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboardlayanan" data-toggle="" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1">
                            <i class="fa fa-fw fa-chart-pie"></i>Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/layanan"  data-toggle="" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"> 
                            <i class="fas fa-th-list"></i> Layanan </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3">
                            <i class="fas fa-cart-arrow-down"></i>Pemesanan
                        </a>
                        <div id="submenu-3" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/pemesanan"> <i class="far fa-file-alt"></i> Foto/Video</a>
                                </li>
                            </ul>
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/pemesananyb"> <i class="far fa-file-alt"></i> Yearbook</a>
                                </li>
                            </ul>
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/reviewe"> <i class="fa fa-clipboard-check"></i> Review Pelanggan</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/bioyb"  data-toggle="" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"> 
                            <i class="fas fa-address-book"></i> Biodata Yearbook </a>
                    </li>
                    @endif
                    @if(auth()->user()->role == 'admin portofolio')
                    <li class="nav-item ">
                        <a class="nav-link" href="/dashboardport" data-toggle="" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1">
                            <i class="fa fa-fw fa-chart-pie"></i>Dashboard <span class="badge badge-success">6</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/kategori"> <i class="fas fa-fw fa-chevron-circle-down"></i>Kategori</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2">
                            <i class="fa fa-fw fa-table"></i>Portofolio
                        </a>
                        <div id="submenu-2" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/port_foto"> <i class="fas fa-fw fa-chevron-circle-down"></i> Fotografi </a>
                                </li>
                            </ul>
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/port_vidio"> <i class="fas fa-fw fa-chevron-circle-down"></i> Vidiografi </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3">
                            <i class="fa fa-fw fa-table"></i>Komentar
                        </a>
                        <div id="submenu-3" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/komentar_foto"> <i class="fas fa-fw fa-chevron-circle-down"></i> Fotografi </a>
                                </li>
                            </ul>
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/komentar_vidio"> <i class="fas fa-fw fa-chevron-circle-down"></i> Vidiografi </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4">
                            <i class="fa fa-fw fa-table"></i>Rating
                        </a>
                        <div id="submenu-4" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/rating_foto"> <i class="fas fa-fw fa-chevron-circle-down"></i> Fotografi </a>
                                </li>
                            </ul>
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="/rating_vidio"> <i class="fas fa-fw fa-chevron-circle-down"></i> Vidiografi </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
        </nav>
    </div>
</div>
<!-- ============================================================== -->
<!-- end left sidebar -->
<!-- ============================================================== -->