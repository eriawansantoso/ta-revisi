<!-- ============================================================== -->
<!-- navbar -->
<!-- ============================================================== -->
<div class="dashboard-header">
    <nav class="navbar navbar-expand-lg bg-white fixed-top">
        <a class="navbar-brand" href="#">CV BEE CREATIVE</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto navbar-right-top">
                <li class="nav-item">
                    <!-- <div id="custom-search" class="top-search-bar">
                        <input class="form-control" type="text" placeholder="Search..">
                    </div> -->
                </li>
                <li class="nav-item dropdown nav-user">
                    <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <i class="fa fa-cog"></i>
                        <!-- <img src="{{asset('assets/images/avatar-1.jpg')}}" alt="" class="user-avatar-md rounded-circle"> -->
                    </a>
                    <!-- <a class="dropdown-item" href="/profiladm"><i class="fas fa-power-off mr-2">Profil</a> -->

                    <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                        <div class="nav-user-info">
                            <h5 class="mb-0 text-white nav-user-name">
                                @if (Auth::guest())
                                    @else 
                                    {{ Auth::user()->name }}
                                @endif</h5>
                        </div>
                        @if(auth()->user()->role == 'owner')
                        <a class="dropdown-item" href="/profil" ><i class="fas fa-cog mr-2"></i>Profile</a>
                        @elseif(auth()->user()->role == 'admin portofolio')
                        <a class="dropdown-item" href="/profiladmport" ><i class="fas fa-cog mr-2"></i>Profile</a>
                        @elseif(auth()->user()->role == 'admin layanan')
                        <a class="dropdown-item" href="/profiladmlay" ><i class="fas fa-cog mr-2"></i>Profile</a>
                        @endif
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-power-off mr-2"></i>Logout</a>
                    </div>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        </div>
    </nav>
</div>
<!-- ============================================================== -->
<!-- end navbar -->
<!-- ============================================================== -->