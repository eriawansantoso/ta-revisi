@extends('pelanggan.masterpelanggan')
@section('content')

<div class="container">
    <div class="portfolio-single-wrap unslate_co--section" id="portfolio-single-section">
        <div class="portfolio-single-inner"   style="margin-top: 100px;">
            <div class="row justify-content-between align-items-stretch">
                <div class="col-lg-8">
            <form action="" class="" method="POST">
            @csrf      
                    <div class="detail-v1">
                        <h3 class="mb-5 gsap-reveal">Masukkan Data Berikut</h3>
                        <div class="form-group row ">
                        <div class="form-group col-lg-6 gsap-reveal">
                            <label class="detail-label">Jumlah eksemplar yearbook</label>
                            <input type="number"  name="jmlh_yb" id="jmlh_yb" class="form-control " required>
                        </div>
                        <div class="form-group col-lg-6 gsap-reveal">
                            <label class="detail-label">Tipe Pembayaran</label>
                            <select  class="form-control "  name="tipe_bayar" id="tipe_bayar" required>
                                <option value="" selected>-- pilih tipe pembayaran --</option>
                                <option value="1">Tipe A</option>
                                <option value="2">Tipe B</option>
                                <option value="3">Tipe C</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-12 gsap-reveal">
                            <span class="detail-label">Nama Sekolah</span>
                            <textarea name="ket_pesan" id="ket_pesan" cols="10" rows="3" class="form-control" required></textarea>
                        </div>
                        <div class="form-group col-lg-4 gsap-reveal">
                            <input type="submit" value="Pesan" class="btn btn-primary btn-md">
                        </div>
                        <div class="col-lg-8" style="float:right;">
                            <p class="gsap-reveal font-weight-bold" style="float:right;">*Baca Tipe Pembayaran 
                                <a href="javascript:;" data-toggle="modal" data-target="#tipebayar"> Disini</a>
                            </p>
                        </div>
                        
                    </div>
                    </div>
                </div>
        @foreach($layanans as $l)
        <input type="hidden" name="id_lay" value="{{$l->id}}">
        <input type="hidden" name="id_pesan" value="{{$psn}}">
        <input type="hidden" name="status_pesan" value="0">
        </form>
            <div class="col-lg-4 pl-lg-5">
                <div class="unslate_co--sticky">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Foto Layanan</span>
                            <p style="width: 30%;"><a href="{{asset('data_file/'.$l->foto_lay)}}" data-fancybox="gal">
                                <img src="{{asset('data_file/'.$l->foto_lay)}}" alt="Image" class="img-fluid"></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Nama Layanan</span>
                            <span class="detail-val">{{$l->nama_lay}}</span>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Harga Layanan</span>
                            <span class="detail-val">Rp.{{$l->harga_lay}}.000 /Eksemplar</span><br>
                            <span class="detail-val"><small class="font-weight-bold">harga diatas untuk jumlah 151-450 Eksemplar</small></span><br>
                            <span class="detail-val"><small class="font-weight-bold text-primary">Kurang dari 150 Eksemplar (+30k/Eksemplar)</small></span><br>
                            <span class="detail-val"><small class="font-weight-bold text-primary" >Lebih dari 450 Eksemplar (-30k/Eksemplar)</small></span>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Kategori Layanan</span>
                            <span class="detail-val">{{$l->jenis_lay}}</span>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Keterangan Layanan</span>
                            <span class="detail-val">{{$l->ket_lay}}</span>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            @endforeach
            
        </div>
    </div>
</div>



@endsection

<!-- start modal tipe pembayaran -->
<div class="modal fade" id="tipebayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tipe Pembayaran Yearbook</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <div class="modal-body">
            <div class="detail-v1  mb-4">
                <span class="detail-label">Tipe A</span>
                <span class="detail-val">Pembayaran DP 50% , Pelunasan 50% Setelah Pengiriman.</span>
            </div>
            <div class="detail-v1  mb-4">
                <span class="detail-label">Tipe B</span>
                <span class="detail-val">Pembayaran DP 10% , Setelah Sesi Foto 30%, Setelah Sesi Layout 30%, Pelunasan 30% Setelah Pengiriman.</span>
            </div>
            <div class="detail-v1  mb-4">
                <span class="detail-label">Tipe C</span>
                <span class="detail-val">Pembayaran DP 10%, Pelunasan 90% Setelah Pengiriman.</span>
            </div>
            </div>
            <div class="modal-footer" >
                <small class="text-center" style="color: red; margin-right: 7%;" > hubungi admin via Whatsapp untuk info lebih lanjut 089708264091</small>
            </div>
        </div>
    </div>
</div>
<!-- end modal tipe pembayaran -->