@extends('pelanggan.masterpelanggan')
@section('content')

<div class="container">
    <div class="portfolio-single-wrap unslate_co--section" id="portfolio-single-section">
        <div class="portfolio-single-inner"   style="margin-top: 100px;">
            <div class="row justify-content-between align-items-stretch">
                <div class="col-lg-8">
            <form action="" class="" method="POST">
            @csrf      
                    <div class="detail-v1">
                        <h3 class="mb-5">Masukkan Data Berikut</h3>
                        <span class="detail-label">Tanggal Pelaksanaan</span>
                        <div class="input-group date">
                            <input type="text"  name="tgl_pesan" id="tgl_pesan" class="form-control datepicker" required>
                            <i class="icon-date_range input-prefix mt-3" ></i>
                        </div><br>
                        <div class="form-group">
                            <span class="detail-label">Keterangan tambahan</span>
                            <textarea name="ket_pesan" id="ket_pesan" cols="10" rows="5" class="form-control" required></textarea>
                        </div>
                        <div class="">
                            <p class="gsap-reveal " style="float:right;">*Harga belum termasuk Uang Transport Luar Kota Kediri</p>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Pesan" class="btn btn-primary btn-md">
                        </div>
                        
                    </div>
                </div>
        @foreach($layanans as $l)
        <input type="hidden" name="id_lay" value="{{$l->id}}">
        <input type="hidden" name="id_pesan" value="{{$psn}}">
        <input type="hidden" name="status_pesan" value="0">
        </form>
            <div class="col-lg-4 pl-lg-5">
                <div class="unslate_co--sticky">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Foto Layanan</span>
                            <p style="width: 30%;"><a href="{{asset('data_file/'.$l->foto_lay)}}" data-fancybox="gal">
                                <img src="{{asset('data_file/'.$l->foto_lay)}}" alt="Image" class="img-fluid"></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Nama Layanan</span>
                            <span class="detail-val">{{$l->nama_lay}}</span>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Harga Layanan</span>
                            <span class="detail-val">Rp.{{$l->harga_lay}}.000</span>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Kategori Layanan</span>
                            <span class="detail-val">{{$l->jenis_lay}}</span>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                            <span class="detail-label">Keterangan Layanan</span>
                            <span class="detail-val">{{$l->ket_lay}}</span>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            @endforeach
            
        </div>
    </div>
</div>

@endsection