<!doctype html>
<html lang="en">
  <head>
    <title>BeeCreative &mdash; We're Ready to Creative Your Moment</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="A onepage portfolio HTML template by Unslate.co">
    <meta name="keywords" content="html, css, javascript, jquery">
    <meta name="author" content="Unslate.co">

    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/aos.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/jquery.fancybox.min.css')}}">
    

    <!-- Theme Style -->
    <link rel="stylesheet" href="{{asset('pengunjung/css/style.css')}}">

  </head>
  <body data-spy="scroll" data-target=".site-nav-target" data-offset="200">

    <nav class="unslate_co--site-mobile-menu">
      <div class="close-wrap d-flex">
        <a href="#" class="d-flex ml-auto js-menu-toggle">
          <span class="close-label">Close</span>
          <div class="close-times">
            <span class="bar1"></span>
            <span class="bar2"></span>
          </div>
        </a>
      </div>
      <div class="site-mobile-inner"></div>
    </nav>


    <div class="unslate_co--site-wrap">

      <div class="unslate_co--site-inner">

        <div class="lines-wrap">
          <div class="lines-inner">
            <div class="lines"></div>
          </div>
        </div>
        <!-- END lines -->
      
      <nav class="unslate_co--site-nav dark site-nav-target">

        <div class="container">
        
          <div class="row align-items-center justify-content-between text-left">

            <div class="col-3">
              <div class="site-logo">
                <a href="/welcomepel" class="unslate_co--site-logo">Bee<span>Creative.</span></a>
              </div>
            </div>
            <div class="col text-right">
              <ul class="site-nav-ul js-clone-nav text-left d-none d-lg-inline-block">
                <li><a href="#home-section" class="nav-link">Home</a></li>
                <li><a href="#services-section" class="nav-link">Layanan</a></li>
                <li><a href="#portfolio-section" class="nav-link">Portofolio</a></li>
                <!-- <li><a href="#skills-section" class="nav-link">Order</a></li> -->
                <li><a href="#testimonial-section" class="nav-link">Testimonial</a></li>
                <li><a href="#about-section" class="nav-link">About</a></li>
                <li><a href="#contact-section" class="nav-link">Contact</a></li>
                <li class="has-children">
                  <a href="#login-section" class="nav-link active">
                    <div class="icon-user-circle" style="color: black;"> 
                      @if (Auth::guest())
                      @else 
                        <!-- {{ Auth::user()->name }} -->
                        {{ substr(Auth::user()->name,0,5) }}
                      @endif
                    </div>
                  </a>
                  <ol class="dropdown">
                    <li>
                      <a href="/order">Pemesanan</a>
                    </li>
                    <li>
                      <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    </li>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </ol>
                </li>                       
              </ul>

              <ul class="site-nav-ul-none-onepage text-right d-inline-block d-lg-none">
                <li><a href="#" class="js-menu-toggle">Menu</a></li>
              </ul>
            </div>
            
          </div>
        </div>

      </nav>
      <!-- END nav -->

      <div class="cover-v1 jarallax overlay" style="background-image: url('{{asset('pengunjung/images/bg1.jpg')}}');" id="home-section">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-7 mx-auto text-center">
              <h1 class="heading gsap-reveal-hero text-white">Bee Creative</h1>
              <h2 class="subheading gsap-reveal-hero text-white"> We're Ready to Creative Your Moment</h2>
              <h5 class="text-white gsap-reveal-hero"> Photo | Video | Yearbook | Events</h5>
            </div>
          </div>
        </div>

        <!-- dov -->
        <a href="#services-section" class="mouse-wrap smoothscroll">
          <span class="mouse">
            <span class="scroll"></span>
          </span>
          <span class="mouse-label">Scroll Down</span>
        </a>

      </div>
      <!-- END .cover-v1 -->


      <!-- start layanan -->      
      <div class="unslate_co--section" id="services-section">
        <div class="container">
          <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">Daftar Layanan</span></h2>
            <span class="gsap-reveal"><img src="{{asset('pengunjung/images/divider.png')}}" alt="divider" width="76"></span>
          </div>
          <div class="row gutter-v3">
            <div class="col-md-6 col-lg-4 mb-4">
              <div class="feature-v1" data-aos="fade-up" data-aos-delay="0">
                <div class="wrap-icon mb-3">
                  <img src="{{asset('pengunjung/images/svg/001-options.svg')}}" alt="Image" width="45">
                </div>
                <h3>Video <br> graphy</h3>
                <p> all about Videography services you need in here<a href="/layvideopel">... see more</a></p> 
              </div> 
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
              <div class="feature-v1" data-aos="fade-up" data-aos-delay="100">
                <div class="wrap-icon mb-3">
                  <img src="{{asset('pengunjung/images/svg/002-chat.svg')}}" alt="Icon" width="45">
                </div>
                <h3>Photo <br> graphy</h3>
                <p>all about Photography services you need in here<a href="/layfotopel">... see more</a></p>
              </div> 
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
              <div class="feature-v1" data-aos="fade-up" data-aos-delay="200">
                <div class="wrap-icon mb-3">
                  <img src="{{asset('pengunjung/images/svg/003-contact-book.svg')}}" alt="Image" class="img-fluid" width="45">
                </div>
                <h3>Year <br> book</h3>
                <p>all about yaerbook services you need in here<a href="/laybookpel">... see more</a></p>
              </div> 
            </div>
            <!-- <div class="col-md-6 col-lg-4 mb-4">
              <div class="feature-v1" data-aos="fade-up" data-aos-delay="200">
                <div class="wrap-icon mb-3">
                  <img src="{{asset('pengunjung/images/svg/004-percentage.svg')}}" alt="Image" class="img-fluid" width="45">
                </div>
                <h3>Events <br> and Bundling</h3>
                <p>all about Events and Bundling package services you need in here<a href="/layevent">... see more</a></p>
              </div> 
            </div> -->
          </div>
            <div class="unslate_co--section text-center ">
              <h3 class="mr-auto heading-h3 gsap-reveal">Rekomendasi untuk Anda</h3>
              <div class="row">
              
              <?php $no=1; ?>
              @foreach($foryou as $l)
                <div class="col-sm-6 col-md-6 col-lg-4 blog-post-entry  gsap-reveal-img" style="margin-top:30px; margin-bottom: 170px; height:250px;">
                  <a href="{{asset('data_file/'.$l->foto_lay)}}" class="grid-item blog-item w-100 h-100" data-fancybox="gal"
                  data-caption=" {{$l->nama_lay}} - {{$l->harga_lay}}K // {{$l->ket_lay}}" >
                    <div class="overlay">
                    <span class="wrap-icon badge badge-primary float-right">Top order <?php  echo $no;$no++;?></span>
                      <!-- <span class="wrap-icon icon-eye"></span> -->
                        <div class="portfolio-item-content">
                            <h3></h3>
                            <p class="post-meta">
                            </p>
                        </div>
                    </div>
                    <img src="{{asset('data_file/'.$l->foto_lay)}}" class="lazyload img-fluid" alt="Image" />
                  </a>
                  <div class="card" >
                    <div class="card-body">
                      <h6 class="card-title " >{{$l->nama_lay}} &bullet; {{$l->harga_lay}}K</h6>
                      <!-- <small class="card-text mb">{{ Str::limit($l->ket_lay, 37) }}</small><br> -->
                      @if($l->jenis_lay == "Photography")
                        <a href="{{url('/laypesanfoto/'.$l->id_lay)}}" class="btn btn-primary btn-small "> Pesan Layanan <span class="wrap-icon icon-add_shopping_cart"></span> </a>
                      @elseif($l->jenis_lay == "Videography")
                        <a href="{{url('/laypesanvid/'.$l->id_lay)}}" class="btn btn-primary btn-small "> Pesan Layanan <span class="wrap-icon icon-add_shopping_cart"></span> </a>
                        @else
                        <a href="{{url('/laypesanbook/'.$l->id_lay)}}" class="btn btn-primary btn-small "> Pesan Layanan <span class="wrap-icon icon-add_shopping_cart"></span> </a>
                      @endif
                    </div>
                  </div>
                </div>
              @endforeach
              </div>
            </div>
        </div>
      </div>
      <!-- end layanan -->

      <!-- start portofolio -->
      <div class="unslate_co--section" id="portfolio-section">
        <div class="container">
          <div class="section-heading-wrap text-center mb-4">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">Portofolio</span></h2>
            <span class="gsap-reveal"><img src="{{asset('pengunjung/images/divider.png')}}" alt="divider" width="76"></span>
          </div>
          <div class="relative"><div class="loader-portfolio-wrap"><div class="loader-portfolio dark"></div></div> </div>
          <div id="portfolio-single-holder"></div>
          <div class="portfolio-wrapper">

            <div class="d-flex align-items-center mb-4 gsap-reveal gsap-reveal-filter">
              <!-- <h2 class="heading-h2">Portofolio</h2> -->
              
              <a href="#" class="text-black js-filter d-inline-block d-lg-none">Filter</a>
              
              <div class="filter-wrap">
                <div class="filter ml-auto" id="filters">  
                  <a href="#" class="active" data-filter="*">All</a>
                  <a href="#" data-filter=".video">Videography</a>
                  <a href="#" data-filter=".photo">Photography</a>
                  @foreach($kategori_port as $f)
                  <a href="#" data-filter=".{{$f->id}}">{{$f->nama}}</a>
                  @endforeach
                </div>
              </div>
            </div>

            <div id="posts" class="row gutter-isotope-item">
            <form action="" class="" method="POST">
            @foreach($port_foto as $f)
            @csrf
              <div class="item photo {{$f->kategori_foto}} col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
              <input type="hidden" name="id_foto" id="id_foto" value="{{$f->id}}">
                <a href="/welcomepel/detilfotpel/{{$f->id}}" class="portfolio-item isotope-item gsap-reveal-img" data-id="1" type="submit" value="Kirim">
                    <div class="overlay">
                      <span class="wrap-icon icon-link2"></span>
                      <div class="portfolio-item-content">
                        <h3> {{$f->nama_foto}} </h3>
                        <p>{{$f->nama}}</p>
                      </div>
                    </div>
                    <img src="{{asset('portofolio_file/'.$f->foto)}}" class="lazyload  img-fluid" alt="Images" />
                  </a>
              </div>
              
            @endforeach

            @foreach($port_vidio as $v)
            <div class="item video {{$v->kategori_vid}} col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
            <input type="hidden" name="id_vidio" value="{{$v->id}}">
                <a href="/welcomepel/detilvidpel/{{$v->id}}" class="portfolio-item isotope-item gsap-reveal-img" type="submit" value="Kirim">
                    <div class="overlay">
                      <span class="wrap-icon icon-link2"></span>
                      <div class="portfolio-item-content">
                        <h3>{{$v->nama_vid}}</h3>
                        <p>{{$v->nama}}</p>
                      </div>
                    </div>
                    <div class="text-center" style="float: left;">
                      {!! Embed::make($v->vidio)->parseUrl()->setAttribute(['width' => 350])->getIframe() !!}
                    </div>
                  </a>
              </div>
            @endforeach
            </form>
            </div>

            <div class="row mt-5 justify-content-md-center">
              <h2 class="mr-auto heading-h2  col-lg-12  text-center mb-4">Portofolio Terbaru</h2>
              <div class="row col-sm-12 col-md-12 col-lg-12" >
              <?php $mostfot=1; ?>
            @foreach($foto_baru as $f)
            @csrf
              <div class=" blog-post-entry  col-sm-4 col-md-4 col-lg-4 isotope-mb-1 gsap-reveal-img" >
                <input type="hidden" name="id_foto" id="id_foto" value="{{$f->id}}">
                  <a href="/welcomepel/detilfotpel/{{$f->id}}" class="portfolio-item isotope-item " data-id="1" type="submit" value="Kirim">
                    <div class="overlay">
                    <span class="wrap-icon badge badge-primary float-right"><?php  echo $mostfot;$mostfot++;?></span>
                      <div class="portfolio-item-content">
                      </div>
                    </div>
                    <img src="{{asset('portofolio_file/'.$f->foto)}}" class="lazyload  img-fluid" alt="Images" style="width: 600px; height: 120px; object-fit:cover;" />
                  </a>
                  <div class="card" >
                    <div class="card-body text-center">
                        <div class="badge badge-primary text-center" style="float: center;">Upload On  {{substr( $f->created_at,0, 10)}}</div>
                    </div>
                  </div>
              </div>
            @endforeach
              </div>
              <div class="row col-sm-12 col-md-12 col-lg-12" >
              <?php $mostvid=1; ?>
            @foreach($vidio_baru as $v)
            @csrf
              <div class=" blog-post-entry col-sm-4 col-md-4 col-lg-4 isotope-mb-1 text-right  gsap-reveal-img" style="float: right;">
                <input type="hidden" name="id_vidio" id="id_vidio" value="{{$v->id}}">
                  <a href="/welcomepel/detilvidpel/{{$v->id}}" class="portfolio-item isotope-item" data-id="1" type="submit" value="Kirim">
                    <div class="overlay">
                    <span class="wrap-icon badge badge-primary float-right"><?php  echo $mostvid;$mostvid++;?></span>
                      <div class="portfolio-item-content">
                      </div>
                    </div>
                    <div class="text-center" style="float: left;">
                      {!! Embed::make($v->vidio)->parseUrl()->setAttribute(['width' => 350])->getIframe() !!}
                    </div>
                  </a>
                  <div class="card" >
                    <div class="card-body text-center">
                        <div class="badge badge-primary text-center" style="float: center;">Upload On {{substr( $v->created_at,0, 10)}}</div>
                    </div>
                  </div>
              </div><br>
            @endforeach
              </div>
            </div>

            <div class="row mt-5 justify-content-md-center">
            <div class="row col-lg-6 justify-content-md-center">
              <h2 class="mr-auto heading-h2  col-lg-12  text-center mb-4">Most Seen Portofolio</h2>
              <div class="row col-sm-6 col-md-6 col-lg-6" >
              <?php $mostfot=1; ?>
            @foreach($rekom_port as $f)
            @csrf
              <div class=" blog-post-entry  col-sm-12 col-md-12 col-lg-12 isotope-mb-1 gsap-reveal-img" >
                <input type="hidden" name="id_foto" id="id_foto" value="{{$f->id}}">
                  <a href="/welcomepel/detilfotpel/{{$f->id_foto}}" class="portfolio-item isotope-item " data-id="1" type="submit" value="Kirim">
                    <div class="overlay">
                    <span class="wrap-icon badge badge-primary float-right"><?php  echo $mostfot;$mostfot++;?></span>
                      <div class="portfolio-item-content">
                      </div>
                    </div>
                    <img src="{{asset('portofolio_file/'.$f->foto)}}" class="lazyload  img-fluid" alt="Images" style="width: 600px; height: 120px; object-fit:cover;" />
                  </a>
                  <div class="card" >
                    <div class="card-body text-center">
                        <div class="badge badge-primary text-center" style="float: center;">Total dikunjungi {{$f->jml}}</div>
                    </div>
                  </div>
              </div>
            @endforeach
              </div>
              <div class="row col-sm-6 col-md-6 col-lg-6" >
              <?php $mostvid=1; ?>
            @foreach($rekom_vid as $v)
            @csrf
              <div class=" blog-post-entry col-sm-12 col-md-12 col-lg-12 isotope-mb-1 text-right  gsap-reveal-img" style="float: right;">
                <input type="hidden" name="id_vidio" id="id_vidio" value="{{$v->id}}">
                  <a href="/welcomepel/detilvidpel/{{$v->id_vidio}}" class="portfolio-item isotope-item" data-id="1" type="submit" value="Kirim">
                    <div class="overlay">
                    <span class="wrap-icon badge badge-primary float-right"><?php  echo $mostvid;$mostvid++;?></span>
                      <div class="portfolio-item-content">
                      </div>
                    </div>
                    <div class="text-center" style="float: left;">
                      {!! Embed::make($v->vidio)->parseUrl()->setAttribute(['width' => 210])->getIframe() !!}
                    </div>
                  </a>
                  <div class="card" >
                    <div class="card-body text-center">
                        <div class="badge badge-primary text-center" style="float: center;">Total dikunjungi {{$v->jml}}</div>
                    </div>
                  </div>
              </div><br>
            @endforeach
              </div>
            </div>

            <div class="row col-lg-6 justify-content-md-center">
              <h2 class="mr-auto heading-h2 col-lg-12 text-center mb-4">Top Rate Portofolio</h2>
              <div class="row col-sm-6 col-md-6 col-lg-6" >
              <?php $ratefot=1; ?>
            @foreach($rekom_ratfot as $f)
            @csrf
              <div class=" blog-post-entry  col-sm-12 col-md-12 col-lg-12 isotope-mb-1 gsap-reveal-img" >
                <input type="hidden" name="id_foto" id="id_foto" value="{{$f->id}}">
                  <a href="/welcomepel/detilfotpel/{{$f->id_foto}}" class="portfolio-item isotope-item " data-id="1" type="submit" value="Kirim">
                    <div class="overlay">
                    <span class="wrap-icon badge badge-primary float-right"><?php  echo $ratefot;$ratefot++;?></span>
                      <div class="portfolio-item-content">
                      </div>
                    </div>
                    <img src="{{asset('portofolio_file/'.$f->foto)}}" class="lazyload  img-fluid" alt="Images" style="width: 600px; height: 119px; object-fit:cover;" />
                  </a>
                  <div class="card" >
                        <div class="row card-body" style="margin: auto;">
                          <div class="col-lg-7">
                            <div class=" text-center rateyo2" data-rateyo-rating="{{round($f->average,1)}}"></div>
                          </div>
                          <div class="col-lg-5 mt-2 font-weight-bold">({{round($f->average,1)}})</div>
                        </div>
                  </div>
              </div>
            @endforeach
              </div>
              <div class="row col-sm-6 col-md-6 col-lg-6" >
              <?php $ratevid=1; ?>
            @foreach($rekom_ratvid as $v)
            @csrf
              <div class=" blog-post-entry col-sm-12 col-md-12 col-lg-12 isotope-mb-1 text-right  gsap-reveal-img" style="float: right;">
                <input type="hidden" name="id_vidio" id="id_vidio" value="{{$v->id}}">
                  <a href="/welcomepel/detilvidpel/{{$v->id_vidio}}" class="portfolio-item isotope-item" data-id="1" type="submit" value="Kirim">
                    <div class="overlay">
                    <span class="wrap-icon badge badge-primary float-right"><?php  echo $ratevid;$ratevid++;?></span>
                      <div class="portfolio-item-content">
                      </div>
                    </div>
                    <div class="text-center" style="float: left;">
                      {!! Embed::make($v->vidio)->parseUrl()->setAttribute(['width' => 210])->getIframe() !!}
                    </div>
                  </a>
                  <div class="card" >
                        <div class="row card-body" style="margin: auto;">
                          <div class="col-lg-7">
                            <div class=" text-center rateyo2" data-rateyo-rating="{{round($v->average,1)}}"></div>
                          </div>
                          <div class="col-lg-5 mt-2 font-weight-bold">({{round($v->average,1)}})</div>
                        </div>
                  </div>
              </div><br>
            @endforeach
              </div>
            </div>

            </div>

            </div>
          </div>
        </div>

     
      
    <!-- end portofolio -->

<!-- start percentage -->
    <!-- <div class="unslate_co--section section-counter" id="skills-section">
        <div class="container">
          <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">How To Order</span></h2>
            <span class="gsap-reveal"><img src="{{asset('pengunjung/images/divider.png')}}" alt="divider" width="76"></span>
          </div>

          
        </div>
    </div> -->
<!-- END .percentage -->

<!-- start testimonial -->
    <div class="unslate_co--section" id="testimonial-section">
        <div class="container">
          <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">Our Happy Clients</span></h2>
            <span class="gsap-reveal"><img src="{{asset('pengunjung/images/divider.png')}}" alt="divider" width="76"></span>
          </div>
        </div>

        <div class="owl-carousel testimonial-slider">
          @foreach($review as $r)
          <div>
            <div class="testimonial-v1">
              <div class="testimonial-inner-bg">
                <span class="quote">&ldquo;</span>
                <blockquote>
                  {{$r->isi_rev}}
                </blockquote>
              </div>
              
              <div class="testimonial-author-info"> 
                <a href="{{asset('review/'.$r->foto_rev)}}"   width="50%" height="50%" data-fancybox="gal" 
                data-caption=" {{$r->name}}  ||  {{$r->jenis_lay}}  ||  {{$r->nama_lay}}  ||  {{$r->isi_rev}}">
                  <img src="{{asset('review/'.$r->foto_rev)}}" width="50%" height="50%" alt="Image" class="img-fluid">
                </a>
                <h3>{{$r->nama_pemesan}}</h3>
                <span class="d-block position">{{$r->jenis_lay}}  ||  {{$r->nama_lay}}</span>
              </div>

            </div>
          </div>
          @endforeach
        </div>

    </div>
<!-- END testimonial -->

<!-- start about -->
      <div class="unslate_co--section" id="about-section">
        <div class="container">
          <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">About Us</span></h2>
            <span class="gsap-reveal"><img src="{{asset('pengunjung/images/divider.png')}}" alt="divider" width="76"></span>
          </div>
          <div class="row mt-5 justify-content-between">
            <div class="col-lg-7 mb-5 mb-lg-0" data-aos="fade-up">
              <figure class="dotted-bg gsap-reveal-img">
                <img src="{{asset('pengunjung/images/aboutt.png')}}" alt="Image" class="img-fluid">
              </figure>
            </div>
            <div class="col-lg-4 pr-lg-5">
              <h3 class="mb-4 heading-h3"><span class="gsap-reveal">We can make it together</span></h3>
              <p class="lead gsap-reveal">Bee Creative adalah perusahaan yang bergerak pada bidang multimedia. Meliputi Jasa Desain Grafis, Layouter, Dokumentasi, Branding / Brand Identity, Item Promotion dan Publishing.</p>
              <p class="mb-4 gsap-reveal">Dengan konsep yang pasti up to date dan menarik demi meningkatkan brand awareness di mata konsumen, juga harga yang menarik dan kualitas terbaik.</p>
              <!-- <p class="gsap-reveal"><a href="#" class="btn btn-outline-pill btn-custom-light">Download my CV</a></p> -->
            </div>
          </div>
          <div class="row pt-5">
            <div class="col-6 col-sm-6 mb-5 mb-lg-0 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="300">
              <div class="counter-v1 text-center">
                <span class="number-wrap">
                  <span class="number number-counter" data-number="100">{{$jmlreview}}</span>
                  <span class="append-text"></span>
                </span>
                <span class="counter-label">Testimonials</span>
              </div>
            </div>
            <div class="col-6 col-sm-6 mb-5 mb-lg-0 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="0">
              <div class="counter-v1 text-center">
                <span class="number-wrap">
                  <span class="number number-counter" data-number="100">{{$jmlmember}}</span>
                  <span class="append-text"></span>
                </span>
                <span class="counter-label">Members</span>
              </div>
            </div>
            <div class="col-6 col-sm-6 mb-5 mb-lg-0 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="100">
              <div class="counter-v1 text-center">
                <span class="number-wrap">
                  <span class="number number-counter" data-number="5">{{$jmlstaff}}</span>
                  <span class="append-text"></span>
                </span>
                <span class="counter-label">Staff</span>
              </div>
            </div>
            <div class="col-6 col-sm-6 mb-5 mb-lg-0 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="200">
              <div class="counter-v1 text-center">
                <span class="number-wrap">
                  <span class="number number-counter" data-number="3">3</span>
                  <span class="append-text"></span>
                </span>
                <span class="counter-label">Freelancer</span>
              </div>
            </div>
          </div>
        </div>
      </div>
<!-- end about -->


<!-- start contact -->
      <div class="unslate_co--section" id="contact-section">
        <div class="container">
          <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">Get In Touch</span></h2>
            <span class="gsap-reveal"><img src="{{asset('pengunjung/images/divider.png')}}" alt="divider" width="76"></span>
          </div>


          <div class="row justify-content-between">
            
            <div class="col-md-6">
              <form method="post" class="form-outline-style-v1" id="contactForm">
                <div class="form-group row mb-0">

                  <div class="col-lg-6 form-group gsap-reveal">
                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" id="name">
                  </div>
                  <div class="col-lg-6 form-group gsap-reveal">
                    <label for="email">Email</label>
                    <input name="email" type="email" class="form-control" id="email">
                  </div>
                  <div class="col-lg-12 form-group gsap-reveal">
                    <label for="message">Write your message...</label>
                    <textarea name="message" id="message" cols="30" rows="7" class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group row gsap-reveal">
                    <div class="col-md-12 d-flex align-items-center">
                      <input type="submit" class="btn btn-outline-pill btn-custom-light mr-3" value="Send Message">
                      <span class="submitting"></span>
                    </div>
                  </div>
              </form>
              <div id="form-message-warning" class="mt-4"></div> 
              <div id="form-message-success">
                Your message was sent, thank you!
              </div>

            </div>

            <div class="col-md-4">
              <div class="contact-info-v1">
                <div class="gsap-reveal d-block">
                  <span class="d-block contact-info-label">Email</span>
                  <a href="mailto:cvbeecreative@gmail.com" >cvbeecreative@gmail.com</a>
                </div>
                <div class="gsap-reveal d-block">
                  <span class="d-block contact-info-label">Instagram</span>
                  <a href="https://www.instagram.com/beecreative_id/" >@beecreative_id</a>
                </div>
                <div class="gsap-reveal d-block">
                  <span class="d-block contact-info-label">Address</span>
                  <a href="https://goo.gl/maps/QDNiJBzh7pPmb5ie7" >East Java, Indonesia <br> Perum Candra Kirana Blok Q15 KEDIRI</a>
                  <!-- <address class="contact-info-val">📍 East Java, Indonesia <br> Perum Candra Kirana Blok Q15 KEDIRI</address> -->
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
<!-- end contact -->
    
    </div>
 
<!-- footer -->
      <footer class="unslate_co--footer unslate_co--section">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-7">
                
                <div class="footer-site-logo">
                  <!-- <a href="#">Bee<span>Creative.</span></a>   -->
                  <img src="{{asset('pengunjung/images/logo.png')}}" width="80px" alt="">              
                  <h6>📍 East Java, Indonesia <br> Perum Candra Kirana Blok Q15 KEDIRI</h6>
                </div>
              <ul class="footer-site-social">
                <li><a href="mailto:cvbeecreative@gmail.com">Email</a></li>
                <li><a href="https://www.instagram.com/beecreative_id/">Instagram</a></li>
              </ul>
               
              <p class="site-copyright">
                <small>&copy; 2021 <a href="#">Bee Creative</a>. All Rights Reserved. Design with <span class="icon-heart text-danger"></span> by <a href="#">Unslate.co</a>.</small>
              </p>

            </div>
          </div>
        </div>
      </footer>
<!-- end footer -->
      
    </div>

    <!-- Loader -->
    <div id="unslate_co--overlayer"></div>
    <div class="site-loader-wrap">
      <div class="site-loader dark"></div>
    </div>

    <script src="{{asset('pengunjung/js/scripts-dist.js')}}"></script>
    <script src="{{asset('pengunjung/js/main.js')}}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157808202-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-157808202-1');
    </script>
 
  <script type="text/javascript" src="{{asset('assets/vendor/rate/main.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/rate/jquery.rateyo.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/rate/jquery.rateyo.min.js')}}"></script>
        <script>
            $(function () {
            
            $(".rateyo2").rateYo({
            starWidth: "18px",
            readOnly: true
            });
        });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

<link rel="stylesheet" href="{{asset('assets/vendor/rate/jquery.rateyo.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/rate/jquery.rateyo.min.css')}}">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<!-- Latest compiled and minified JavaScript -->


  </body>
</html>