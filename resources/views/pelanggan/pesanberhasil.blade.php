@extends('pelanggan.masterpelanggan')
@section('content')

<div class="cover-v1 jarallax overlay" style="background-image: url('{{asset('pengunjung/images/bg1.jpg')}}');"  id="home-section">
    <div class="container">
        <div class="row align-items-center">

        <div class="col-md-10 mx-auto text-center">
            <h1 class="heading" data-aos="fade-up">Pemesanan Berhasil</h1>
            <h4 class="subheading" data-aos="fade-up" data-aos-delay="100">silahkan tunggu konfirmasi dari admin untuk upload bukti pembayaran.  
            </h4>
            <span>
                <a href="/order"> cek status pemesanan</a>
                <span>  ||  </span>
                <a href="/welcomepel">kembali ke beranda</a>
            </span>
        </div>

        </div>
    </div>
</div>

@endsection