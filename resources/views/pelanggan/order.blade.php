@extends('pelanggan.masterpelanggan')
@section('content')



<div class="container">
    <div class="portfolio-single-wrap unslate_co--section" id="login-section order-section">
        <div class="portfolio-single-inner"   style="margin-top: 70px;">
            <div class=" jumbotron ">
                <div class="text-center">
                    <h1 class="mb-4 font-weight-bold"> Daftar Pesanan Anda</h1>
                </div>
                <div  class="table-responsive ">
                    <table id="datatable" class="table table-bordered table-hover first " style="background-color: white;">
                        <thead>
                            <tr>
                                <th hidden></th>
                                <th style="width: 5%;">No.</th>
                                <th style="width: 10%;">ID Pesan</th>
                                <th>Kategori</th>
                                <th>Nama Layanan</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; ?>
                                @foreach($pemesanan as $p)
                            <tr>
                                <td hidden>{{$p->id}}</td>
                                <th><?php  echo $no;$no++;?>.</th>
                                <th>{{$p->id_pesan}}</th>
                                <td>{{$p->jenis}}</td>
                                <td>{{$p->nama_layanan}}</td>
                                <td>
                                    @if($p->status_pesan == 0)
                                        <div class="alert alert-warning" role="alert">
                                            <span class="font-weight-bold">Belum Terverifikasi</span>
                                        </div>
                                    @elseif($p->status_pesan == 1 && $p->jenis != "Yearbook")
                                        <div class="alert alert-success" >
                                            <span class="text-right"><i class="icon-check"></i> </span>
                                            <span class="font-weight-bold">Tanggal Terverifikasi</span>
                                        </div>
                                    @elseif($p->status_pesan == 1 && $p->jenis == "Yearbook")
                                        <div class="alert alert-success" >
                                            <span class="text-right"><i class="icon-check"></i> </span>
                                            <span class="font-weight-bold">Tipe Bayar Terverifikasi</span>
                                        </div>
                                    @elseif($p->status_pesan == 2)
                                        <div class="alert alert-warning" role="alert">
                                            <span class="font-weight-bold">Menunggu Verifikasi Tanggal</span>
                                        </div>
                                    @elseif($p->status_pesan == 3 && $p->jenis != "Yearbook")
                                        <div class="alert alert-danger" >
                                            <span class="text-right"><i class="icon-close"></i> </span>
                                            <span class="font-weight-bold">Tanggal Gagal Terverifikasi</span>
                                        </div>
                                    @elseif($p->status_pesan == 3 && $p->jenis == "Yearbook")
                                        <div class="alert alert-danger" >
                                            <span class="text-right"><i class="icon-close"></i> </span>
                                            <span class="font-weight-bold">Tipe Bayar Gagal Terverifikasi</span>
                                        </div>
                                    @elseif($p->status_pesan == 4)
                                        <div class="alert alert-success" >
                                            <span class="text-right"><i class="icon-check"></i> </span>
                                            <span class="font-weight-bold">Selesai Verifikasi</span>
                                        </div>
                                    @elseif($p->status_pesan == 5)
                                        <div class="alert alert-warning" role="alert">
                                            <span class="font-weight-bold">Menunggu Verifikasi Bukti Pembayaran</span>
                                        </div>
                                    @elseif($p->status_pesan == 6)
                                        <div class="alert alert-danger" >
                                            <span class="text-right"><i class="icon-close"></i> </span>
                                            <span class="font-weight-bold">Pembayaran Gagal Terverifikasi</span>
                                        </div>
                                    @elseif($p->status_pesan == 10)
                                        <div class="alert alert-info" >
                                            <span class="text-right"><i class="icon-check"></i> </span>
                                            <span class="font-weight-bold">Pemesanan Selesai</span>
                                        </div>
                                    @else
                                        <div class="alert alert-dark" >
                                            <span class="text-right"><i class="icon-close"></i> </span>
                                            <span class="font-weight-bold">PESANAN GAGAL</span>
                                        </div>
                                    @endif
                                </td>
                                <td  class="text-center">
                                    <div class="btn-group mr-2">
                                        @if($p->jenis == "Yearbook")
                                        <a href="/detilorderyb/{{$p->id}}">
                                            <button class="btn btn-xs btn-custom-light btn-outline-pill">Detail</button>
                                        @else
                                        <a href="/detilorder/{{$p->id}}">
                                            <button class="btn btn-xs btn-custom-light btn-outline-pill">Detail</button>
                                        </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>            



@endsection



