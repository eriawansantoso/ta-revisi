@extends('pelanggan.masterpelanggan')
@section('content')

@foreach($pemesanan as $p)
<div class="container">
    <div class="portfolio-single-wrap unslate_co--section" id="login-section">
        <div class="portfolio-single-inner" style="margin-top: 100px;">
                
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
            <link rel="stylesheet" href="{{asset('pengunjung/invoice/invoice-css.css')}}">

                <div class="container">
                <div class="text-left"> <a href="/order"> <i class="fa fa-arrow-left"></i> kembali ke menu pemesanan</a></div><br>
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                        @foreach($errors->all() as $error)
                        <li class="font-weight-bold">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(\Session::has('suksesrev'))
                <div class="alert alert-success" role="alert">
                    <strong class="font-weight-bold">{{\Session::get('suksesrev')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif
                @if(\Session::has('gagalrev'))
                <div class="alert alert-danger" role="alert">
                    <strong class="font-weight-bold">{{\Session::get('gagalrev')}}</strong>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </a>
                </div>
                @endif
                <div class="border col-md-12">
                    <div class="invoice">
                        <!-- begin invoice-company -->
                        <div class="invoice-company text-inverse f-w-600">
                            <span class="pull-right hidden-print text-center">
                                @if($p->status_pesan == 0)
                                    <div class="alert alert-warning" role="alert">
                                        <span class="font-weight-bold">Belum Terverifikasi</span>
                                    </div>
                                @elseif($p->status_pesan == 1)
                                    <div class="alert alert-success" >
                                        <span class="text-right"><i class="icon-check"></i> </span>
                                        <span class="font-weight-bold">Tipe Pembayaran Terverifikasi</span>
                                    </div>
                                @elseif($p->status_pesan == 2)
                                    <div class="alert alert-warning" role="alert">
                                        <span class="font-weight-bold">Menunggu Verifikasi Tipe Pembayaran</span>
                                    </div>
                                @elseif($p->status_pesan == 3)
                                    <div class="alert alert-danger" >
                                        <span class="text-right"><i class="icon-close"></i> </span>
                                        <span class="font-weight-bold">Tipe Pembayaran Gagal Terverifikasi</span>
                                    </div>
                                @elseif($p->status_pesan == 4)
                                    <div class="alert alert-success" >
                                        <span class="text-right"><i class="icon-check"></i> </span>
                                        <span class="font-weight-bold">Selesai Verifikasi</span>
                                    </div>
                                @elseif($p->status_pesan == 5)
                                    <div class="alert alert-warning" role="alert">
                                        <span class="font-weight-bold">Menunggu Verifikasi Bukti Pembayaran</span>
                                    </div>
                                @elseif($p->status_pesan == 6)
                                    <div class="alert alert-danger" >
                                        <span class="text-right"><i class="icon-close"></i> </span>
                                        <span class="font-weight-bold">Bukti Pembayaran Gagal Terverifikasi</span>
                                    </div>
                                @elseif($p->status_pesan == 10)
                                    <div class="alert alert-info" >
                                        <span class="text-right"><i class="icon-check"></i> </span>
                                        <span class="font-weight-bold">Pemesanan Selesai</span>
                                    </div>
                                @else
                                    <div class="alert alert-dark" >
                                        <span class="text-right"><i class="icon-close"></i> </span>
                                        <span class="font-weight-bold">PESANAN GAGAL</span>
                                    </div>
                                @endif
                            </span>
                            <span><img src="{{asset('pengunjung/images/logo.png')}}" width="60px" alt=""> </span>
                            <!-- <h1>BeeCreative.</h1> -->
                        </div>
                        <!-- end invoice-company -->
                        <!-- begin invoice-header -->
                        <div class="invoice-header">
                            <div class="invoice-from">
                            <small>from</small>
                            <address class="m-t-5 m-b-5">
                                <strong class="text-inverse">CV. Bee Creative</strong><br>
                                Perum Candra Kirana Blok Q15 KEDIRI<br>
                                Kediri, Jawa Timur, Indonesia<br>
                                Phone: (123) 456-7890<br>
                            </address>
                            </div>
                            <div class="invoice-to">
                            <small>to</small>
                            <address class="m-t-5 m-b-5">
                                <strong class="text-inverse">{{$p->nama_pemesan}}</strong><br>
                                {{$p->alamat}}<br>
                                Phone: {{$p->hp}}<br>
                            </address>
                            </div>
                            <div class="invoice-date">
                            <small>Tanggal Pesan</small>
                            <div class="date text-inverse m-t-5">{{ substr($p->tgl_nota,0,10) }}</div>
                            <div class="invoice-detail">
                                ID# {{$p->id_pesan}}<br>
                                {{$p->jenis_lay }}
                            </div>
                            <small class="font-italic" style="color: #999;">terakhir diperbaharui {{ substr($p->riwayat,0,10) }}</small>
                            </div>
                        </div>
                        <!-- end invoice-header -->
                        <!-- begin invoice-content -->
                        <div class="invoice-content">
                            <!-- begin table-responsive -->
                            <div class="table-responsive">
                            <table class="table table-invoice">
                                <thead>
                                    <tr>
                                        <th>DESKRIPSI PEMESANAN</th>
                                        <th class="text-center  " width="35%">STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <span> <strong> {{$p->nama_lay}}</strong></span><br>
                                            <small >{{$p->ket_lay}}</small><br><br>
                                            <small><strong>Sekolah:</strong> {{$p->ket_pesan}}</small><br><br>
                                            @if($p->tipe_bayar == 1)
                                                <small><strong>Tipe pembayaran:</strong> A</small><br>
                                                <small>Pembayaran DP 50% , Pelunasan 50% Setelah Pengiriman.</small><br><br>
                                            @elseif($p->tipe_bayar == 2)
                                                <small><strong>Tipe pembayaran:</strong> B</small><br>
                                                <small>Pembayaran DP 10% , Setelah Sesi Foto 30%, Setelah Sesi Layout 30%, Pelunasan 30% Setelah Pengiriman.</small><br><br>
                                            @else
                                                <small><strong>Tipe pembayaran:</strong> C</small><br>
                                                <small>Pembayaran DP 10%, Pelunasan 90% Setelah Pengiriman.</small>
                                            @endif
                                            <!-- <small><strong>Jumlah Eksemplar:</strong> {{$p->jmlh_yb}} buku</small> <br> -->
                                        </td>
                                        <td class="text-center" >
                                            @if($p->status_pesan == 0)
                                                <br><small style="font-size: 10px;">silahkan tunggu verifikasi dari admin</small>
                                            @elseif($p->status_pesan == 1)
                                                <a href="javascript:;" class="btn btn-sm btn-primary edibukti mb-2" data-toggle="modal" data-target="#edibukti" data-id_pemesanan="{{$p->id_pemesanan}}" data-bukti_pesan="{{$p->bukti_pesan}}"><i class="fa fa-file t-plus-1  fa-fw fa-lg"></i> Upload Bukti Pembayaran</a>
                                                <br><small style="font-size: 10px;">silahkan upload bukti pembayaran</small>
                                            @elseif($p->status_pesan == 2)
                                                <br><small style="font-size: 10px;">silahkan tunggu verifikasi dari admin</small>
                                            @elseif($p->status_pesan == 3)
                                                <a href="javascript:;" class="btn btn-sm btn-primary editipe mb-2" data-toggle="modal" data-target="#editipe" data-id_pemesanan="{{$p->id_pemesanan}}" data-tipe_bayar="{{$p->tipe_bayar}}"><i class="fa fa-calendar t-plus-1  fa-fw fa-lg"></i> Ganti Tipe Pembayaran</a>
                                                <br><small style="font-size: 10px;">silahkan ganti Tipe Pembayaran</small>
                                                <br><small style="font-size: 10px;">panduan tipe pembayaran <a href="javascript;:" data-toggle="modal" data-target="#tipebayar" >disini</a> </small>
                                            @elseif($p->status_pesan == 4)
                                                <a href="javascript:;"  onclick="printDiv('nota')" class="btn btn-sm btn-primary mb-2"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Cetak Nota</a>
                                                <br><small style="font-size: 10px;"> pemesanan selesai silahkan cetak nota pemesanan</small>
                                            @elseif($p->status_pesan == 5)
                                                <br><small style="font-size: 10px; ">silahkan tunggu verifikasi dari admin</small>
                                            @elseif($p->status_pesan == 6)
                                                <a href="javascript:;" class="btn btn-sm btn-primary edibukti mb-2" data-toggle="modal" data-target="#edibukti" data-id_pemesanan="{{$p->id_pemesanan}}" data-bukti_pesan="{{$p->bukti_pesan}}"><i class="fa fa-file t-plus-1  fa-fw fa-lg"></i> Upload Bukti Pembayaran</a>
                                                <br><small style="font-size: 10px;">silahkan upload ulang bukti pembayaran</small>
                                            @elseif($p->status_pesan == 10)
                                                <a href="javascript:;"  onclick="printDiv('nota')" class="btn btn-sm btn-primary mb-2"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Cetak Nota</a>
                                                <br><small style="font-size: 10px;"> pemesanan selesai silahkan cetak nota pemesanan</small>
                                            @else
                                                <br><small style="font-size: 10px;">silahkan ulangi pemesanan anda</small>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <div class="card border-light" style="background-color: #fff7e3;">
                                <div class="card-body">
                                    <h6 class="font-weight-bold">Rincial Pembayaran</h6>
                                    <table class="">
                                        <thead>
                                            <tr>
                                                <td style="width: 150px;"></td>
                                                <td style="width: 10px;"></td>
                                                <td ></td>
                                            </tr>
                                        </thead>
                                        @if($p->tipe_bayar == 1)
                                        <tbody>
                                            <tr>
                                                <td><small>Jumlah Eksemplar Buku</small></td>
                                                <td>:</td>
                                                <td>{{$p->jmlh_yb}}</td>
                                            </tr>
                                            <tr>
                                                <td><small>Harga Satuan Buku</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 - Rp.30,000 <small> (pemesanan diatas 450 eksemplar)</small></td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 + Rp.30,000 <small> (pemesanan dibawah 150 eksemplar)</small></td>
                                                @else
                                                    <td>Rp.{{number_format($p->harga_lay)}},000</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td></td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay-30)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay+30)}},000</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td><small>Total Keseluruhan</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000</td>
                                                @else
                                                    <td>Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000</td>
                                                @endif
                                            </tr>
                                            <tr><td class="font-weight-bold">. <br>Tahap Pembayaran</td><td></td><td></td></tr>
                                            @if($p->jmlh_yb >= 451)
                                                <tr>
                                                    <td><small>DP 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @elseif($p->jmlh_yb <= 150)
                                                <tr>
                                                    <td><small>DP 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><small>DP 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        @elseif($p->tipe_bayar == 2)
                                        <tbody>
                                            <tr>
                                                <td><small>Jumlah Eksemplar Buku</small></td>
                                                <td>:</td>
                                                <td>{{$p->jmlh_yb}}</td>
                                            </tr>
                                            <tr>
                                                <td><small>Harga Satuan Buku</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 - Rp.30,000 <small> (pemesanan diatas 450 eksemplar)</small></td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 + Rp.30,000 <small> (pemesanan dibawah 150 eksemplar)</small></td>
                                                @else
                                                    <td>Rp.{{number_format($p->harga_lay)}},000</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td></td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay-30)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay+30)}},000</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td><small>Total Keseluruhan</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000</td>
                                                @else
                                                    <td>Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000</td>
                                                @endif
                                            </tr>
                                            <tr><td class="font-weight-bold">. <br>Tahap Pembayaran</td><td></td><td></td></tr>
                                            @if($p->jmlh_yb >= 451)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Foto 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Layout 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @elseif($p->jmlh_yb <= 150)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Foto 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Layout 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Foto 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Layout 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        @else
                                        <tbody>
                                            <tr>
                                                <td><small>Jumlah Eksemplar Buku</small></td>
                                                <td>:</td>
                                                <td>{{$p->jmlh_yb}}</td>
                                            </tr>
                                            <tr>
                                                <td><small>Harga Satuan Buku</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 - Rp.30,000 <small> (pemesanan diatas 450 eksemplar)</small></td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 + Rp.30,000 <small> (pemesanan dibawah 150 eksemplar)</small></td>
                                                @else
                                                    <td>Rp.{{number_format($p->harga_lay)}},000</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td></td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay-30)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay+30)}},000</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td><small>Total Keseluruhan</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000</td>
                                                @else
                                                    <td>Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000</td>
                                                @endif
                                            </tr>
                                            <tr><td class="font-weight-bold">. <br>Tahap Pembayaran</td><td></td><td></td></tr>
                                            @if($p->jmlh_yb >= 451)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 90%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.9*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @elseif($p->jmlh_yb <= 150)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 90%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.9*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 90%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.9*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <!-- end table-responsive -->
                            <br>
                            <!-- begin invoice-price -->
                            <div class="invoice-price">
                            <div class="invoice-price-left">
                                <div class="invoice-price-row">
                                    <div class="sub-price">
                                        @if($p->status_pesan == 4)
                                        <span style="color: green;">review pesanan anda, <a href="#" data-toggle="modal" data-target="#addreview" data-id_pemesanan="{{$p->id_pemesanan}}" data-id_pel="{{$p->id_pel}}">DI SINI</a></span>
                                        @elseif($p->status_pesan == 10)
                                        <span style="color: grey;">terima kasih telah mereview pesanan anda</span>
                                        @else
                                            <small class="font-weight-bold">*Pemesanan kurang dari 150 Eksemplar (+30k/Eksemplar)</small>
                                            <small class="font-weight-bold">*Pemesanan lebih dari 450 Eksemplar (-30k/Eksemplar)</small>
                                        @endif
                                        <!-- <span class="text-inverse">WA:087654456444</span> -->
                                    </div>
                                    <!-- <div class="sub-price">
                                        <i class="fa fa-plus text-muted"></i>
                                    </div>
                                    <div class="sub-price">
                                        <small>PAYPAL FEE (5.4%)</small>
                                        <span class="text-inverse">$108.00</span>
                                    </div> -->
                                </div>
                            </div>
                            <div class="invoice-price-right">
                                @if($p->jmlh_yb >= 451)
                                <small>TOTAL</small> <span class="f-w-600">Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000.-</span>
                                @elseif($p->jmlh_yb <= 150)
                                <small>TOTAL</small> <span class="f-w-600">Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000.-</span>
                                @else
                                <small>TOTAL</small> <span class="f-w-600">Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000.-</span>
                                @endif
                            </div>
                            </div>
                            <!-- end invoice-price -->
                        </div>
                        <!-- end invoice-content -->
                        <!-- begin invoice-note -->
                        <div class="invoice-note">
                        <h4><strong>NOTE :</strong></h4>
                            * Rekening Pembayaran : <strong>BCA 9876543334</strong> a.n Bee Creative<br>
                            * Cetak nota pemesanan ini jika status sudah <strong>Selesai Verifikasi</strong><br>
                            * Simpan sebagai bukti pemesanan dan jangan sampai hilang<br>
                            * Jika ada yang ingin ditanyakan, silahkan hubungi admin WA [085708264081]
                        </div>
                        <!-- end invoice-note -->
                        <!-- begin invoice-footer -->
                        <div class="invoice-footer">
                            <p class="text-center m-b-5 f-w-600">
                            THANK YOU FOR YOUR ORDER
                            <p class="text-center">
                            <span class="m-r-10"><i class="fa fa-fw fa-lg fa-instagram"></i> @beecreative_id</span>
                            <span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone"></i> 085-708264091</span> <span></span>
                            <span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> cvbeecreative@gmail.com</span>
                            </p>
                        </div>
                        <!-- end invoice-footer -->
                    </div>
                </div>


<!-- nota kudu di hidden -->
                <br>
                <div class="border col-md-12" id="nota" hidden> 
                    <div class="invoice">
                        <!-- begin invoice-company -->
                        <div class="invoice-company text-inverse f-w-600">
                            <span class="pull-right hidden-print text-center">
                                    <div class="alert alert-info" >
                                        <span class="font-weight-bold">CV. BEE CREATIVE</span>
                                    </div>
                            </span>
                            <span><img src="{{asset('pengunjung/images/logo.png')}}" width="60px" alt=""> </span>
                            <!-- <h1>BeeCreative.</h1> -->
                        </div>
                        <!-- end invoice-company -->
                        <!-- begin invoice-header -->
                        <div class="invoice-header">
                            <div class="invoice-from">
                            <small>from</small>
                            <address class="m-t-5 m-b-5">
                                <strong class="text-inverse">CV. Bee Creative</strong><br>
                                Perum Candra Kirana Blok Q15 KEDIRI<br>
                                Kediri, Jawa Timur, Indonesia<br>
                                Phone: (123) 456-7890<br>
                            </address>
                            </div>
                            <div class="invoice-to">
                            <small>to</small>
                            <address class="m-t-5 m-b-5">
                                <strong class="text-inverse">{{$p->nama_pemesan}}</strong><br>
                                {{$p->alamat}}<br>
                                Phone: {{$p->hp}}<br>
                            </address>
                            </div>
                            <div class="invoice-date">
                            <small>Tanggal Pesan</small>
                            <div class="date text-inverse m-t-5">{{ substr($p->tgl_nota,0,10) }}</div>
                            <div class="invoice-detail">
                                ID# {{$p->id_pesan}}<br>
                                {{$p->jenis_lay }}
                            </div>
                            </div>
                        </div>
                        <!-- end invoice-header -->
                        <!-- begin invoice-content -->
                        <div class="invoice-content">
                            <!-- begin table-responsive -->
                            <div class="table-responsive">
                            <table class="table table-invoice">
                                <thead>
                                    <tr>
                                        <th>DESKRIPSI PEMESANAN</th>
                                        <th class="text-center  " width="35%">STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <span> <strong> {{$p->nama_lay}}</strong></span><br>
                                            <small >{{$p->ket_lay}}</small><br><br>
                                            <small><strong>Sekolah:</strong> {{$p->ket_pesan}}</small><br><br>
                                            @if($p->tipe_bayar == 1)
                                                <small><strong>Tipe pembayaran:</strong> A</small><br>
                                                <small>Pembayaran DP 50% , Pelunasan 50% Setelah Pengiriman.</small><br><br>
                                            @elseif($p->tipe_bayar == 2)
                                                <small><strong>Tipe pembayaran:</strong> B</small><br>
                                                <small>Pembayaran DP 10% , Setelah Sesi Foto 30%, Setelah Sesi Layout 30%, Pelunasan 30% Setelah Pengiriman.</small><br><br>
                                            @else
                                                <small><strong>Tipe pembayaran:</strong> C</small><br>
                                                <small>Pembayaran DP 10%, Pelunasan 90% Setelah Pengiriman.</small>
                                            @endif
                                        </td>
                                        <td class="text-center" >
                                            <br><small class="font-weight-bold" style="font-size: 20px;">PEMESANAN SELESAI</small><br>
                                            <small  style="font-size: 10px;"> terima kasih telah memesan layanan kami</small>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <div class="card border-light" style="background-color: #fff7e3;">
                                <div class="card-body">
                                    <h6 class="font-weight-bold">Rincial Pembayaran</h6>
                                    <table class="">
                                        <thead>
                                            <tr>
                                                <td style="width: 150px;"></td>
                                                <td style="width: 10px;"></td>
                                                <td ></td>
                                            </tr>
                                        </thead>
                                        @if($p->tipe_bayar == 1)
                                        <tbody>
                                            <tr>
                                                <td><small>Jumlah Eksemplar Buku</small></td>
                                                <td>:</td>
                                                <td>{{$p->jmlh_yb}}</td>
                                            </tr>
                                            <tr>
                                                <td><small>Harga Satuan Buku</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 - Rp.30,000 <small> (pemesanan diatas 450 eksemplar)</small></td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 + Rp.30,000 <small> (pemesanan dibawah 150 eksemplar)</small></td>
                                                @else
                                                    <td>Rp.{{number_format($p->harga_lay)}},000</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td></td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay-30)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay+30)}},000</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td><small>Total Keseluruhan</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000</td>
                                                @else
                                                    <td>Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000</td>
                                                @endif
                                            </tr>
                                            <tr><td class="font-weight-bold">. <br>Tahap Pembayaran</td><td></td><td></td></tr>
                                            @if($p->jmlh_yb >= 451)
                                                <tr>
                                                    <td><small>DP 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @elseif($p->jmlh_yb <= 150)
                                                <tr>
                                                    <td><small>DP 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><small>DP 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 50%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.5*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        @elseif($p->tipe_bayar == 2)
                                        <tbody>
                                            <tr>
                                                <td><small>Jumlah Eksemplar Buku</small></td>
                                                <td>:</td>
                                                <td>{{$p->jmlh_yb}}</td>
                                            </tr>
                                            <tr>
                                                <td><small>Harga Satuan Buku</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 - Rp.30,000 <small> (pemesanan diatas 450 eksemplar)</small></td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 + Rp.30,000 <small> (pemesanan dibawah 150 eksemplar)</small></td>
                                                @else
                                                    <td>Rp.{{number_format($p->harga_lay)}},000</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td></td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay-30)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay+30)}},000</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td><small>Total Keseluruhan</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000</td>
                                                @else
                                                    <td>Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000</td>
                                                @endif
                                            </tr>
                                            <tr><td class="font-weight-bold">. <br>Tahap Pembayaran</td><td></td><td></td></tr>
                                            @if($p->jmlh_yb >= 451)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Foto 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Layout 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @elseif($p->jmlh_yb <= 150)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Foto 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Layout 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Foto 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah sesi Layout 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 30%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.3*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        @else
                                        <tbody>
                                            <tr>
                                                <td><small>Jumlah Eksemplar Buku</small></td>
                                                <td>:</td>
                                                <td>{{$p->jmlh_yb}}</td>
                                            </tr>
                                            <tr>
                                                <td><small>Harga Satuan Buku</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 - Rp.30,000 <small> (pemesanan diatas 450 eksemplar)</small></td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format($p->harga_lay)}},000 + Rp.30,000 <small> (pemesanan dibawah 150 eksemplar)</small></td>
                                                @else
                                                    <td>Rp.{{number_format($p->harga_lay)}},000</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td></td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay-30)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>:</td>
                                                    <td>Rp.{{number_format($p->harga_lay+30)}},000</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td><small>Total Keseluruhan</small></td>
                                                <td>:</td>
                                                @if($p->jmlh_yb >= 451)
                                                    <td>Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000</td>
                                                @elseif($p->jmlh_yb <= 150)
                                                    <td>Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000</td>
                                                @else
                                                    <td>Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000</td>
                                                @endif
                                            </tr>
                                            <tr><td class="font-weight-bold">. <br>Tahap Pembayaran</td><td></td><td></td></tr>
                                            @if($p->jmlh_yb >= 451)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 90%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.9*($p->harga_lay-30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @elseif($p->jmlh_yb <= 150)
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 90%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.9*($p->harga_lay+30)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><small>DP 10%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.1*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                                <tr>
                                                    <td><small>Setelah Pengiriman 90%</small></td>
                                                    <td>:</td>
                                                    <td>Rp.{{number_format((0.9*($p->harga_lay)*$p->jmlh_yb))}},000</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <!-- end table-responsive -->
                            <br>
                            <!-- begin invoice-price -->
                            <div class="invoice-price" style="background: #f0f3f4; display: table; width: 100%">
                            <div class="invoice-price-left" style="">
                                <div class="invoice-price-row">
                                    <div class="sub-price">
                                        <span style="color: orange;">nota ini adalah bukti yang valid <br> telah melakukan pemesanan layanan kami
                                        <!-- <span class="text-inverse">WA:087654456444</span> -->
                                    </div>
                                    <!-- <div class="sub-price">
                                        <i class="fa fa-plus text-muted"></i>
                                    </div>
                                    <div class="sub-price">
                                        <small>PAYPAL FEE (5.4%)</small>
                                        <span class="text-inverse">$108.00</span>
                                    </div> -->
                                </div>
                            </div>
                            <div class="invoice-price-right" style="width: 50%; background: #2d353c; color: #fff; font-size: 28px; text-align: right; vertical-align: bottom; font-weight: 300">
                                @if($p->jmlh_yb >= 451)
                                <small style="color: grey;">TOTAL</small> <span class="font-weight-bold" style="color: red;">Rp.{{number_format(($p->harga_lay-30)*$p->jmlh_yb)}},000.-</span>
                                @elseif($p->jmlh_yb <= 150)
                                <small style="color: grey;">TOTAL</small> <span class="font-weight-bold" style="color: red;">Rp.{{number_format(($p->harga_lay+30)*$p->jmlh_yb)}},000.-</span>
                                @else
                                <small style="color: grey;">TOTAL</small> <span class="font-weight-bold" style="color: red;">Rp.{{number_format(($p->harga_lay)*$p->jmlh_yb)}},000.-</span>
                                @endif
                            </div>
                            </div>
                            <!-- end invoice-price -->
                        </div>
                        <!-- end invoice-content -->
                        <!-- begin invoice-note --> 
                        <div class="invoice-note" style="color: #999; margin-top: 80px; font-size: 85%">
                        <h4><strong>NOTE :</strong></h4>
                            * Rekening Pembayaran : <strong>BCA 9876543334</strong> a.n Bee Creative<br>
                            * Cetak nota pemesanan ini jika status sudah <strong>Selesai Verifikasi</strong><br>
                            * Simpan sebagai bukti pemesanan dan jangan sampai hilang<br>
                            * Jika ada yang ingin ditanyakan, silahkan hubungi admin WA [085708264081]
                        </div>
                        <!-- end invoice-note -->
                        <!-- begin invoice-footer -->
                        <div class="invoice-footer" style="border-top: 1px solid #ddd; padding-top: 10px; font-size: 10px">
                            <p class="text-center m-b-5 f-w-600">
                            THANK YOU FOR YOUR ORDER
                            <p class="text-center">
                            <span class="m-r-10"><i class="fa fa-fw fa-lg fa-instagram"></i> @beecreative_id</span>
                            <span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone"></i> 085-708264091</span> <span></span>
                            <span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> cvbeecreative@gmail.com</span>
                            </p>
                        </div>
                        <!-- end invoice-footer -->
                    </div>
                </div>

<!-- end nota kudu di hidden -->

                </div>

        </div>
    </div>
</div>            
@endforeach
@endsection

<!-- start modal review pesanan pelanggan -->
<div class="modal fade" id="addreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; "modal-focus="true"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Review Pemesanan Layanan</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
        <form action="{{route('addreview')}}" name="form1" id="form1" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-body">
                @foreach($pemesanan as $p)
                <input name="id_pesan" id="id_pesan" type="text" class="form-control " value="{{$p->id_pemesanan}}" hidden>
                <input name="id_pel" id="id_pel" type="text" class="form-control" value="{{$p->id_pel}}" hidden>
                @endforeach
                <div class="form-group detail-v1">
                    <label for="isi_rev" class="detail-label">isi review anda disini</label>
                    <textarea name="isi_rev" id="isi_rev" cols="10" rows="3" class="form-control  {{ $errors->has('isi_rev') ? ' is-invalid' : '' }}" value="{{old('isi_rev')}}" placeholder="masukkan review anda disini"required> </textarea>
                </div>
                <div class="form-group detail-v1">
                    <label for="foto_rev" class="detail-label">Foto</label>
                    <div class="form-group">
                        <input name="foto_rev" id="foto_rev" type="file" class="form-control  {{ $errors->has('foto_rev') ? ' is-invalid' : '' }}" required>
                    <label for="foto_rev" class="detail-label">Format file: .jpeg,.png,.jpg (MAX : 2 Mb)</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit"  class="btn btn-success">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal review pesanan pelanggan -->

<!-- start modal ubah tgl -->
<div class="modal fade" id="editipe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Tipe Pembayaran</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{route('updatipe')}}" name="form2" id="form2" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <input name="id" id="id" type="text" class="form-control "  hidden>
                    <div class="form-group detail-v1">
                        <label class="detail-label">Tipe Pembayaran</label>
                        <select  class="form-control "  name="tipe_bayar" id="tipe_bayar" required>
                            <option value="" selected>-- pilih tipe pembayaran --</option>
                            <option value="1">Tipe A</option>
                            <option value="2">Tipe B</option>
                            <option value="3">Tipe C</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit"  class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal ubah tipe -->



<!-- start modal ubah bukti -->
<div class="modal fade" id="edibukti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{route('updabukti')}}" name="form4" id="form4" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    @foreach($pemesanan as $p)
                    <input name="id" id="id" type="text" class="form-control" value="{{$p->id_pemesanan}}" hidden>
                    @endforeach
                    <div class="form-group detail-v1">
                        <label for="tgl_pesan" class="detail-label">Bukti Pembayaran </label>
                        <div class="form-group">
                            <input name="bukti_pesan" id="bukti_pesan" type="file" class="form-control edit_bukti_pesan {{ $errors->has('bukti_pesan') ? ' is-invalid' : '' }}" required>
                        <label for="tgl_pesan" class="detail-label">Format file: .jpeg,.png,.jpg (MAX : 2 Mb)</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit"  class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal ubah bukti -->

<!-- start modal tipe pembayaran -->
<div class="modal fade" id="tipebayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tipe Pembayaran Yearbook</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <div class="modal-body">
            <div class="detail-v1  mb-4">
                <span class="detail-label">Tipe A</span>
                <span class="detail-val">Pembayaran DP 50% , Pelunasan 50% Setelah Pengiriman.</span>
            </div>
            <div class="detail-v1  mb-4">
                <span class="detail-label">Tipe B</span>
                <span class="detail-val">Pembayaran DP 10% , Setelah Sesi Foto 30%, Setelah Sesi Layout 30%, Pelunasan 30% Setelah Pengiriman.</span>
            </div>
            <div class="detail-v1  mb-4">
                <span class="detail-label">Tipe C</span>
                <span class="detail-val">Pembayaran DP 10%, Pelunasan 90% Setelah Pengiriman.</span>
            </div>
            </div>
            <div class="modal-footer" >
                <small class="text-center" style="color: red; margin-right: 7%;" > hubungi admin via Whatsapp untuk info lebih lanjut 089708264091</small>
            </div>
        </div>
    </div>
</div>
<!-- end modal tipe pembayaran -->