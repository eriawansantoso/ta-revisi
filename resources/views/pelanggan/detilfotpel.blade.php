@extends('pelanggan.masterpelanggan')
@section('content')
      <div class="cover-v1 jarallax overlay"  id="home-section">
        <div class="container">
          <div class="row align-items-center">
          @foreach($port_foto as $f)

            <div class="col-md-10 mx-auto text-center">
              <h1 class="heading" data-aos="fade-up">Portofolio Photography</h1>
              <h2 class="subheading" data-aos="fade-up" data-aos-delay="100">{{$f->nama_foto}}</h2>
              <span class="badge badge-dark subheading text-light icon-eye"data-aos="fade-up" data-aos-delay="200"> {{$jmllihat}}</span>
              </div>

          </div>
        </div>

        <!-- dov -->
        <a href="#portfolio-single-section" class="mouse-wrap dark smoothscroll">
          <span class="mouse">
            <span class="scroll"></span>
          </span>
          <span class="mouse-label">Scroll</span>
        </a>

      </div>
      <!-- END .cover-v1 -->

      <div class="container">
        <div class="portfolio-single-wrap unslate_co--section" id="portfolio-single-section">
            <div class="portfolio-single-inner">
            <h2 class="heading-portfolio-single-h2">Portofolio Photography</h2>
              <div class="row justify-content-between align-items-stretch">

              
                <div class="col-lg-8">
                  <p><a href="{{asset('portofolio_file/'.$f->foto)}}" data-fancybox="gal">
                  <img src="{{asset('portofolio_file/'.$f->foto)}}" alt="Image" class="img-fluid"></a></p>
                  <div class="comment-form-wrap pt-5">
                <h3 class="mb-5">Tinggalkan Komentar</h3>
                <div class="row">
                  <div class="col-lg-12" > 
                    @if(\Session::has('sukses'))
                      <div class="alert alert-success" role="alert">
                        <strong class="font-weight-bold">Komentar berhasil ditambahkan, </strong>
                        <small>Tunggu validasi dari admin untuk melihat komentar anda tampil</small>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                    </div>
                    @endif
                  </div>
                </div>
                <form action="{{ url('/komentar_foto') }}"  class="" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="id_foto" value="{{$f->id}}">
                <input type="hidden" name="status_kom" value="0">
                  <div class="form-group">
                    <label for="komentar">Komentar</label>
                    <textarea name="komentar" id="komentar" cols="30" rows="3" class="form-control" required></textarea>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-md">KIRIM</button>  
                  </div>
                </form>
              </div>
                </div>
                
                <div class="col-lg-4 pl-lg-5">
                  <div class="unslate_co--sticky">
                    <div class="row">
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Nama Portofolio</span> 
                          <p>{{$f->nama_foto}} </p>
                        </div>
                        
                      </div>

                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Tanggal Upload</span>
                          <span class="detail-val">{{substr( $f->created_at,0, 10)}}</span>
                        </div>
                      </div>
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Kategori</span>
                          <span class="detail-val"><a href="#">Fotografi</a>, <a>{{$f->nama}}</a></span>
                        </div>
                      </div>
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Keterangan Foto</span>
                          <span class="detail-val">{{$f->ket_foto}}</span>
                        </div>
                      </div>
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Rating ({{round($jmlrating,1)}}/5) </span>
                          <div class="rateyo2" data-rateyo-rating="{{round($jmlrating,1)}}"> 
                          </div>
                          <div class="row detail-label mt-3" >
                          @if(\Session::has('berhasil'))
                            <div class="alert alert-success" role="alert">
                              <strong class="font-weight-bold">Berhasil Tambah Rating</strong>
                              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                              </a>
                          </div>
                          @endif

                          @if($rat)
                          
                          @else
                            <small>masukkan penilaian portofolio disini</small>
                            <form action="{{ url('/rating_foto') }}" method="POST">
                            @csrf
                                <div class="rateyo1 col-lg-8" data-rateyo-rating="0" data-rateyo-num-stars="5" data-rateyo-score="3" required></div>
                                <div class="col-lg-4" >
                                  <span class='result detail-val '>0</span><span class="detail-val">/5</span>
                                </div>
                                <input type="hidden" name="id_foto" value="{{$f->id}}">
                                <input type="hidden" name="rating" required> 
                                <div class="form-group">
                                  <button type="submit"  class="btn btn-primary btn-md">KIRIM</button>  
                                </div>
                            </form>
                          @endif

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="comment-form-wrap pt-5">
                    <h3 class="mb-5">{{$jmlkomen}} Komentar</h3>
                    <ul class="comment-list">
                      @foreach($komentar_foto as $kom)
                      @if($kom->id_foto==$id && $kom->status_kom==1)
                      <li class="comment">
                        <div class="vcard bio">
                          <img src="{{asset('pengunjung/images/person_woman_1.jpg')}}" alt="Image placeholder">
                        </div>
                        <div class="comment-body">
                          <h3 class="font-weight-bold">{{$kom->name}}</h3>
                          <div class="meta">{{\Carbon\Carbon::parse($kom->created_at)->diffForHumans()}}</div>
                          <p>{{$kom->komentar}}</p>
                        </div>
                      </li>
                      @endif
                      @endforeach
                    </ul>
                  </div>
                
              </div>
              @endforeach
              
            </div>
            
        </div>
      </div>  
@endsection

@section('js')
<script type="text/javascript" src="{{asset('assets/vendor/rate/main.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/rate/jquery.rateyo.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/rate/jquery.rateyo.min.js')}}"></script>
    <script>
            $(function () {
            
            $(".rateyo1").rateYo({
            starWidth: "30px"
            });
        
            $(".rateyo1").rateYo().on("rateyo.change", function(e, data){
            var rating = data.rating;
            $(this).parent().find('.score').text('score :'+ $(this).attr('data-rateyo-score'));
            $(this).parent().find('.result').text(rating);
            $(this).parent().find('input[name=rating]').val(rating);
            });

        });
        </script>
        <script>
            $(function () {
            
            $(".rateyo2").rateYo({
            starWidth: "20px",
            readOnly: true
            });
        });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('assets/vendor/rate/jquery.rateyo.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/rate/jquery.rateyo.min.css')}}">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<!-- Latest compiled and minified JavaScript -->

@endsection