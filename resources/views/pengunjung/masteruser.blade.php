<!doctype html>
<html lang="en">
  <head>
    <title>BeeCreative &mdash; We're Ready to Creative Your Moment</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="A onepage portfolio HTML template by Unslate.co">
    <meta name="keywords" content="html, css, javascript, jquery">
    <meta name="author" content="Unslate.co">

    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/aos.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('pengunjung/css/vendor/jquery.fancybox.min.css')}}">
    @yield('css')

    <!-- Theme Style -->
    <link rel="stylesheet" href="{{asset('pengunjung/css/style.css')}}">

  </head>
  <body data-spy="scroll" data-target=".site-nav-target" data-offset="200">

    <nav class="unslate_co--site-mobile-menu">
      <div class="close-wrap d-flex">
        <a href="#" class="d-flex ml-auto js-menu-toggle">
          <span class="close-label">Close</span>
          <div class="close-times">
            <span class="bar1"></span>
            <span class="bar2"></span>
          </div>
        </a>
      </div>
      <div class="site-mobile-inner"></div>
    </nav>


    <div class="unslate_co--site-wrap">

      <div class="unslate_co--site-inner">

        <div class="lines-wrap">
          <div class="lines-inner">
            <div class="lines"></div>
          </div>
        </div>
        <!-- END lines -->
      
      <nav class="unslate_co--site-nav dark site-nav-target">

        <div class="container">
        
          <div class="row align-items-center justify-content-between text-left">

            <div class="col-3">
              <div class="site-logo">
                <a href="/" class="unslate_co--site-logo">Bee<span>Creative.</span></a>
              </div>
            </div>
            <div class="col text-right">
              <ul class="site-nav-ul js-clone-nav text-left d-none d-lg-inline-block">
                <li ><a href="/#home-section" class="nav-link">Home</a></li>
                <li ><a href="/#services-section" class="nav-link">Layanan</a></li>
                <li ><a href="/#portfolio-section" class="nav-link">Portofolio</a></li>
                <!-- <li><a href="/#skills-section" class="nav-link">Order</a></li> -->
                <li><a href="/#testimonial-section" class="nav-link">Testimonial</a></li>
                <li><a href="/#about-section" class="nav-link">About</a></li>
                <li><a href="/#contact-section" class="nav-link">Contact</a></li>
                <li>
                  <a href="/login" class="nav-link {{ 'loginuser' == request()->path() ? 'active' : ''}}">Login</a>
                </li>
              </ul>

              <ul class="site-nav-ul-none-onepage text-right d-inline-block d-lg-none">
                <li><a href="#" class="js-menu-toggle">Menu</a></li>
              </ul>
            </div>
            
          </div>
        </div>

      </nav>
      <!-- END nav -->

      @yield('content')
      </div>
<!-- footer -->
      <footer class="unslate_co--footer unslate_co--section">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="footer-site-logo">
                  <!-- <a href="#">Bee<span>Creative.</span></a>                 -->
                  <img src="{{asset('pengunjung/images/logo.png')}}" width="80px" alt="">   
                    <h6>📍 East Java, Indonesia <br> Perum Candra Kirana Blok Q15 KEDIRI</h6>
                </div>
              <ul class="footer-site-social">
                <li><a href="mailto:cvbeecreative@gmail.com">Email</a></li>
                <li><a href="https://www.instagram.com/beecreative_id/">Instagram</a></li>
              </ul>
               
              <p class="site-copyright">
                <small>&copy; 2021 <a href="#">Bee Creative</a>. All Rights Reserved. Design with <span class="icon-heart text-danger"></span> by <a href="#">Unslate.co</a>.</small>
              </p>

            </div>
          </div>
        </div>
      </footer>
<!-- end footer -->

      
    </div>

    <!-- Loader -->
    <div id="unslate_co--overlayer"></div>
    <div class="site-loader-wrap">
      <div class="site-loader dark"></div>
    </div>

    <script src="{{asset('pengunjung/js/scripts-dist.js')}}"></script>
    <script src="{{asset('pengunjung/js/main.js')}}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157808202-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-157808202-1');
    </script>

  <!-- edit bio -->
  <script>
      $(document).ready(function() {
    //edit data
        $('.edibio').on("click",function() {
          var id = $(this).attr('data-id');
          var id_pesan = $(this).attr('data-id_pesan');
          var kelas_bio = $(this).attr('data-kelas_bio');
          var nama_bio = $(this).attr('data-nama_bio');
          var jk_bio = $(this).attr('data-jk_bio');
          var tlhr_bio = $(this).attr('data-tlhr_bio');
          var tgllhr_bio = $(this).attr('data-tgllhr_bio');
          var alamat_bio  = $(this).attr('data-alamat_bio');
          var sosmed_bio  = $(this).attr('data-sosmed_bio');
          var bio_bio  = $(this).attr('data-bio_bio');

          $.ajax({
            url : "{{route('edibio')}}?id="+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
              console.log("hii");
              $('#id').val(data.id);
              $('#id_pesan').val(data.id_pesan);
              $('#kelas_bio').val(data.kelas_bio);
              $('#nama_bio').val(data.nama_bio);
              $('#jk_bio').val(data.jk_bio);
              $('#tlhr_bio').val(data.tlhr_bio);
              $('#tgllhr_bio').val(data.tgllhr_bio);
              $('#alamat_bio').val(data.alamat_bio);
              $('#sosmed_bio').val(data.sosmed_bio);
              $('#bio_bio').val(data.bio_bio);
              $('#edibio').modal('show');
            }
          });
        });
      });
  </script>   

    <script>
    $('.carousel').carousel({
        interval: false,
    });
    </script>
    @yield('js')

  </body>
</html>