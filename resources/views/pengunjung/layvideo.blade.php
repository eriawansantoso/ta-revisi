@extends('pengunjung.masteruser')
@section('content')
<div class="cover-v1 jarallax overlay" style="background-image: url('{{asset('pengunjung/images/bg1.jpg')}}');"  id="home-section">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-md-10 mx-auto text-center">
              <h1 class="heading" data-aos="fade-up">Layanan Videography</h1>
              <h4 class="" data-aos="fade-up" data-aos-delay="100">dengan sentuhan tangan dan ide-ide kreatif, jadikan momen yang berharga menjadi bermakna.</h4>
            </div>

          </div>
        </div>

        <!-- dov -->
        <a href="#portfolio-single-section" class="mouse-wrap dark smoothscroll">
          <span class="mouse">
            <span class="scroll"></span>
          </span>
          <span class="mouse-label">Scroll Down</span>
        </a>

      </div>
      <!-- END .cover-v1 -->

      <div class="container">
        <div class="portfolio-single-wrap unslate_co--section" id="portfolio-single-section">
            <div class="portfolio-single-inner">
              <h2 class="heading-portfolio-single-h4">Daftar Layanan Videography</h2>
              <div class="row gutter-v4 align-items-stretch mb-5">
                
              @foreach($layanan as $l)
              <div class="col-sm-6 col-md-6 col-lg-4 blog-post-entry" style="margin-top:30px; margin-bottom: 100px; height:250px;">
                <a href="{{asset('data_file/'.$l->foto_lay)}}" class="grid-item blog-item w-100 h-100" data-fancybox="gal"
                data-caption=" {{$l->nama_lay}} - {{$l->harga_lay}}K // {{$l->ket_lay}}" >
                  <div class="overlay">
                    <span class="wrap-icon icon-eye"></span>
                      <div class="portfolio-item-content">
                          <h3></h3>
                          <p class="post-meta">
                          </p>
                      </div>
                  </div>
                  <img src="{{asset('data_file/'.$l->foto_lay)}}" class="lazyload img-fluid" alt="Image" />
                </a>
                <div class="card" >
                  <div class="card-body">
                    <h6 class="card-title">{{$l->nama_lay}} &bullet; {{$l->harga_lay}}K</h6>
                    <p class="card-text">{{ Str::limit($l->ket_lay, 37) }}</p>
                    <!-- <a href="/login" class="btn btn-primary btn-small "> Pesan Layanan <span class="wrap-icon icon-add_shopping_cart"></span> </a> -->
                  </div>
                </div>
                
              </div>
              @endforeach

              </div>
            </div>
        </div>

     <!-- start testimonial -->
<div class="unslate_co--section" id="testimonial-section">
        <div class="container">
          <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">Our Happy Clients</span></h2>
            <span class="gsap-reveal"><img src="{{asset('pengunjung/images/divider.png')}}" alt="divider" width="76"></span>
          </div>
        </div>

        <div class="owl-carousel testimonial-slider">
          @foreach($reviewvid as $r)
          <div>
            <div class="testimonial-v1">
              <div class="testimonial-inner-bg">
                <span class="quote">&ldquo;</span>
                <blockquote>
                  {{$r->isi_rev}}
                </blockquote>
              </div>
              
              <div class="testimonial-author-info">
                <a href="{{asset('review/'.$r->foto_rev)}}"   width="50%" height="50%" data-fancybox="gal" 
                data-caption=" {{$r->name}}  ||  {{$r->jenis_lay}}  ||  {{$r->nama_lay}}  ||  {{$r->isi_rev}}">
                  <img src="{{asset('review/'.$r->foto_rev)}}" width="50%" height="50%" alt="Image" class="img-fluid">
                </a>
                <h3>{{$r->name}}</h3>
                <span class="d-block position">{{$r->jenis_lay}}  ||  {{$r->nama_lay}}</span>
              </div>

            </div>
          </div>
          @endforeach
        </div>

    </div>
<!-- END testimonial -->  
                
      </div>

@endsection