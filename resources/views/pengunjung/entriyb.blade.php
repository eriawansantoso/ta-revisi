@extends('pengunjung.masteruser')
@section('content')

<div class="container">
    <div class="portfolio-single-wrap unslate_co--section" id="login-section">
        <div class="portfolio-single-inner" style="margin-top: 100px;">
                
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

                <div class="container">
                    <div class="text-left gsap-reveal"> <a href="/laybook"> <i class="fa fa-arrow-left"></i> kembali ke layanan yearbook</a></div><br>
                        <div class="jumbotron col-md-12">
                            <h2 class="text-center font-weight-bold " style="margin-bottom: 50px;">Masukkan biodata siswa untuk yearbook</h2>
                            @if(\Session::has('berhasil'))
                                <div class="alert alert-success " role="alert">
                                <strong class="font-weight-bold">Data Berhasi Ditambah!! periksa kembali biodata anda pada tabel dibawah</strong>
                                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </a>
                            </div>
                            @endif
                            <div class="row">
                                <form action="{{ action('LaybookController@bio') }}" method="POST">
                                    @csrf
                                    <div class="form-group row mb-0 detail-v1">
                                        <input type="text" name="term" id="term" value="{{$term}}" hidden>
                                        <div class="col-lg-4 form-group gsap-reveal " style="margin-bottom: 50px;">
                                            <label for="kelas_bio" class="detail-label">Kelas</label>
                                            <input name="kelas_bio" type="text" class="form-control" id="kelas_bio" required>
                                            @if ($errors->has('kelas_bio'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('kelas_bio') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-4 form-group gsap-reveal " style="margin-bottom: 50px;">
                                            <label for="nama_bio" class="detail-label">Nama</label>
                                            <input name="nama_bio" type="text" class="form-control" id="nama_bio" required>
                                            @if ($errors->has('nama_bio'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('nama_bio') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-4 form-group gsap-reveal" style="margin-bottom: 50px;">
                                            <label for="jk_bio" class="detail-label">Jenis Kelamin</label>
                                            <select class="form-control" name="jk_bio" id="jk_bio" required>
                                                <option  selected="selected">-- Pilih Jenis Kelamin --</option>
                                                <option>Laki - Laki</option>
                                                <option>Perempuan</option>
                                            </select>
                                            @if ($errors->has('jk_bio'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('jk_bio') }}</strong>
                                                </span>
                                            @endif                                                  
                                        </div>
                                        <div class="col-lg-4 form-group gsap-reveal" style="margin-bottom: 50px;">
                                            <label for="tlhr_bio" class="detail-label">Tempat Lahir</label>
                                            <input name="tlhr_bio" type="text" class="form-control" id="tlhr_bio" required>
                                            @if ($errors->has('tlhr_bio'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('tlhr_bio') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-4 form-group gsap-reveal" style="margin-bottom: 50px;">
                                            <label for="tgllhr_bio" class="detail-label">Tanggal Lahir</label>
                                            <input name="tgllhr_bio" type="date" class="form-control" id="tgllhr_bio" required>
                                            @if ($errors->has('tgllhr_bio'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('tgllhr_bio') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-4 form-group gsap-reveal" style="margin-bottom: 50px;">
                                            <label for="sosmed_bio" class="detail-label">Sosial Media <small>*pilih salah satu (IG/FB/etc.)</small></label>
                                            <input name="sosmed_bio" type="text" class="form-control" id="sosmed_bio" required>
                                            @if ($errors->has('sosmed_bio'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('sosmed_bio') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 form-group gsap-reveal" >
                                            <label for="alamat_bio" class="detail-label">Alamat</label>
                                            <textarea name="alamat_bio" id="alamat_bio" cols="1" rows="3" class="form-control" required></textarea>
                                        </div>
                                        <div class="col-lg-6 form-group gsap-reveal" >
                                            <label for="bio_bio" class="detail-label">Bio</label>
                                            <textarea name="bio_bio" id="bio_bio" cols="1" rows="3" class="form-control" required></textarea>
                                        </div>
                                        <div class="col-lg-12">
                                            <p class="gsap-reveal">
                                            <button class="btn btn-primary" type="submit">Submit</button>
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                            
                    </div>
                </div> <br>
                
            <form  action="{{ action('LaybookController@submityb') }}" method="POST">
            @csrf
                <input type="text" name="term" id="term" value="{{$term}}" hidden>
                <div class="border col-md-12 detail-v1">
                    <h2 class="text-center font-weight-bold" style=" margin-top: 20px;">Check Biodata</h2>
                    <p class="font-weight-bold text-center detail-label gsap-reveal" style="margin-bottom: 20px; color: darkorange;">Periksa apakah biodata anda sudah benar. Setelah klik tombol kirim dibawah, anda sudah tidak dapat mengedit biodata ini lagi</p>
                    @if(\Session::has('full'))
                        <div class="alert alert-danger" role="alert">
                        <strong class="font-weight-bold">{{\Session::get('full')}}</strong>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                    </div>
                    @elseif(\Session::has('sukses'))
                        <div class="alert alert-warning" role="alert">
                        <strong class="font-weight-bold">{{\Session::get('sukses')}}</strong>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                    </div>
                    @elseif(\Session::has('yey'))
                        <div class="alert alert-success" role="alert">
                        <strong class="font-weight-bold">{{\Session::get('yey')}}</strong>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                    </div>
                    @elseif(\Session::has('wek'))
                        <div class="alert alert-danger" role="alert">
                        <strong class="font-weight-bold">{{\Session::get('wek')}}</strong>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                    </div>
                    @elseif(\Session::has('suksesbio'))
                        <div class="alert alert-success" role="alert">
                        <strong class="font-weight-bold">{{\Session::get('suksesbio')}}</strong>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                    </div>
                    @elseif(\Session::has('gagalbio'))
                        <div class="alert alert-danger" role="alert">
                        <strong class="font-weight-bold">{{\Session::get('gagalbio')}}</strong>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </a>
                    </div>
                    @endif
                   
                    <div  class="table-responsive gsap-reveal">
                    <table id="datatable" class="table table-bordered table-hover first " style="background-color: white;">
                        <thead>
                            <tr>
                                <th hidden></th>
                                <th hidden></th>
                                <th style="width: 5%;">No.</th>
                                <th style="width: 8%;">Kelas</th>
                                <th style="width: 15%;">Nama</th>
                                <th>JK</th>
                                <th style="width: 20%;" class="text-center">TTL</th>
                                <th style="width: 15%;" class="text-center">Alamat</th>
                                <th style="width: 10%;" class="text-center">Sosmed</th>
                                <th style="width: 10%;" class="text-center">Bio</th>
                                <th  class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; ?>
                                @foreach($bio as $p)
                            <tr>
                                <td hidden>{{$p->id}}</td>
                                <td hidden>{{$p->id_pesan}}</td>
                                <th><?php  echo $no;$no++;?>.</th>
                                <td>{{$p->kelas_bio}}</td>
                                <td>{{$p->nama_bio}}</td>
                                <td>
                                    @if($p->jk_bio == "Laki - Laki")
                                        L
                                    @elseif($p->jk_bio == "Perempuan")
                                        P
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    {{$p->tlhr_bio}}, {{$p->tgllhr_bio}}
                                </td>
                                <td>{{$p->alamat_bio}}</td>
                                <td>{{$p->sosmed_bio}}</td>
                                <td>{{$p->bio_bio}}</td>
                                <td  class="text-center">
                                    <div class="btn-group mr-2">
                                        <a href="javascript:;" class="edibio" data-toggle="modal" data-target="#edibio" data-id="{{$p->id}}" data-id_pesan="{{$p->id_pesan}}" data-kelas_bio="{{$p->kelas_bio}}" data-nama_bio="{{$p->nama_bio}}" data-jk_bio="{{$p->jk_bio}}" data-tlhr_bio="{{$p->tlhr_bio}}" data-tgllhr_bio="{{$p->tgllhr_bio}}" data-alamat_bio="{{$p->alamat_bio}}" data-sosmed_bio="{{$p->sosmed_bio}}" data-bio_bio="{{$p->bio_bio}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    <div class="btn-group mr-2">
                                        <a href="/desbio:{{$p->id}}"  onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA INI ... ?')">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <p class="gsap-reveal">
                        <button class="btn btn-primary" type="submit" onclick="return confirm('pastikan data anda benar!! Anda tidak dapat mengubah data ini lagi setelah kklik OK')">Kirim</button>
                        </p>
                    </div>
                    <!-- <div class="col-lg-9 detail-v1 text-right" style="padding: right;">
                        @foreach($totalyb as $t)
                        <span class="detail-label" style="color: red;">jumlah biodata sudah terkirim sebanyak <span style="color: black;">"{{$sisayb}}"</span>, dari total pemesanan <span style="color: black;">"{{$t->jmlh_yb}}"</span></span>
                        @endforeach
                    </div> -->
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>      

@endsection

<!-- start modal ubah bio -->
<div class="modal fade" id="edibio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; " modal-focus="true" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Biodata Anda</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <form action="{{route('updabio')}}" name="form2" id="form2" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row modal-body">
                    <input name="id" id="id" type="text" class="form-control edit_id"  hidden>
                    <input name="id_pesan" id="id_pesan" type="text" class="form-control edit_id_pesan"  hidden>
                    <div class="form-group detail-v1 col-lg-4">
                        <label for="kelas_bio" class="detail-label">Kelas</label>
                        <div class="input-group">
                            <input type="text"  name="kelas_bio" id="kelas_bio" class="form-control edit_kelas_bio" required>
                        </div>
                    </div>
                    <div class="form-group detail-v1 col-lg-4">
                        <label for="nama_bio" class="detail-label">Nama</label>
                        <div class="input-group">
                            <input type="text"  name="nama_bio" id="nama_bio" class="form-control edit_nama_bio" required>
                        </div>
                    </div>
                    <div class="form-group detail-v1 col-lg-4">
                        <label for="jk_bio" class="detail-label">Jenis Kelamin</label>
                        <div class="input-group">
                            <input type="text"  name="jk_bio" id="jk_bio" class="form-control edit_jk_bio" required>
                        </div>
                    </div>
                    <div class="form-group detail-v1 col-lg-4">
                        <label for="tlhr_bio" class="detail-label">Tempat Lahir</label>
                        <div class="input-group">
                            <input type="text"  name="tlhr_bio" id="tlhr_bio" class="form-control edit_tlhr_bio" required>
                        </div>
                    </div>
                    <div class="form-group detail-v1 col-lg-4">
                        <label for="tgllhr_bio" class="detail-label">Tanggal Lahir</label>
                        <div class="input-group">
                            <input type="date"  name="tgllhr_bio" id="tgllhr_bio" class="form-control edit_tgllhr_bio" required>
                        </div>
                    </div>
                    <div class="form-group detail-v1 col-lg-4">
                        <label for="sosmed_bio" class="detail-label">Sosmed <small>*(IG/FB/etc.)</small></label>
                        <div class="input-group">
                            <input type="text"  name="sosmed_bio" id="sosmed_bio" class="form-control edit_sosmed_bio" required>
                        </div>
                    </div>
                    <div class="col-lg-6 form-group detail-v1 " >
                        <label for="alamat_bio" class="detail-label">Alamat</label>
                        <textarea name="alamat_bio" id="alamat_bio" cols="1" rows="3" class="form-control edit_alamat_bio" required></textarea>
                    </div>
                    <div class="col-lg-6 form-group detail-v1 " >
                        <label for="bio_bio" class="detail-label">Bio</label>
                        <textarea name="bio_bio" id="bio_bio" cols="1" rows="3" class="form-control edit_bio_bio" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit"  class="btn btn-warning">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal ubah bio -->