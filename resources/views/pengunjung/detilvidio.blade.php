@extends('pengunjung.masteruser')
@section('content')
<div class="cover-v1 jarallax" style="" id="home-section">
        <div class="container">
          <div class="row align-items-center">
          @foreach($port_vidio as $f)
            <div class="col-md-10 mx-auto text-center">
              <h1 class="heading" data-aos="fade-up">Portofolio Videography</h1>
              <h2 class="subheading" data-aos="fade-up" data-aos-delay="100">{{$f->nama_vid}}</h2>
              <span class="badge badge-dark subheading text-light icon-eye"data-aos="fade-up" data-aos-delay="200"> {{$jmllihat}}</span>
            </div>

          </div>
        </div>

        <!-- dov -->
        <a href="#portfolio-single-section" class="mouse-wrap dark smoothscroll">
          <span class="mouse">
            <span class="scroll"></span>
          </span>
          <span class="mouse-label">Scroll</span>
        </a>

      </div>
      <!-- END .cover-v1 -->

      <div class="container">
        <div class="portfolio-single-wrap unslate_co--section" id="portfolio-single-section">
            <div class="portfolio-single-inner">
              
              <h2 class="heading-portfolio-single-h2">Portofolio Videography</h2>
              
              
              <div class="row justify-content-between align-items-stretch">

              
                <div class="col-lg-8">
                  <p class="text-center">
                  {!! Embed::make($f->vidio)->parseUrl()->setAttribute(['width' => 700])->getIframe() !!}
                    <!-- <iframe width="400" height="250" src="{!! $f->vidio !!}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                  </p>
              @endforeach


                </div>
                @foreach($port_vidio as $f)
                <div class="col-lg-4 pl-lg-5">
                  <div class="unslate_co--sticky">
                    <div class="row">
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Nama Portofolio</span>
                          <span class="detail-val">{{$f->nama_vid}}</span>
                        </div>
                      </div>
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Tanggal Upload</span>
                          <span class="detail-val">{{substr( $f->created_at,0, 10)}}</span>
                        </div>
                      </div>
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Kategori</span>
                          <span class="detail-val"><a href="#">Videografi</a>, <a>{{$f->nama}}</a></span>
                        </div>
                      </div>
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Keterangan vidio</span>
                          <span class="detail-val">{{$f->ket_vid}}</span>
                        </div>
                      </div>
                      <div class="col-md-12 mb-4">
                        <div class="detail-v1">
                          <span class="detail-label">Rating ({{round($jmlrating,1)}}/5) </span>
                          <div class="rateyo2" data-rateyo-rating="{{round($jmlrating,1)}}"> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="comment-form-wrap pt-5">
                    <h3 class="mb-5">{{$jmlkomen}} Komentar</h3>
                    <ul class="comment-list">
                      @foreach($komentar_vidio as $kom)
                      @if($kom->id_vidio==$id && $kom->status_kom==1)
                      <li class="comment">
                        <div class="vcard bio">
                          <img src="{{asset('pengunjung/images/person_woman_1.jpg')}}" alt="Image placeholder">
                        </div>
                        <div class="comment-body">
                          <h3 class="font-weight-bold">{{$kom->name}}</h3>
                          <div class="meta">{{\Carbon\Carbon::parse($kom->created_at)->diffForHumans()}}</div>
                          <p>{{$kom->komentar}}</p>
                        </div>
                      </li>
                      @endif
                      @endforeach
                    </ul>
                  </div>
                  
              @endforeach
              </div>
            </div>
        </div>
      </div>
      
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('assets/vendor/rate/main.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/rate/jquery.rateyo.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/rate/jquery.rateyo.min.js')}}"></script>
    <script>
            $(function () {
            
            $(".rateyo1").rateYo({
            starWidth: "30px"
            });
        
            $(".rateyo1").rateYo().on("rateyo.change", function(e, data){
            var rating = data.rating;
            $(this).parent().find('.score').text('score :'+ $(this).attr('data-rateyo-score'));
            $(this).parent().find('.result').text(rating);
            $(this).parent().find('input[name=rating]').val(rating);
            });

        });
        </script>
        <script>
            $(function () {
            
            $(".rateyo2").rateYo({
            starWidth: "20px",
            readOnly: true
            });
        });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('assets/vendor/rate/jquery.rateyo.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/rate/jquery.rateyo.min.css')}}">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<!-- Latest compiled and minified JavaScript -->

@endsection