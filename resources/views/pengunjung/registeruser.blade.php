@extends('pengunjung.masteruser')
@section('content')

<div class="cover-v1 jarallax overlay" style="background-image: url('{{asset('pengunjung/images/bg1.jpg')}}');" id="home-section">
    <div class="container">
      <div class="row align-items-center">

      <div class="col-md-12" style="margin-top:110px">
        <h3 class=" text-center text-red "> <strong>Daftar Member</strong></h3> <br>
        <form method="POST" action="{{ route('register') }}" class="col-md-7 mx-auto">
            @csrf
            <div class="form-group row mb-0 detail-v1">
                <input type="text" value="pengunjung" name="role" id="role" hidden>
                <div class="col-lg-6 form-group gsap-reveal">
                    <label for="name" class="detail-label">Nama</label>
                    <input name="name" type="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" id="name" required>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-lg-6 form-group gsap-reveal">
                    <label for="hp" class="detail-label">No. HP</label>
                    <input name="hp" type="hp" class="form-control {{ $errors->has('hp') ? ' is-invalid' : '' }}" id="hp" value="{{ old('hp') }}" required>
                    @if ($errors->has('hp'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('hp') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-lg-6 form-group gsap-reveal">
                    <label for="jk" class="detail-label">Jenis Kelamin</label>
                    <select class="form-control {{ $errors->has('jk') ? ' is-invalid' : '' }}" name="jk" id="jk" value="{{ old('jk') }}" required>
                        <option  >-- Pilih Jenis Kelamin --</option>
                        <option value="Laki - Laki" >Laki - Laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                    @if ($errors->has('jk'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('jk') }}</strong>
                        </span>
                    @endif                                                  
                </div>
                <div class="col-lg-6 form-group gsap-reveal">
                    <label for="email" class="detail-label">Email</label>
                    <input name="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-lg-6 form-group gsap-reveal">
                    <label for="tglhr" class="detail-label">Tanggal Lahir</label>
                    <input name="tglhr" type="date" class="form-control {{ $errors->has('tglhr') ? ' is-invalid' : '' }}" id="tglhr" value="{{ old('tglhr') }}" required>
                    @if ($errors->has('tglhr'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tglhr') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-lg-6 form-group gsap-reveal">
                    <label for="password" class="detail-label">Password <small >(minimal 6 character)</small></label>
                    <input name="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-lg-12 form-group gsap-reveal">
                    <label for="hp" class="detail-label">Alamat</label>
                    <textarea name="alamat" id="alamat" cols="1" rows="1" class="form-control {{ $errors->has('alamat') ? ' is-invalid' : '' }}" value="{{ old('alamat') }}"  required></textarea>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-lg-12">
                    <p class="gsap-reveal">
                    <button class="btn btn-outline-pill btn-bg-white--hover btn-custom-light" type="submit">Daftar</button>
                    </p>
                    <p class="gsap-reveal " style="float:right;">Sudah memiliki akun ? <a href="/login">login</a></p>
                </div>
            </div>
        </form>
        </div>

      </div>
    </div>
  </div>
      <!-- END .cover-v1 -->
</div>


@endsection